let arenaAnimations = function(myPlayerId) {
    let stopped = false;
    let currentAnimations = [];

    function addCurrentAnimation(view) {
        currentAnimations.push(view);
    }

    function removeCurrentAnimation(view) {
        let ind = currentAnimations.indexOf(view);
        if (ind >= 0) {
            currentAnimations.splice(ind, 1);
        }
    }

    function stopAllAnimations() {
        currentAnimations.forEach(function(element) {
            element.stop();
        });
        currentAnimations = [];
    }

    function animate(view, properties, time, easing) {
        return new Promise(function(resolve, reject) {
            if (stopped) {
                resolve(0);
                return;
            }
            addCurrentAnimation(view);
            view.animate(properties, time, easing, _ => {
                removeCurrentAnimation(view);
                resolve(0);
            });
        });
    }

    function sequence(promiseProviders) {
        let head = Promise.resolve(null);
        let tail = promiseProviders.reduce((prev, next) => {
            return prev.then(_ => {
                return next();
            });
        });
        return tail;
    }

    function isYours(playerId) {
        return playerId == myPlayerId;
    }

    function CheckGimmick(obj) {
        let objid = obj["node"]["id"];
        let gimmick;
        let cannon = "cannon";
        let vabank = "vabank";
        let spring = "spring";
        let shield = "shield";
        let fire = "fire";
        let second = "second";
        if (objid.includes(cannon)) {
            gimmick = cannon;
            return gimmick;
        }
        if (objid.includes(vabank)) {
            gimmick = vabank;
            return gimmick;
        }
        if (objid.includes(spring)) {
            gimmick = spring;
            return gimmick;
        }
        if (objid.includes(shield)) {
            gimmick = shield;
            return gimmick;
        }
        if (objid.includes(fire)) {
            gimmick = fire;
            return gimmick;
        }
        if (objid.includes(second)) {
            gimmick = second;
            return gimmick;
        }
    }

    function EventForSound(gimmick) {
        let uniqeventname = new CustomEvent(gimmick, { bubbles: false });
        window.dispatchEvent(uniqeventname);
    }

    return {
        stopAll: () => {
            stoppped = true;
            stopAllAnimations();
        },

        animateGettingReady: () => {
            return new Promise(function(resolve, reject) {
                let countdown = document.getElementById("countdown");
                countdown.classList.add("on");

                let firstCountdounCircle = countdown.getElementsByTagName(
                    "circle"
                )[0];
                firstCountdounCircle.addEventListener(
                    "animationstart",
                    SendSoundTrigger,
                    false
                );

                function SendSoundTrigger() {
                    //ставим звук
                    let startcounter = new CustomEvent("startcounter", {
                        bubbles: false
                    });
                    window.dispatchEvent(startcounter);

                    // удаляем ивентлистенеры
                    firstCountdounCircle.removeEventListener(
                        "animationstart",
                        SendSoundTrigger,
                        false
                    );
                }

                let lastCountdounCircle = document
                    .getElementById("countdown")
                    .getElementsByTagName("circle")[2];
                lastCountdounCircle.addEventListener(
                    "webkitAnimationEnd",
                    AnimationListener,
                    false
                );

                function AnimationListener(event) {
                    countdown.classList.remove("on");

                    lastCountdounCircle.removeEventListener(
                        "webkitAnimationEnd",
                        AnimationListener,
                        false
                    );
                    resolve(event);
                }
            });
        },

        animateGimmickPlayed: gimmickView => {
            return animate(gimmickView, { opacity: 0.3 }, 500).then(() => {
                //console.log(gimmickView['node']['id']);
                let gimmick = CheckGimmick(gimmickView);
                if (gimmick) {
                    EventForSound(gimmick);
                }
                return animate(gimmickView, { opacity: 1.0 }, 500);
            });
        },

        animateGimmickRemoved: gimmickView => {
            return animate(gimmickView, { opacity: 0.3 }, 500);
        },

        animateMoveToHole: (rockView, path) => {
            var pathLength = Snap.path.getTotalLength(path);
            return new Promise(function(resolve, reject) {
                Snap.animate(
                    0,
                    pathLength,
                    function(val) {
                        if (stopped) {
                            return resolve(0);
                        }
                        let p = Snap.path.getPointAtLength(path, val);
                        rockView.attr("cx", p.x);
                        rockView.attr("cy", p.y);
                    },
                    500,
                    null,
                    _ => resolve(0)
                );
            });
        },

        animateRockMirrored: (rockView, mirrorPosition) => {
            return new Promise(function(resolve, reject) {
                if (stopped) {
                    return resolve(0);
                }
                //Звук — «Зеркало»
                let rockMirrored = new CustomEvent("rockMirrored", {
                    bubbles: false
                });
                window.dispatchEvent(rockMirrored);

                rockView.attr({ cx: mirrorPosition.cx, cy: mirrorPosition.cy });
                resolve(0);
            });
        },

        animateHoleDefenced: (rockView, startPosition) => {
            //Звук — лунка защищена
            let holeDefenced = new CustomEvent("holeDefenced", {
                bubbles: false
            });
            window.dispatchEvent(holeDefenced);

            return animate(
                rockView,
                { cx: startPosition.cx, cy: startPosition.cy },
                500
            );
        },

        animateAttackDroped: (rockView, path) => {
            var pathLength = Snap.path.getTotalLength(path);
            return new Promise(function(resolve, reject) {
                Snap.animate(
                    pathLength,
                    0,
                    function(val) {
                        let p = Snap.path.getPointAtLength(path, val);
                        if (stopped) {
                            return resolve(0);
                        }
                        rockView.attr("cx", p.x);
                        rockView.attr("cy", p.y);
                    },
                    500,
                    null,
                    _ => resolve(0)
                );
            });
        },

        animateAttackFailed: (rockView, startPosition) => {
            console.log("animateAttackFailed");
            return animate(
                rockView,
                { cx: startPosition.cx, cy: startPosition.cy },
                500
            );
        },

        animateGoal: (rockView, atHolePosition, startPosition) => {
            //console.log(rockView);
            return animate(rockView, { opacity: 0.0 }, 500).then(() => {
                rockView.attr("cx", startPosition.cx);
                rockView.attr("cy", startPosition.cy);
                return animate(rockView, { opacity: 1.0 }, 500);
            });
        }
    };
};
