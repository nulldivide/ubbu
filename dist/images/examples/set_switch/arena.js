//КАМНИ и их обработчики
//левое направление
function LeftDirection() {
    var rock = event.target.parentNode;
    rock.getElementsByTagName('circle')[0].classList.add('left');
    
  	if (rock.id == 'firstRock') {
  		document.getElementById('defenceLineYour0').classList.add('on');
  		document.getElementById('defenceYour0').classList.add('on');
  	} else if (rock.id == 'secondRock') {
  		document.getElementById('defenceLineYour2').classList.add('on');
  		document.getElementById('defenceYour1').classList.add('on');
  	} else if (rock.id == 'thirdRock') {
  		document.getElementById('defenceLineYour4').classList.add('on');
  		document.getElementById('defenceYour2').classList.add('on');
  	} else if (rock.id == 'fourthRock') {
  		document.getElementById('defenceLineYour6').classList.add('on');
  		document.getElementById('defenceYour3').classList.add('on');
  	}
} 
function OffDirectionLeft() {
    var rock = event.target.parentNode;
    rock.getElementsByTagName('circle')[0].classList.remove('left');

    if (rock.id == 'firstRock') {
  		document.getElementById('defenceLineYour0').classList.remove('on');
  		document.getElementById('defenceYour0').classList.remove('on');
  	} else if (rock.id == 'secondRock') {
  		document.getElementById('defenceLineYour2').classList.remove('on');
  		document.getElementById('defenceYour1').classList.remove('on');
  	} else if (rock.id == 'thirdRock') {
  		document.getElementById('defenceLineYour4').classList.remove('on');
  		document.getElementById('defenceYour2').classList.remove('on');
  	} else if (rock.id == 'fourthRock') {
  		document.getElementById('defenceLineYour6').classList.remove('on');
  		document.getElementById('defenceYour3').classList.remove('on');
  	}
}

//правое направление
function TopDirection() {
    var rock = event.target.parentNode;
    rock.getElementsByTagName('circle')[0].classList.add('top');

    if (rock.id == 'firstRock') {
  		document.getElementById('attackFirstLineYour0').classList.add('on');
  		document.getElementById('attackYour0').classList.add('on');
  	} else if (rock.id == 'secondRock') {
  		document.getElementById('attackFirstLineYour1').classList.add('on');
  		document.getElementById('attackYour1').classList.add('on');
  	} else if (rock.id == 'thirdRock') {
  		document.getElementById('attackFirstLineYour2').classList.add('on');
  		document.getElementById('attackYour2').classList.add('on');
  	} else if (rock.id == 'fourthRock') {
  		document.getElementById('attackFirstLineYour3').classList.add('on');
  		document.getElementById('attackYour3').classList.add('on');
  	}
} 
function OffDirectionTop() {
    var rock = event.target.parentNode;
    rock.getElementsByTagName('circle')[0].classList.remove('top');

    if (rock.id == 'firstRock') {
  		document.getElementById('attackFirstLineYour0').classList.remove('on');
  		document.getElementById('attackYour0').classList.remove('on');
  	} else if (rock.id == 'secondRock') {
  		document.getElementById('attackFirstLineYour1').classList.remove('on');
  		document.getElementById('attackYour1').classList.remove('on');
  	} else if (rock.id == 'thirdRock') {
  		document.getElementById('attackFirstLineYour2').classList.remove('on');
  		document.getElementById('attackYour2').classList.remove('on');
  	} else if (rock.id == 'fourthRock') {
  		document.getElementById('attackFirstLineYour3').classList.remove('on');
  		document.getElementById('attackYour3').classList.remove('on');
  	}
}

// вперёд
function RightDirection() {
    var rock = event.target.parentNode;
    rock.getElementsByTagName('circle')[0].classList.add('right');

  	if (rock.id == 'firstRock') {
  		document.getElementById('defenceLineYour1').classList.add('on');
  		document.getElementById('defenceYour1').classList.add('on');
  	} else if (rock.id == 'secondRock') {
  		document.getElementById('defenceLineYour3').classList.add('on');
  		document.getElementById('defenceYour2').classList.add('on');
  	} else if (rock.id == 'thirdRock') {
  		document.getElementById('defenceLineYour5').classList.add('on');
  		document.getElementById('defenceYour3').classList.add('on');
  	} 
} 
function OffDirectionRight() {
    var rock = event.target.parentNode;
    rock.getElementsByTagName('circle')[0].classList.remove('right');

  	if (rock.id == 'firstRock') {
		document.getElementById('defenceLineYour1').classList.remove('on');
		document.getElementById('defenceYour1').classList.remove('on');
  	} else if (rock.id == 'secondRock') {
  		document.getElementById('defenceLineYour3').classList.remove('on');
  		document.getElementById('defenceYour2').classList.remove('on');
  	} else if (rock.id == 'thirdRock') {
  		document.getElementById('defenceLineYour5').classList.remove('on');
  		document.getElementById('defenceYour3').classList.remove('on');
  	} 
}

//ставим обработчики событий на зонах активации камней
for (i = 0; i < 4; i++){
  var leftZone = 'activationZoneLeft' + i;
  var rightZone = 'activationZoneRight' + i;
  var topZone = 'activationZoneTop' + i;

  if (i != 3){
    document.getElementById(leftZone).addEventListener('mouseenter', LeftDirection);
    document.getElementById(leftZone).addEventListener('mouseleave', OffDirectionLeft);
    
    document.getElementById(rightZone).addEventListener('mouseenter', RightDirection);
    document.getElementById(rightZone).addEventListener('mouseleave', OffDirectionRight);
  
    document.getElementById(topZone).addEventListener('mouseenter', TopDirection);
    document.getElementById(topZone).addEventListener('mouseleave', OffDirectionTop);
  } else {
    document.getElementById(leftZone).addEventListener('mouseenter', LeftDirection);
    document.getElementById(leftZone).addEventListener('mouseleave', OffDirectionLeft);
    
    document.getElementById(topZone).addEventListener('mouseenter', TopDirection);
    document.getElementById(topZone).addEventListener('mouseleave', OffDirectionTop);
  }
};
//////////////////////////////////////////

//ПРИЁМЫ и их обаботчики
// находим зоны активации приёмов
var zone0 = document.getElementById('gimmicksActivationZone0');
var zone1 = document.getElementById('gimmicksActivationZone1');
var zone2 = document.getElementById('gimmicksActivationZone2');
var zone3 = document.getElementById('gimmicksActivationZone3');

//добавляем обработчики событи на все приёмы
function Gimmicks(){
  var Cannon = document.getElementById('cannon');
  var Fire = document.getElementById('fire');
  var Shield = document.getElementById('shield');
  var Mirror = document.getElementById('mirror');
  var Switch = document.getElementById('switch');
  var Spring = document.getElementById('spring');
  var Second = document.getElementById('second');
  var Vabank = document.getElementById('vabank');
  
  var Gimmicks = [Cannon, Fire, Shield, Mirror, Switch, Spring, Second, Vabank];
  
  //при наведение на зону активации приёма подсвечиваем приём и рамку
  for(i = 0; i < Gimmicks.length; i++) {
    Gimmicks[i].addEventListener('mouseenter', LightGimmickInSet);
    Gimmicks[i].addEventListener('mouseleave', OffGimmickInSet);
  }

  // добавляем события при нажатии на приёмы
  Cannon.addEventListener("click", ClickOnGimmick);
  Fire.addEventListener("click", ClickOnGimmick);
  Switch.addEventListener("click", ClickOnGimmick);

}

Gimmicks();

//подсвечиваем приём на панели при наведени на него
function LightGimmickInSet() {
  event.target.parentNode.getElementsByTagName('use')[0].classList.add('on');
}

//убираем подсветку приёма на панели, когда курсор ушёл с него
function OffGimmickInSet() {
  event.target.parentNode.getElementsByTagName('use')[0].classList.remove('on');
}

// действия при выборе приёма
function ClickOnGimmick() {
  //записываем, какой приём выбран
  var gimmick = event.target.parentNode.getAttribute('class');
  console.log("Выбраный приём:" + gimmick);

  //убираем события на выбранном приёме
  event.target.removeEventListener('mouseenter', LightGimmickInSet);
  event.target.removeEventListener('mouseleave', OffGimmickInSet);
  event.target.removeEventListener("click", ClickOnGimmick);

  //помечаем, что приём использован
  event.target.parentNode.classList.add('used');

  // отображаем подсказки нажатого приёма на поле
  for(i = 0; i < 4; i++ ){
    document.getElementById(gimmick + 'Your' + i).classList.add('visibleMiddle');
  };

  if (gimmick != 'switch'){
    //включаем зоны расположения приёмов
    gimmicksActivationZoneOn();

    // дабавляем обработчики событий на зоках применения приёмов
    zone0.addEventListener('mouseenter', Zone0On);
    zone0.addEventListener('mouseleave', Zone0Off);
    zone1.addEventListener('mouseenter', Zone1On);
    zone1.addEventListener('mouseleave', Zone1Off);
    zone2.addEventListener('mouseenter', Zone2On);
    zone2.addEventListener('mouseleave', Zone2Off);
    zone3.addEventListener('mouseenter', Zone3On);
    zone3.addEventListener('mouseleave', Zone3Off);

    function Zone0On() {
      document.getElementById(gimmick + 'Your3').classList.remove('visibleMiddle');
      document.getElementById(gimmick + 'Your3').classList.add('on');
    }
    function Zone0Off() {
      document.getElementById(gimmick + 'Your3').classList.remove('on');
      document.getElementById(gimmick + 'Your3').classList.add('visibleMiddle');
    }
    

    function Zone1On() {
      document.getElementById(gimmick + 'Your2').classList.remove('visibleMiddle');
      document.getElementById(gimmick + 'Your2').classList.add('on');
    }
    function Zone1Off() {
      document.getElementById(gimmick + 'Your2').classList.remove('on');
      document.getElementById(gimmick + 'Your2').classList.add('visibleMiddle');
    }


    function Zone2On() {
      document.getElementById(gimmick + 'Your1').classList.remove('visibleMiddle');
      document.getElementById(gimmick + 'Your1').classList.add('on');
    }
    function Zone2Off() {
      document.getElementById(gimmick + 'Your1').classList.remove('on');
      document.getElementById(gimmick + 'Your1').classList.add('visibleMiddle');
    }


    function Zone3On() {
      document.getElementById(gimmick + 'Your0').classList.remove('visibleMiddle');
      document.getElementById(gimmick + 'Your0').classList.add('on');
    }
    function Zone3Off() {
      document.getElementById(gimmick + 'Your0').classList.remove('on');
      document.getElementById(gimmick + 'Your0').classList.add('visibleMiddle');
    }
  } else {
    // тут должен быть обработчик для перевода
    switchManipulation();
  }
  
  function putGimmick() {
    //отключаем обработчик событий для зон применения приёма
    zone0.removeEventListener('mouseenter', Zone0On);
    zone0.removeEventListener('mouseleave', Zone0Off);
    zone1.removeEventListener('mouseenter', Zone1On);
    zone1.removeEventListener('mouseleave', Zone1Off);
    zone2.removeEventListener('mouseenter', Zone2On);
    zone2.removeEventListener('mouseleave', Zone2Off);
    zone3.removeEventListener('mouseenter', Zone3On);
    zone3.removeEventListener('mouseleave', Zone3Off);

    event.target.removeEventListener('click', putGimmick);

    //выключаем зоны активации для приёма
    gimmicksActivationZoneOff();
    
    // убираем подсказки с поля
    if (event.target == zone0) {
      document.getElementById(gimmick + 'Your0').classList.remove('visibleMiddle');
      document.getElementById(gimmick + 'Your1').classList.remove('visibleMiddle');
      document.getElementById(gimmick + 'Your2').classList.remove('visibleMiddle');
    } else if (event.target == zone1) {
      document.getElementById(gimmick + 'Your0').classList.remove('visibleMiddle');
      document.getElementById(gimmick + 'Your1').classList.remove('visibleMiddle');
      document.getElementById(gimmick + 'Your3').classList.remove('visibleMiddle');
    } else if (event.target == zone2) {
      document.getElementById(gimmick + 'Your0').classList.remove('visibleMiddle');
      document.getElementById(gimmick + 'Your2').classList.remove('visibleMiddle');
      document.getElementById(gimmick + 'Your3').classList.remove('visibleMiddle');
    } else if (event.target == zone3) {
      document.getElementById(gimmick + 'Your1').classList.remove('visibleMiddle');
      document.getElementById(gimmick + 'Your2').classList.remove('visibleMiddle');
      document.getElementById(gimmick + 'Your3').classList.remove('visibleMiddle');
    }
  }
  
  // включаем обработчик событий при нажатии на зону применения приёма
  zone0.addEventListener('click', putGimmick);
  zone1.addEventListener('click', putGimmick);
  zone2.addEventListener('click', putGimmick);
  zone3.addEventListener('click', putGimmick);
}

function gimmicksActivationZoneOn() {
  zone0.classList.add('on');
  zone1.classList.add('on');
  zone2.classList.add('on');
  zone3.classList.add('on');
}

function gimmicksActivationZoneOff() {
  zone0.classList.remove('on');
  zone1.classList.remove('on');
  zone2.classList.remove('on');
  zone3.classList.remove('on');
}

function switchManipulation() {
  
  //зоны выбора для перевода
  var zoneLeft0 = document.getElementById('activationZoneLeft0');
  var zoneRight0 = document.getElementById('activationZoneRight0');
  var zoneLeft1 = document.getElementById('activationZoneLeft1');
  var zoneRight1 = document.getElementById('activationZoneRight1');
  var zoneLeft2 = document.getElementById('activationZoneLeft2');
  var zoneRight2 = document.getElementById('activationZoneRight2');
  var zoneLeft3 = document.getElementById('activationZoneLeft3');

  // включаем зоны для активации перевода
  zoneLeft0.classList.add('on');
  zoneRight0.classList.add('on');
  zoneLeft1.classList.add('on');
  zoneRight1.classList.add('on');
  zoneLeft2.classList.add('on');
  zoneRight2.classList.add('on');
  zoneLeft3.classList.add('on');

  // удаляем обработчики событий для камней на зонах
  zoneLeft0.removeEventListener('mouseenter', LeftDirection);
  zoneRight0.removeEventListener('mouseenter', RightDirection);
  zoneLeft1.removeEventListener('mouseenter', LeftDirection);
  zoneRight1.removeEventListener('mouseenter', RightDirection);
  zoneLeft1.removeEventListener('mouseenter', LeftDirection);
  zoneRight1.removeEventListener('mouseenter', RightDirection);
  zoneLeft2.removeEventListener('mouseenter', LeftDirection);
  zoneRight2.removeEventListener('mouseenter', RightDirection);
  zoneLeft3.removeEventListener('mouseenter', LeftDirection);

  zoneLeft0.removeEventListener('mouseleave', OffDirectionLeft);
  zoneRight0.removeEventListener('mouseleave', OffDirectionRight);
  zoneLeft1.removeEventListener('mouseleave', OffDirectionLeft);
  zoneRight1.removeEventListener('mouseleave', OffDirectionRight);
  zoneLeft1.removeEventListener('mouseleave', OffDirectionLeft);
  zoneRight1.removeEventListener('mouseleave', OffDirectionRight);
  zoneLeft2.removeEventListener('mouseleave', OffDirectionLeft);
  zoneRight2.removeEventListener('mouseleave', OffDirectionRight);
  zoneLeft3.removeEventListener('mouseleave', OffDirectionLeft);
  
  //ставим обработчики событий на зоны применения перевода
  zoneLeft0.addEventListener('mouseenter', addLeftSwitchLine0);
  zoneLeft0.addEventListener('mouseleave', removeLeftSwitchLine0);
  zoneLeft0.addEventListener('click', putSwitch);

  zoneRight0.addEventListener('mouseenter', addRightSwitchLine0);
  zoneRight0.addEventListener('mouseleave', removeRightSwitchLine0);
  zoneRight0.addEventListener('click', putSwitch);

  zoneLeft1.addEventListener('mouseenter', addLeftSwitchLine1);
  zoneLeft1.addEventListener('mouseleave', removeLeftSwitchLine1);
  zoneLeft1.addEventListener('click', putSwitch);

  zoneRight1.addEventListener('mouseenter', addRightSwitchLine1);
  zoneRight1.addEventListener('mouseleave', removeRightSwitchLine1);
  zoneRight1.addEventListener('click', putSwitch);

  zoneLeft2.addEventListener('mouseenter', addLeftSwitchLine2);
  zoneLeft2.addEventListener('mouseleave', removeLeftSwitchLine2);
  zoneLeft2.addEventListener('click', putSwitch);

  zoneRight2.addEventListener('mouseenter', addRightSwitchLine2);
  zoneRight2.addEventListener('mouseleave', removeRightSwitchLine2);
  zoneRight2.addEventListener('click', putSwitch);

  zoneLeft3.addEventListener('mouseenter', addLeftSwitchLine3);
  zoneLeft3.addEventListener('mouseleave', removeLeftSwitchLine3); 
  zoneLeft3.addEventListener('click', putSwitch);

  function RemoveSwitchListeners() {
    zoneLeft0.removeEventListener('mouseenter', addLeftSwitchLine0);
    zoneLeft0.removeEventListener('mouseleave', removeLeftSwitchLine0);
    zoneLeft0.removeEventListener('click', putSwitch);
  
    zoneRight0.removeEventListener('mouseenter', addRightSwitchLine0);
    zoneRight0.removeEventListener('mouseleave', removeRightSwitchLine0);
    zoneRight0.removeEventListener('click', putSwitch);
  
    zoneLeft1.removeEventListener('mouseenter', addLeftSwitchLine1);
    zoneLeft1.removeEventListener('mouseleave', removeLeftSwitchLine1);
    zoneLeft1.removeEventListener('click', putSwitch);
  
    zoneRight1.removeEventListener('mouseenter', addRightSwitchLine1);
    zoneRight1.removeEventListener('mouseleave', removeRightSwitchLine1);
    zoneRight1.removeEventListener('click', putSwitch);
  
    zoneLeft2.removeEventListener('mouseenter', addLeftSwitchLine2);
    zoneLeft2.removeEventListener('mouseleave', removeLeftSwitchLine2);
    zoneLeft2.removeEventListener('click', putSwitch);
  
    zoneRight2.removeEventListener('mouseenter', addRightSwitchLine2);
    zoneRight2.removeEventListener('mouseleave', removeRightSwitchLine2);
    zoneRight2.removeEventListener('click', putSwitch);
  
    zoneLeft3.removeEventListener('mouseenter', addLeftSwitchLine3);
    zoneLeft3.removeEventListener('mouseleave', removeLeftSwitchLine3); 
    zoneLeft3.removeEventListener('click', putSwitch);
  }

  //линии переводов
  var switchLineToLeftEnemy2 = document.getElementById('switchLineToLeftEnemy2');
  var switchLineToLeftEnemy1 = document.getElementById('switchLineToLeftEnemy1');
  var switchLineToLeftEnemy0 = document.getElementById('switchLineToLeftEnemy0');
  var switchLineToRightEnemy2 = document.getElementById('switchLineToRightEnemy2');
  var switchLineToRightEnemy1 = document.getElementById('switchLineToRightEnemy1');
  var switchLineToRightEnemy0 = document.getElementById('switchLineToRightEnemy0');

  function putSwitch() {
    //удаляем обработчики собитий на зонах применения перевода
    RemoveSwitchListeners();

    var target = event.target;
    console.log(target.id);
    
    // на какой зоне кликнут пользователь? — Там и оставляем перевод
    switch(target.id) {
      case activationZoneLeft0:
        addLeftSwitchLine0();
        break;
      case activationZoneLeft1:
        addLeftSwitchLine1();
        break;
      case activationZoneLeft2:
        addLeftSwitchLine2();
        break;
      case activationZoneLeft3:
        addLeftSwitchLine3();
        break;
      case activationZoneRight0:
       addRightSwitchLine0();
        break;
      case activationZoneRight1:
        addRightSwitchLine1();
        break;
      case activationZoneRight2:
        addRightSwitchLine2();
        break;
    }

  // убираем с поля подсказки о возможных точках направления перевода
  if (event.target == activationZoneLeft0 ) {
    document.getElementById('switchYour0').classList.remove('visibleMiddle');
    document.getElementById('switchYour1').classList.remove('visibleMiddle');
    document.getElementById('switchYour2').classList.remove('visibleMiddle');
  }  else if (event.target == activationZoneRight0 ) {
    document.getElementById('switchYour0').classList.remove('visibleMiddle');
    document.getElementById('switchYour1').classList.remove('visibleMiddle');
    document.getElementById('switchYour3').classList.remove('visibleMiddle');
  } else if (event.target == activationZoneLeft1) {
    document.getElementById('switchYour0').classList.remove('visibleMiddle');
    document.getElementById('switchYour1').classList.remove('visibleMiddle');
    document.getElementById('switchYour3').classList.remove('visibleMiddle');
  } else if (event.target == activationZoneRight1) {
    document.getElementById('switchYour0').classList.remove('visibleMiddle');
    document.getElementById('switchYour2').classList.remove('visibleMiddle');
    document.getElementById('switchYour3').classList.remove('visibleMiddle');
  } else if (event.target == activationZoneLeft2) {
    document.getElementById('switchYour0').classList.remove('visibleMiddle');
    document.getElementById('switchYour2').classList.remove('visibleMiddle');
    document.getElementById('switchYour3').classList.remove('visibleMiddle');
  } else if (event.target == activationZoneRight2) {
    document.getElementById('switchYour1').classList.remove('visibleMiddle');
    document.getElementById('switchYour2').classList.remove('visibleMiddle');
    document.getElementById('switchYour3').classList.remove('visibleMiddle');
  } else if (event.target == activationZoneLeft3) {
    document.getElementById('switchYour1').classList.remove('visibleMiddle');
    document.getElementById('switchYour2').classList.remove('visibleMiddle');
    document.getElementById('switchYour3').classList.remove('visibleMiddle');
  } 

  // выключаем зоны для активации перевода
  zoneLeft0.classList.remove('on');
  zoneRight0.classList.remove('on');
  zoneLeft1.classList.remove('on');
  zoneRight1.classList.remove('on');
  zoneLeft2.classList.remove('on');
  zoneRight2.classList.remove('on');
  zoneLeft3.classList.remove('on');
  }


  function addLeftSwitchLine0(){
    switchLineToLeftEnemy2.classList.add('on');
    document.getElementById('attackSecondLineEnemy2').classList.add('off');
    document.getElementById('switchYour3').classList.remove('visibleMiddle');
    document.getElementById('switchYour3').classList.add('on');
  }

  function removeLeftSwitchLine0(){
    switchLineToLeftEnemy2.classList.remove('on');
    document.getElementById('attackSecondLineEnemy2').classList.remove('off');
    document.getElementById('switchYour3').classList.remove('on');
    document.getElementById('switchYour3').classList.add('visibleMiddle');
  }

  function addRightSwitchLine0(){
    switchLineToRightEnemy2.classList.add('on');
    document.getElementById('attackSecondLineEnemy3').classList.add('off');
    document.getElementById('switchYour2').classList.remove('visibleMiddle');
    document.getElementById('switchYour2').classList.add('on');
  }

  function removeRightSwitchLine0(){
    switchLineToRightEnemy2.classList.remove('on');
    document.getElementById('attackSecondLineEnemy3').classList.remove('off');
    document.getElementById('switchYour2').classList.add('visibleMiddle');
    document.getElementById('switchYour2').classList.remove('on');
  }

  function addLeftSwitchLine1(){
    switchLineToLeftEnemy1.classList.add('on');
    document.getElementById('attackSecondLineEnemy1').classList.add('off');
    document.getElementById('switchYour2').classList.remove('visibleMiddle');
    document.getElementById('switchYour2').classList.add('on');
  }

  function removeLeftSwitchLine1(){
    switchLineToLeftEnemy1.classList.remove('on');
    document.getElementById('attackSecondLineEnemy1').classList.remove('off');
    document.getElementById('switchYour2').classList.remove('on');
    document.getElementById('switchYour2').classList.add('visibleMiddle');
  }

  function addRightSwitchLine1(){
    switchLineToRightEnemy1.classList.add('on');
    document.getElementById('attackSecondLineEnemy2').classList.add('off');
    document.getElementById('switchYour1').classList.remove('visibleMiddle');
    document.getElementById('switchYour1').classList.add('on');
  }

  function removeRightSwitchLine1(){
    switchLineToRightEnemy1.classList.remove('on');
    document.getElementById('attackSecondLineEnemy2').classList.remove('off');
    document.getElementById('switchYour1').classList.add('visibleMiddle');
    document.getElementById('switchYour1').classList.remove('on');
  }

  function addLeftSwitchLine2(){
    switchLineToLeftEnemy0.classList.add('on');
    document.getElementById('attackSecondLineEnemy0').classList.add('off');
    document.getElementById('switchYour1').classList.remove('visibleMiddle');
    document.getElementById('switchYour1').classList.add('on');
  }

  function removeLeftSwitchLine2(){
    switchLineToLeftEnemy0.classList.remove('on');
    document.getElementById('attackSecondLineEnemy0').classList.remove('off');
    document.getElementById('switchYour1').classList.remove('on');
    document.getElementById('switchYour1').classList.add('visibleMiddle');
  }

  function addRightSwitchLine2(){
    switchLineToRightEnemy0.classList.add('on');
    document.getElementById('attackSecondLineEnemy1').classList.add('off');
    document.getElementById('switchYour0').classList.remove('visibleMiddle');
    document.getElementById('switchYour0').classList.add('on');
  }

  function removeRightSwitchLine2(){
    switchLineToRightEnemy0.classList.remove('on');
    document.getElementById('attackSecondLineEnemy1').classList.remove('off');
    document.getElementById('switchYour0').classList.add('visibleMiddle');
    document.getElementById('switchYour0').classList.remove('on');
  }

  function addLeftSwitchLine3(){
    switchLineToRightEnemy0.classList.add('on');
    document.getElementById('attackSecondLineEnemy1').classList.add('off');
    document.getElementById('switchYour0').classList.remove('visibleMiddle');
    document.getElementById('switchYour0').classList.add('on');
  }

  function removeLeftSwitchLine3(){
    switchLineToRightEnemy0.classList.remove('on');
    document.getElementById('attackSecondLineEnemy1').classList.remove('off');
    document.getElementById('switchYour0').classList.add('visibleMiddle');
    document.getElementById('switchYour0').classList.remove('on');
  }

}

//////////////////////////////////////////

// СВЕТОФОР
// чекаем окончание анимации светофора
var lastCountdounCircle = document.getElementById('countdown').getElementsByTagName('circle')[2];
lastCountdounCircle.addEventListener("webkitAnimationEnd", AnimationListener, false);

function AnimationListener(){
  console.log('Анимация закончилась');
}