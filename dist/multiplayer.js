var game = null;

function connectServer(gameId, playerId) {
    var g = gameById(gameId)
    var server = new ServerLogic(g, playerId);
    return server;
}

function gameById(gameId) {
    if (game == null) {
        game = ServerLogic.create(gameId, {positionStageTime:20, roundsCount:4});
    }
    return game;
}