const gulp = require('gulp');
const scss = require('gulp-sass');

gulp.task('scss', () => {
    return gulp.src('src/frontend/vkcom/style/main.scss')
        .pipe( scss({outputStyle: 'compressed'}).on('error', scss.logError) )
        .pipe(gulp.dest('public'))
});

gulp.task('watch', () => {
    gulp.watch('src/frontend/vkcom/style/main.scss', ['scss']);
});


gulp.task('default', ['scss', 'watch']);