const express = require("express");
const http = require("http");
const socketIO = require("socket.io");

const DB = require("./components/DBservices/DB.js");
const Fabric = require("./components/Fabric.js");

const Achievements = require("./components/achievements.js");

const Notify = require("./components/notifications");
Notify.Init();

const app = express();
const server = http.Server(app);
const io = socketIO(server);

const port = process.env.PORT;
const privateIP = process.env.privateIP;

//прячем консолинг на проде
if (process.env.NODE_ENV === "production") {
    console.log = function() {};
    console.dir = function() {};
}

const exphbs = require("express-handlebars");
app.engine(
    ".hbs",
    exphbs({
        extname: ".hbs",
        defaultLayout: "main"
    })
);
app.set("view engine", ".hbs");

app.set("port", process.env.PORT || port);

app.get("/", (req, res) => {
    res.render("home");
});

app.get("/stickers", (req, res) => {
    res.sendFile("stickers.json", { root: __dirname + "/../public" });
});

app.use((req, res) => {
    res.status(404);
    res.render("404");
});

app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500);
    res.render("500");
});

server.listen(port, privateIP, () => {
    console.log(
        ` \n[[ ${app.get("env")} UBBU on ${privateIP}:${app.get("port")} ]]`
    );
});

let gamefabric = new Fabric();

let connectCounter = 0;

io.on("connection", socket => {
    socket.on("shuttleToServer", data => {
        // console.log(typeof data);
        // console.log(data);

        switch (typeof data) {
            case "number":
                console.log(`Player ${data} on server`);
                //logger.info(`Player ${data} on server`);

                //запрашиваем историю игрока
                DB.GetUserData(data).then(playerdata => {
                    socket.emit("suttleToClient", playerdata);
                });
                break;

            case "string":
                DB.GetRating().then(obj => {
                    socket.emit("suttleToClient", obj);
                });
                break;
        }
    });

    //обрабатываем запрос на актуализацию данных пользователя
    socket.on("actualizeuser", playerid => {
        console.log(`${playerid} actualisation`);

        //запрашиваем информацию по игроку
        DB.GetUserData(playerid).then(playerdata => {
            socket.emit("userdata", playerdata);
            //console.dir(playerdata, {colors: true});
            //актуализируем ачивки
            Achievements.Refresh({ socket, playerid, playerdata });
        });

        //шлем число онлайн юзеров
        socket.emit("onlineusers", connectCounter);
    });

    socket.on("PLAY", playerid => {
        console.log(`${playerid} 🎮`);
        gamefabric.Play(socket, playerid);
    });

    socket.on("setvalue", m => {
        console.log(`${m.playerId} — ${m.key}: "${m.value}"`);
        DB.SetValue(m.playerId, m.key, m.value);
    });

    socket.on("setpushechkarating", m => {
        //console.dir(m, {colors: true});
        DB.SetPushechkaRating(m.playerid, m.incr);
    });

    socket.on("getpushechkarating", () => {
        DB.GetPushechkaRating().then(obj => {
            socket.emit("pushechkarating", obj);
        });
    });

    //считаем пользователей онлайн
    connectCounter++;
    socket.broadcast.emit("onlineusers", connectCounter);
    socket.on("disconnect", () => {
        if (connectCounter !== 0) {
            connectCounter--;
            socket.broadcast.emit("onlineusers", connectCounter);
        }
        console.log(connectCounter + " 💭 one escaped");

        let listenerstokill = [
            "shuttleToServer",
            "actualizeuser",
            "PLAY",
            "setvalue",
            "setpushechkarating",
            "getpushechkarating"
        ];

        for (let i = 0; i < listenerstokill.length; i++) {
            socket.removeAllListeners(listenerstokill[i]);
            //console.log(`🚫 ${listenerstokill[i]}`);
        }
    });
});

exports.gamefabric = gamefabric;
