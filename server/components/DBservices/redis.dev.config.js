const redisDevConfig = {
    host: "10.135.15.113",
    port: 6379,
    db: 2,
    showFriendlyErrorStack: true
};

module.exports = redisDevConfig;
