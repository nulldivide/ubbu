const prodconfig = require("./sentinelconfig");
const devconfig = require("./redis.dev.config.js");
let config = devconfig;

if (process.env.NODE_ENV === "production") {
    config = prodconfig;
}

const Redis = require("ioredis");
const client = new Redis(config);

class DB {
    static async CreateGameCard(params) {
        let gameid = params.gameid;
        let date = Date.now();
        let player1id = params.player1;
        let player2id = params.player2;

        try {
            //пишем айдишки игр в карточки игроков
            DB._AddGameIDtoPlayerCard(player1id, gameid);
            DB._AddGameIDtoPlayerCard(player2id, gameid);

            //получаем рейтинги игроков на момент начала игры
            let DBdata = await Promise.all([
                DB._GetPlayerRating(player1id),
                DB._GetPlayerRating(player2id)
            ]);
            let ratingbeforegame1 = DBdata[0];
            let ratingbeforegame2 = DBdata[1];

            //собираем объект игры
            let gameobj = {
                gameid,
                date,
                player1: {
                    id: player1id,
                    ratingBeforeGame: ratingbeforegame1
                },
                player2: {
                    id: player2id,
                    ratingBeforeGame: ratingbeforegame2
                }
            };
            //console.log(gameobj);
            let gamestr = JSON.stringify(gameobj);

            //пишем игру в базу
            client.hmset("Games", gameid, gamestr);
        } catch (e) {
            console.error(e);
        }
    }

    static _AddGameIDtoPlayerCard(playerid, gameid) {
        //получаем игры из карточки игрока
        let playergames = new Promise((resolve, reject) => {
            client.hget(playerid, "games", function(err, data) {
                resolve(data);
            });
        });
        playergames
            .then(function(result) {
                //console.log(`Player games: ${result}`);

                if (!result) {
                    //если игр не было записываем первую айдишку
                    client.hset(playerid, "games", gameid);
                    //console.log(`First game wrote to player ${playerid} card`);
                } else {
                    //если игры были, то приписываем новую игру
                    let newgameslist = gameid + "," + result;
                    client.hset(playerid, "games", newgameslist);
                }
            })
            .catch(e => {
                console.error(e);
            });
    }

    static async AddAchievement(playerid, achid) {
        console.log(playerid, achid);

        try {
            let field = "achievements";

            //получаем ачивки из карточки игрока
            let playerachs = await new Promise((resolve, reject) => {
                client.hget(playerid, field, function(err, data) {
                    resolve(data);
                });
            });

            //если не было ачивок — записываем первую
            if (!playerachs) {
                playerachs = achid;
                console.log("Первая ачивка: " + playerachs);
            } else {
                //если были, то приписываем новую
                playerachs = achid + "," + playerachs;
            }

            //пишем обновлённый сет ачивок в базу
            client.hset(playerid, field, playerachs);
        } catch (e) {
            console.error(e);
        }
    }

    static async AddMultiAch(params) {
        try {
            let { playerid, achid, times, received } = params;
            let field = "achievements";
            let fullach = `${achid}*${times}:${received}`;

            //получаем ачивки из карточки игрока
            let playerachs = await new Promise((resolve, reject) => {
                client.hget(playerid, field, function(err, data) {
                    resolve(data);
                });
            });

            //если не было ачивок — записываем первую
            if (!playerachs) {
                playerachs = fullach;
                console.log("Первая ачивка: " + playerachs);
            } else {
                //ищем была ли такая ачивка
                let arrofplayerachs = playerachs.split(",");
                let index = undefined;
                for (let i = 0; i < arrofplayerachs.length; i++) {
                    let ach = arrofplayerachs[i];
                    //если ачивка была, то записываем её индекс
                    if (~ach.indexOf(achid)) {
                        index = i;
                    }
                }

                //удаляем старую запись об ачивке
                if (index !== undefined) {
                    arrofplayerachs.splice(index, 1);
                }

                //собираем массив назад
                playerachs = arrofplayerachs.join(",");

                //записываем новую
                playerachs = fullach + "," + playerachs;
            }

            //пишем обновлённый сет мультиачивок в базу
            client.hset(playerid, field, playerachs);
        } catch (e) {
            console.error();
        }
    }

    static _GetPlayerRating(id) {
        return new Promise((resolve, reject) => {
            client.zrevrank("Rating", id, function(err, data) {
                let rating = data + 1;
                resolve(rating);
            });
        }).catch(e => {
            console.error(e);
        });
    }

    static _GetPlayerPoints(id) {
        return new Promise((resolve, reject) => {
            client.zscore("Rating", id, function(err, data) {
                resolve(data);
            });
        }).catch(e => {
            console.error(e);
        });
    }

    static async GetUserData(id) {
        try {
            //проверяем, есть ли карточка пользователя в базе
            let playerexist = await new Promise((resolve, reject) => {
                client.exists(id, function(err, data) {
                    //console.log(`Player existent: ${data}`);
                    resolve(data);
                });
            });

            //если нет, то заводим карточку и присваем 0 поинтов
            if (!playerexist) {
                console.log(`Create player card`);
                //cоздаём карточку
                await DB._CreatePlayerCard(id);
                //присваеваем 0 поинтов
                await DB.SetRating(id, "lost");
            }

            //возвращаем юзердату
            let userdata = await DB._MakePlayerCard(id);
            //console.dir(userdata, { colors: true });

            return userdata;
        } catch (e) {
            console.error(e);
        }
    }

    static async _CreatePlayerCard(id) {
        try {
            let date = Date.now();
            await client.hset(id, "registrationDate", date);
            console.log(`🗂 card created for player ${id}`);
        } catch (e) {
            console.error(e);
        }
    }

    static async _MakePlayerCard(id) {
        try {
            //что нужно взять из бызы
            let userdata = new Promise((resolve, reject) => {
                client.hgetall(id, function(err, data) {
                    resolve(data);
                });
            });
            let rating = DB._GetPlayerRating(id);
            let points = DB._GetPlayerPoints(id);

            //записываем данные из базы в один массив
            let DBdata = await Promise.all([rating, points, userdata]);
            let history;

            //console.log(DBdata[2]);

            let playerdata = DBdata[2];

            //если есть игры
            if (playerdata["games"]) {
                //получить из базы Games даные каждой игры по айдишкам
                //перобразуем строку в массив
                let gamesidarr = playerdata["games"].split(",");

                //получаем объект игры для каждой айдишки
                history = await DB._GetGamesDataByID(gamesidarr);

                //console.log(history);

                //преодбразуем строчные данные в объект
                for (let i = 0; i < history.length; i++) {
                    // проверяем на наличие игр с записью null
                    if (history[i] === null) {
                        history.splice(i, 1);
                        i--;
                    } else {
                        history[i] = JSON.parse(history[i]);

                        //преобразуем историю под формат необходимый на клиенте
                        if (history[i]["player1"]["id"] === id) {
                            history[i]["me"] = history[i]["player1"];
                            history[i]["opponent"] = history[i]["player2"];
                            delete history[i]["player1"];
                            delete history[i]["player2"];
                        } else {
                            history[i]["me"] = history[i]["player2"];
                            history[i]["opponent"] = history[i]["player1"];
                            delete history[i]["player1"];
                            delete history[i]["player2"];
                        }
                    }
                }
                playerdata["history"] = history;
                delete playerdata["games"];
                //console.log(history);
            }

            //собираем карточку игрока
            let usercard = Object.assign(playerdata, {
                rating: DBdata[0],
                points: DBdata[1]
            });

            //console.log(`Usercard: ${JSON.stringify(usercard)}`);
            return usercard;
        } catch (e) {
            console.error(e);
        }
    }

    static _GetGamesDataByID(gamesidarr) {
        return new Promise((resolve, reject) => {
            client.hmget("Games", gamesidarr, function(err, data) {
                resolve(data);
            });
        }).catch(e => {
            console.error(e);
        });
    }

    static async SetGameResult(params) {
        let gameid = params.gameid;
        let winnerid = params.winnerid;
        let player1id = params.player1id;
        let player2id = params.player2id;
        let player1score = params.player1score;
        let player2score = params.player2score;

        try {
            let DBdata = await Promise.all([
                DB._GetGamesDataByID([gameid]),
                DB._GetPlayerRating(player1id),
                DB._GetPlayerRating(player2id)
            ]);

            //карточка игры и новые райтинги игроков
            let gamedata = DBdata[0];
            let player1newrating = DBdata[1];
            let player2newrating = DBdata[2];

            //если игра существует в базе
            if (gamedata) {
                //конвертируем строку в объект
                gamedata = JSON.parse(gamedata);

                //добавляем новые данные
                gamedata["winnerid"] = winnerid;

                gamedata["player1"]["ratingAfterGame"] = player1newrating;
                gamedata["player1"]["score"] = player1score;

                gamedata["player2"]["ratingAfterGame"] = player2newrating;
                gamedata["player2"]["score"] = player2score;

                //console.log(gamedata);

                //преобразуем объект в строку
                let gamestr = JSON.stringify(gamedata);

                //перезапысываем данные игры в базе
                client.hmset("Games", gameid, gamestr);
            }
        } catch (e) {
            console.error(e);
        }
    }

    static async AddStickerToGameCard(gameid, stickerURL) {
        try {
            // запрашиваем по ID карточку игры в базе
            let gamedata = await DB._GetGamesDataByID([gameid]);

            //если игра существует в базе
            if (gamedata) {
                //конвертируем строку в объект
                gamedata = JSON.parse(gamedata);

                //дабавляем в карточку новые данные
                gamedata["stickerURL"] = stickerURL;

                //преобразуем объект в строку
                let gamestr = JSON.stringify(gamedata);

                //перезапысываем данные игры в базе
                client.hmset("Games", gameid, gamestr);

                //КОНСОЛИМ
                console.log(`🏷 sticker was added to gamecard`);
            }
        } catch (e) {
            console.error(e);
        }
    }

    static SetValue(playerid, key, value) {
        client.hset(playerid, key, value);
    }

    static SetRating(playerid, result) {
        let incr;

        // обределяем сколько очков рейтинга прибавить
        switch (result) {
            case "won":
                incr = 3;
                break;
            case "lost":
                incr = 0;
                break;
            case "draw":
                incr = 1;
                break;
        }

        //записываем результат в базу
        if (playerid) {
            client.zincrby("Rating", incr, playerid);
        }

        //КОНСОЛИМ
        console.log(`👤 ${playerid} — ${result}`);
    }

    static GetRating() {
        function Decorator(data) {
            let ratinglist = {};
            for (let i = 0; i < data.length; i++) {
                let number = i / 2 + 1;
                ratinglist[number] = {
                    id: data[i],
                    points: data[i + 1]
                };
                i++;
            }
            //console.log(ratinglist);
            return ratinglist;
        }

        return new Promise((resolve, reject) => {
            client.zrevrange("Rating", 0, 99, "WITHSCORES", function(
                err,
                data
            ) {
                let ratinglist = Decorator(data);
                resolve(ratinglist);
            });
        }).catch(e => console.error(e));
    }

    static SetPushechkaRating(playerid, incr) {
        //записываем результат в базу
        if (playerid) {
            client.zincrby("PushechkaRating", incr, playerid);
        }
    }

    static GetPushechkaRating() {
        function Decorator(data) {
            let ratinglist = {};
            for (let i = 0; i < data.length; i++) {
                let number = i / 2 + 1;
                ratinglist[number] = {
                    id: data[i],
                    points: data[i + 1]
                };
                i++;
            }
            //console.log(ratinglist);
            return ratinglist;
        }

        return new Promise((resolve, reject) => {
            client.zrevrange("PushechkaRating", 0, 9, "WITHSCORES", function(
                err,
                data
            ) {
                let ratinglist = Decorator(data);

                resolve(ratinglist);
            });
        }).catch(e => console.error(e));
    }

    static GetUsersIds(from, to) {
        return new Promise((resolve, reject) => {
            client.zrevrange("Rating", from, to, function(err, data) {
                resolve(data);
            });
        }).catch(e => console.error(e));
    }
}

module.exports = DB;
