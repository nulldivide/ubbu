const ServerLogic = require("./serverLogic").ServerLogic;
let f = require("../ubbu.js");
let games = {};

const DB = require("./DBservices/DB.js");
const roundDuration = 20; //seconds

class Game {
    constructor(obj) {
        this.gameid = obj.gameid;
        this.resultnotsetted = true;
        this.resultbenchmarked = false;
        this.player1 = {
            playernumber: obj.playernumber,
            playerid: obj.playerid,
            socket: obj.socket
        };
        this.player2 = {
            playernumber: "",
            playerid: "",
            socket: ""
        };

        this.CreateGame.bind(this)(this.player1);
    }

    CreateGame(player) {
        let gameid = this.gameid;

        let playernumber = player.playernumber;
        let playerid = player.playerid;
        let socket = player.socket;

        //если игрок второй — знакомим игроков
        if (playernumber === 2) {
            this.player2 = { playernumber, playerid, socket };
            this._sendIDofPartner(this.player1, this.player2);

            let gameparams = {
                gameid: this.gameid,
                player1: this.player1.playerid,
                player2: this.player2.playerid
            };

            DB.CreateGameCard(gameparams);
        }

        // добавляем пользователя в room для удобства общения с оппонентом
        socket.join(gameid);
        console.log(`Player's socket was added to room 📥`);

        // создаём экземпляр серверной логики
        let server = this._connectGame(gameid, playerid);
        console.log(
            `Game created for player${playernumber}. Game ID: ${gameid}`
        );

        // loop для остлеживания событий на сервере
        let loop = setInterval(
            this._listenServerLogic,
            200,
            server,
            socket,
            playernumber
        ); //clearInterval

        //слушатели клиентских сообщений
        this._clietListener(playernumber, socket, server);

        //тригеры окончания игры
        this._EndGameTriggers(playernumber, playerid, socket, gameid, loop);
    }

    _connectGame(gameId, playerId) {
        let g = this._gameById(gameId);
        let server = new ServerLogic(g, playerId);
        //console.log(`ServerLogic was created`);
        return server;
    }

    _gameById(gameId) {
        if (!games.hasOwnProperty(gameId)) {
            games[gameId] = ServerLogic.create(gameId, {
                positionStageTime: roundDuration,
                roundsCount: 4
            });
        }
        return games[gameId];
    }

    _listenServerLogic(server, socket, playernumber) {
        if (server.endpoint.output.hasMessages()) {
            let m = server.endpoint.output.get();
            let identifier;
            switch (playernumber) {
                case 1:
                    identifier = "🔶";
                    break;
                case 2:
                    identifier = "🔷";
                    break;
            }
            console.log(`${identifier} 🚀`);
            console.dir(m, { colors: true });
            socket.emit("ServerMessage", m);
        }
    }

    _clietListener(playernumber, socket, server) {
        let identifier;
        switch (playernumber) {
            case 1:
                identifier = "🔶";
                break;
            case 2:
                identifier = "🔷";
                break;
        }

        socket.on("ClientMessage", m => {
            console.log(`${identifier} 🕊`);
            console.dir(m, { colors: true });
            server.putMessage(m);
        });
    }

    _EndGameTriggers(playernumber, playerid, socket, gameid, loop) {
        //обрабатываем выход пользователя из приложения
        socket.on("disconnect", () => {
            let result = "lost";

            //был противник
            if (this.player2.socket) {
                //результат записан?
                if (this.resultnotsetted) {
                    this.resultnotsetted = false;
                    console.log(
                        `Player${playernumber} ${result} by disconnect`
                    );

                    //сообщаем оппоненту об отключении игрока
                    socket.broadcast.to(gameid).emit("opponentDisconected");

                    //счёт для записи в БД
                    let imscore = "W.O.";
                    let opponentscore = "";

                    //запись рейтинга и подготовка параметров для записи в базу
                    let paramsforresult = this._resignResultPreparing.bind(
                        this
                    )(gameid, playernumber, imscore, opponentscore);

                    //пишем результат в базу
                    DB.SetGameResult(paramsforresult);
                }
            } else {
                //не было противника
                console.log(`Player${playernumber} was alone and disconnected`);
            }

            //зачистка методов
            clearInterval(loop);
            this._RemoveListeners(result, socket, playernumber);
            f.gamefabric.KillGame(gameid, socket);
        });

        socket.on("RESIGN", () => {
            let result = "lost";

            //был противник
            if (this.player2.socket) {
                //результат записан?
                if (this.resultnotsetted) {
                    this.resultnotsetted = false;
                    console.log(`Player${playernumber} ${result} resigned 🏳`);
                    socket.broadcast.to(gameid).emit("opponentResigned");

                    let imscore = "W.O."; //walkover
                    let opponentscore = "";

                    let paramsforresult = this._resignResultPreparing.bind(
                        this
                    )(gameid, playernumber, imscore, opponentscore);

                    //пишем результат в базу
                    DB.SetGameResult(paramsforresult);
                }
            } else {
                //не было противника
                console.log(
                    `Player${playernumber} did not wait for the opponent`
                );
            }

            this._RemoveListeners(result, socket, playernumber);
            clearInterval(loop);
            f.gamefabric.KillGame(gameid, socket);
        });

        socket.on("wonfromclient", () => {
            let result = "won";

            console.log(`Player${playernumber} ${result} by opponent resign.`);
            this._RemoveListeners(result, socket, playernumber);
            clearInterval(loop);
        });

        socket.on("sticker", stickerURL => {
            DB.AddStickerToGameCard(gameid, stickerURL);
            this._RemoveListeners("wonandsticker", socket, playernumber);
        });

        socket.on("RESULT", m => {
            let result = m.result;
            let imscore = m.imscore;
            let opponentscore = m.opponentscore;
            let emoji;
            let paramsforresult;

            //отправляем оппоненту контрольный результат
            this._benchmarkResultForOpponent(
                gameid,
                socket,
                result,
                playernumber
            );

            clearInterval(loop);
            this._RemoveListeners(result, socket, playernumber);

            if (this.resultnotsetted) {
                this.resultnotsetted = false;

                //подводим итоги и пишем результат в базу
                paramsforresult = this._countResultAndSetRating.bind(this)(
                    gameid,
                    playernumber,
                    imscore,
                    opponentscore
                );
                DB.SetGameResult(paramsforresult);

                //если оппонент не получил окно завершения матча:
                if (!this.resultbenchmarked) {
                    this._benchmarkResultForOpponent(gameid);
                }

                switch (result) {
                    case "lost":
                        emoji = `😔`;
                        break;

                    case "draw":
                        emoji = `🤝`;
                        break;

                    case "won":
                        emoji = `🏆`;
                        break;
                }

                console.log(
                    `${emoji} player${playernumber} ${result} and set the result`
                );
                f.gamefabric.KillGame(gameid, socket);
            }
        });
    }

    _RemoveListeners(result, socket, playernumber) {
        let listenerstokill = [
            "ClientMessage",
            "RESIGN",
            "wonfromclient",
            "RESULT"
        ];

        switch (result) {
            case "lost":
                listenerstokill.push("sticker");
                break;
            case "draw":
                listenerstokill.push("sticker");
                break;
            case "wonandsticker":
                listenerstokill.push("sticker");
        }

        console.log(`${playernumber} 👂🚫 removeAllListeners`);
        for (let i = 0; i < listenerstokill.length; i++) {
            socket.removeAllListeners(listenerstokill[i]);
            //console.log(`   "${listenerstokill[i]}"`);
        }
    }

    _sendIDofPartner(player1, player2) {
        player1["socket"].emit("OpponentID", player2["playerid"]);
        player2["socket"].emit("OpponentID", player1["playerid"]);
        console.log("IDs sent to players");
    }

    _countResultAndSetRating(gameid, playernumber, imscore, opponentscore) {
        //готовим данные для записи в базу
        let winnerid;
        let player1score;
        let player2score;
        let result;
        let player1id = this.player1.playerid;
        let player2id = this.player2.playerid;

        switch (playernumber) {
            case 1:
                player1score = imscore;
                player2score = opponentscore;

                result = player1score - player2score;
                if (result > 0) {
                    winnerid = player1id;
                    //обновляем рейтинг игрокам
                    DB.SetRating(player1id, "won");
                    DB.SetRating(player2id, "lost");
                } else {
                    if (result < 0) {
                        winnerid = player2id;
                        //обновляем рейтинг игрокам
                        DB.SetRating(player1id, "lost");
                        DB.SetRating(player2id, "won");
                    } else {
                        winnerid = "";
                        //обновляем рейтинг игрокам
                        DB.SetRating(player1id, "draw");
                        DB.SetRating(player2id, "draw");
                    }
                }
                break;

            case 2:
                player1score = opponentscore;
                player2score = imscore;

                result = player1score - player2score;
                if (result > 0) {
                    winnerid = player1id;
                    //обновляем рейтинг игрокам
                    DB.SetRating(player1id, "won");
                    DB.SetRating(player2id, "lost");
                } else {
                    if (result < 0) {
                        winnerid = player2id;
                        //обновляем рейтинг игрокам
                        DB.SetRating(player1id, "lost");
                        DB.SetRating(player2id, "won");
                    } else {
                        winnerid = "";
                        //обновляем рейтинг игрокам
                        DB.SetRating(player1id, "draw");
                        DB.SetRating(player2id, "draw");
                    }
                }
                break;
        }

        let params = {
            gameid,
            winnerid,
            player1id,
            player2id,
            player1score,
            player2score
        };
        return params;
    }

    _resignResultPreparing(gameid, playernumber, imscore, opponentscore) {
        //готовим данные для записи в базу
        let winnerid;
        let player1score;
        let player2score;

        switch (playernumber) {
            case 1:
                //обновляем рейтинг игрокам
                DB.SetRating(this.player1.playerid, "lost");
                DB.SetRating(this.player2.playerid, "won");

                winnerid = this.player2.playerid;
                player1score = imscore;
                player2score = opponentscore;
                break;

            case 2:
                //обновляем рейтинг игрокам
                DB.SetRating(this.player1.playerid, "won");
                DB.SetRating(this.player2.playerid, "lost");

                winnerid = this.player1.playerid;
                player1score = opponentscore;
                player2score = imscore;
                break;
        }

        let player1id = this.player1.playerid;
        let player2id = this.player2.playerid;

        let params = {
            gameid,
            winnerid,
            player1id,
            player2id,
            player1score,
            player2score
        };
        return params;
    }

    _benchmarkResultForOpponent(gameid, socket, result, playernumber) {
        this.resultbenchmarked = true;

        let resultforopponent;

        switch (result) {
            case "won":
                resultforopponent = "lost";
                break;
            case "lost":
                resultforopponent = "won";
                break;
            case "draw":
                resultforopponent = result;
                break;
        }

        socket.broadcast.to(gameid).emit("benchmarkresult", resultforopponent);

        console.log(`${playernumber} send benchmark result`);
    }
}

module.exports = Game;
