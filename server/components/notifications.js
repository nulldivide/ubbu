const rp = require("request-promise-native");
const DB = require("./DBservices/DB");
const versions = require("../versionlist");

const dev = {
    client_id: "6450242",
    client_secret: "Jn4C7Ievmpe3YoC7wjaB",
    token:
        "63ddf5b963ddf5b963ddf5b9e963bf99fb663dd63ddf5b939095ccbd9d8beac644e8d55"
};

const prod = {
    client_id: "6181973",
    client_secret: "9fHqlT39dkaPAe4eP4TE",
    token:
        "9ced25d99ced25d99ced25d9069cb3718c99ced9ced25d9c55ad00b782b1e1ce056662a"
};

let app_keys = dev;
if (process.env.NODE_ENV === "production") {
    app_keys = prod;
}

class Notify {
    static async Init() {
        try {
            Notify._newInGame();
            // let ids;
            // //ids = "345559239";  //3615168, 345559239,468883742
            // let message = "Теперь в УББУ есть ачивки!";
            //
            // let top100ids = await DB.GetUsersIds(0, 99);
            //
            // console.dir(top100ids, { colors: true });
            //
            // if (ids) {
            //     Notify._send({ ids, message });
            // }
        } catch (e) {
            console.error(e);
        }
    }

    static async _newInGame() {
        // получить всех игроков
        let allUsersIds = await DB.GetUsersIds(0, -1);
        //console.log(allUsersIds);

        // let allUsersIds = [];
        //
        // console.time('15000-elements');
        // for (let i = 0; i < 220; i++) {
        //     allUsersIds.push(10000 + i)
        // }
        // console.timeEnd('15000-elements');

        //пачками по 100 отправляем сообщения
        while (allUsersIds.length) {
            let number = 100;
            if (allUsersIds.length < 100) {
                number = allUsersIds.length;
            }

            let usersToSend = allUsersIds.splice(0, number);
            let ids = usersToSend.join(",");
            let message = versions[0]["whatsnew"];
            console.log("Cообщение: " + message);
            console.log("Отправлено игрокам: " + ids);

            Notify._send({ ids, message });
        }
    }

    static _send(obj) {
        let { ids, message } = obj;

        let params = `user_ids=${ids}&message=${message}&client_secret=${
            app_keys.client_secret
        }`;
        let method_name = "secure.sendNotification";

        try {
            let url = `https://api.vk.com/method/${method_name}?${params}&access_token=${
                app_keys.token
            }&v=5.78`;
            url = encodeURI(url);
            console.log(url);
            rp(url)
                .then(result => {
                    console.log(result);
                })
                .catch(e => console.error(e));
        } catch (e) {
            console.error(e);
        }
    }
}

module.exports = Notify;
