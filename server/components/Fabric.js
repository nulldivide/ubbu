const Game = require("./Game.js");

class Fabric {
    constructor() {
        this.i = 100;
        this.activegames = {};
        this.game; //for joining second player
        this.odd = true;
        this.oddplayersocket;
        this.player1id;
    }

    Play(socket, playerid) {
        if (this.odd === true) {
            //console.log(`ODD player`);
            this.odd = false;
            this.oddplayersocket = socket;
            this.player1id = playerid;

            let playernumber = 1;
            let gameid = this.CreateGameID();
            this.game = new Game({ gameid, socket, playerid, playernumber });

            let game = this.game;
            this.activegames[gameid] = { game };

            //КОНСОЛИМ
            // console.log(`Active games:`);
            // for(let key in (this.activegames)) {
            //     console.log(key);
            // }
        } else {
            //проверка на игру с самим собой
            if (this.player1id !== playerid) {
                console.log(`EVEN player`);
                this.odd = true;
                this.oddplayersocket = "";
                this.player1id = "";

                let playernumber = 2;
                this.game.CreateGame({ socket, playerid, playernumber });
            } else {
                console.log("Player trying to play with himself 👯️");
                socket.emit("PlayJustInOneWindow");
            }
        }
    }

    CreateGameID() {
        let gameid = Date.now() + "_" + this.i;
        this.i++;
        if (this.i > 999) {
            this.i = 100;
        }
        return gameid;
    }

    KillGame(gameid, socket) {
        delete this.activegames[gameid];
        console.log(`Game ${gameid} was ❌ killed.`);

        if (this.oddplayersocket === socket) {
            this.odd = true;
            this.oddplayersocket = "";
            console.log(`Player was allone. He has gone away.`);
        }
    }
}

module.exports = Fabric;
