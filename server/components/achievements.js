const DB = require("./DBservices/DB.js");

const UniqAchs = {
    newcomer: {
        id: "newcomer",
        name: "новенький",
        link:
            "https://res.cloudinary.com/dae8pypei/image/upload/c_scale,w_100/v1528826914/newcomer.png",
        type: "onetime",
        title: "новый игрок"
    },
    "100points": {
        id: "100points",
        name: "сотен",
        link:
            "https://res.cloudinary.com/dae8pypei/image/upload/v1528378408/ubbu_achievments/16359030_1408337852541981_949174888_n.png",
        type: "onetime",
        title: "набрал 100 поинтов"
    }
};

const MultyAchs = {
    "1perday": {
        id: "1perday",
        name: "однёра",
        link:
            "https://res.cloudinary.com/dae8pypei/image/upload/c_scale,w_100/v1528797228/ubbu_achievments/1perday.png",
        type: "multi",
        title: "одна игра в день"
    },
    "3perday": {
        id: "3perday",
        name: "трёха",
        link:
            "https://res.cloudinary.com/dae8pypei/image/upload/c_scale,w_100/v1528876752/ubbu_achievments/3perday.png",
        type: "multi",
        title: "три игры за день"
    },
    "10perday": {
        id: "10perday",
        link:
            "https://res.cloudinary.com/dae8pypei/image/upload/v1527167759/ubbu_achievments/ubbu__ach--10perday.png",
        type: "multi",
        title: "10 игр в день"
    },
    number1: {
        id: "number1",
        name: "первый в УББУ",
        link:
            "https://res.cloudinary.com/dae8pypei/image/upload/c_scale,w_100/v1529056763/ubbu_achievments/number1.png",
        type: "multi",
        title: "за первое место в рейтинге УББУ"
    },
    nightplayer: {
        id: "nightplayer",
        link: "",
        type: "multi",
        title: "за ночные игры"
    },
    morningplayer: {
        id: "morningplayer",
        link: "",
        type: "multi",
        title: "за игру ранним утром"
    },
    officeplayer: {
        id: "officeplayer",
        link: "",
        type: "multi",
        title: "за игру в бизнес-тайм"
    }
};

class Achs {
    static Refresh(parameters) {
        try {
            //щлём старые ачивки
            Achs._sendAchs(parameters);

            //находим новые ачивки
            Achs._findNewAchs(parameters);
        } catch (e) {
            console.error(e);
        }
    }

    static _sendAchs(parameters) {
        let { socket, playerdata } = parameters;
        //console.dir(playerdata, { colors: true });
        let oldAchs = playerdata["achievements"];
        // string like this "10perday*3:2018-05-31,newcomer"

        //есть ачивки уже?
        if (oldAchs) {
            console.log("Have achs: " + oldAchs);

            //отправляем старые ачивки
            let oldAchsArr = Achs._build(oldAchs);
            socket.emit("oldachs", oldAchsArr);
            //console.dir(oldAchsArr, { colors: true });
        }
    }

    static _sendNewest(socket, playerid) {
        DB.GetUserData(playerid).then(playerdata => {
            Achs._sendAchs({ playerdata, socket });
        });
    }

    static _findNewAchs(parameters) {
        let { socket, playerid } = parameters;

        //проверяем, что нового можем присвоить из ачивок
        let newachs = Achs._splitAndAdd(parameters);

        //шлём клиенту новые ачивки, если они есть
        if (newachs.length) {
            //шлём сообщение про новые ачивки
            let newAchsArr = Achs._build(newachs);
            socket.emit("newachs", {
                newachs: newAchsArr
            });
            console.log("New Achs sent: " + newAchsArr);

            //отправляем обновлённый набор ачивок
            Achs._sendNewest(socket, playerid);
        }
    }

    static _splitAndAdd(parameters) {
        let { playerdata } = parameters;
        let oldAchs = playerdata["achievements"];
        let userUniqAchs = new Set();
        let userMultiAchs = [];

        if (oldAchs) {
            let oldAchsArr = playerdata["achievements"].split(",");

            //отделим uniq от multi
            for (let i = 0; i < oldAchsArr.length; i++) {
                const ach = oldAchsArr[i];
                if (~ach.indexOf("*")) {
                    userMultiAchs.push(ach);
                } else {
                    userUniqAchs.add(ach);
                }
            }
        }

        //сюда сложим всё новенькое
        let newAchs = [];

        //проверяем ЮНИК
        let newUniq = Achs._addUNIQ(userUniqAchs, parameters);
        //console.log(newUniq.length);
        if (Array.isArray(newUniq) && newUniq.length) {
            //console.log(newUniq);
            newAchs = newAchs.concat(newUniq);
        }

        //проверяем МУЛЬТИ
        let newMulti = Achs._addMULTI(userMultiAchs, parameters);
        if (Array.isArray(newMulti) && newMulti.length) {
            newAchs = newAchs.concat(newMulti);
        }

        //console.log(newAchs);
        return newAchs;
    }

    static _addUNIQ(uniqUserAchs, parameters) {
        let newUniq = [];
        for (const key in UniqAchs) {
            if (!uniqUserAchs.has(key)) {
                let canAdd = Achs._checkUniqCriteria(key, parameters);
                if (canAdd) {
                    DB.AddAchievement(parameters.playerid, key);
                    newUniq.push(key);
                    console.log(`User got "${key}"`);
                }
            }
        }
        return newUniq;
    }

    static _checkUniqCriteria(achid, parameters) {
        let canadd = false;
        let { playerdata } = parameters;

        switch (achid) {
            case "newcomer":
                canadd = true;
                break;
            case "100points":
                let points = parseInt(playerdata["points"], 10);
                if (points > 99) {
                    canadd = true;
                }
                break;
        }

        return canadd;
    }

    static _addMULTI(userMultiAchs, parameters) {
        let newMulti = [];
        let multiAchs = {};

        //у мультиачивок отделяем id от хвоста
        //console.log(userMultiAchs);
        if (userMultiAchs) {
            for (let i = 0; i < userMultiAchs.length; i++) {
                let multiAchsArr = userMultiAchs[i].split("*");
                let id = multiAchsArr[0];
                let timesAndRecieved = multiAchsArr[1];
                let [times, received] = timesAndRecieved.split(":");
                times = parseInt(times, 10);

                //пишем мультиачивку, её кол-во и дату последнего получения
                multiAchs[id] = { id, times, received };
            }
        }

        //проверяем, что можем присвоить
        //PERDAY
        let PERDAYACHS = {
            Check(multiAchs, parameters) {
                let id;
                //пишем сегодняшнюю дату для сверки
                let today = this._makeDate(Date.now());
                let lastgamedate;

                //история игрока
                let gameshistory = parameters.playerdata.history;
                //console.dir(parameters.playerdata.history, {colors: true});

                if (gameshistory) {
                    // let games = gameshistory.split(",");
                    //
                    let games = gameshistory;

                    //очистка айдищек
                    for (let i = 0; i < games.length; i++) {
                        games[i] = games[i]["gameid"].split("_")[0];
                    }

                    //находим дату предыдущей игры
                    lastgamedate = this._makeDate(games[0]);
                    console.log("Last game date: " + lastgamedate);

                    //счётчик для игр предыдущего игрового дня
                    let gamesperday = 0;

                    if (today > lastgamedate) {
                        //находим количество игр предыдущего игрового дня
                        for (let i = 0; i < games.length; i++) {
                            const dateOfGame = this._makeDate(games[i]);

                            if (lastgamedate === dateOfGame) {
                                gamesperday++;
                            } else {
                                break;
                            }
                        }

                        console.log(`${gamesperday} games/day`);

                        //присваиваем ачивку в засисимости от числа игр
                        if (gamesperday > 0 && gamesperday < 3) {
                            id = "1perday";
                        }
                        if (gamesperday > 2 && gamesperday < 5) {
                            id = "3perday";
                        }

                        //если уже присваивали для этого дня, то undefined
                        if (multiAchs[id]) {
                            if (multiAchs[id]["received"] === lastgamedate) {
                                id = undefined;
                            }
                        }
                    }
                }

                //готовим объект для возврата
                let times = 0;
                if (id) {
                    if (!multiAchs.hasOwnProperty(id)) {
                        times++;
                    } else {
                        times = multiAchs[id]["times"]++;
                    }
                }

                if (id) {
                    console.log("Присвоена: " + id);
                } else {
                    console.log(`Ачивки не присвоены`);
                }

                return { id, times, received: lastgamedate };
            },
            _makeDate(date) {
                if (typeof date !== "number") {
                    date = parseInt(date, 10);
                }
                let event = new Date(date);
                let isodate = event.toISOString();
                let isodatearr = isodate.split("T");
                //console.log(isodatearr[0]);
                return isodatearr[0];
                // 2011-01-26
            }
        };
        let perdayach = PERDAYACHS.Check(multiAchs, parameters);
        if (perdayach.id) {
            //записываем к новым ачивкам
            DB.AddMultiAch({
                playerid: parameters.playerid,
                achid: perdayach.id,
                times: perdayach.times,
                received: perdayach.received
            });
            newMulti.push(perdayach);
        }

        // всегда последняя проверка
        // # in RATING ACHS
        let RATING = {
            Check(multiAchs, parameters) {
                const rating = parameters.playerdata.rating;
                let achid;

                switch (rating) {
                    case 1:
                        achid = "number1";
                        break;
                }

                if (multiAchs.hasOwnProperty(achid)) {
                    achid = undefined;
                }

                let received = Achs._makeDate(Date.now());

                return { id: achid, times: "", received };
            }
        };
        let ratingach = RATING.Check(multiAchs, parameters);
        if (ratingach.id) {
            //пишем в базу
            DB.AddMultiAch({
                playerid: parameters.playerid,
                achid: ratingach.id,
                times: ratingach.times,
                received: ratingach.received
            });

            //добавляем к сообщению о новых ачивках
            newMulti.push(ratingach);
        }

        //возвращаем что присвоилось
        return newMulti;
    }

    static _build(achs) {
        let arrtosend = [];
        switch (typeof achs) {
            case "string":
                console.log(typeof achs);

                achs = achs.split(",");

                for (let i = 0; i < achs.length; i++) {
                    let rawAch = achs[i];
                    console.dir(rawAch, { colors: true });
                    if (~rawAch.indexOf("*")) {
                        let achIdAndMulti = rawAch.split("*");
                        let id = achIdAndMulti[0];
                        let timesAndRecieved = achIdAndMulti[1];
                        let [times, recieved] = timesAndRecieved.split(":");

                        //собираем объект мультиачивки
                        let ach = MultyAchs[id];
                        ach["times"] = times;
                        ach["received"] = recieved;
                        arrtosend.push(ach);
                    } else {
                        //для уникальных ачивок
                        const ach = UniqAchs[achs[i]];
                        arrtosend.push(ach);
                    }
                }
                break;
            case "object":
                console.log(typeof achs);

                for (let i = 0; i < achs.length; i++) {
                    let achobj = achs[i]; // like { id: '1perday', times: 1, received: '2018-06-08' }
                    if (achobj.hasOwnProperty("times")) {
                        //собираем объект мультиачивки
                        let ach = MultyAchs[achobj.id];
                        ach["times"] = achobj.times;
                        ach["received"] = achobj.recieved;
                        arrtosend.push(ach);
                    } else {
                        //для уникальных ачивок
                        const ach = UniqAchs[achobj];
                        arrtosend.push(ach);
                    }
                }
                break;
        }

        return arrtosend;
    }

    static _makeDate(date) {
        if (typeof date !== "number") {
            date = parseInt(date, 10);
        }
        let event = new Date(date);
        let isodate = event.toISOString();
        let isodatearr = isodate.split("T");
        //console.log(isodatearr[0]);
        return isodatearr[0];
        // 2011-01-26
    }
}

module.exports = Achs;
