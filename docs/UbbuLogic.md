# Модуль логики игры для nodejs сервера

## Импорт
`var ubbuLogic = require('./ubbuLogic');`

## доступные методы:

* `createGameData(gameId):GameData` создает данные новой игры. см. описание ниже.
* `createGameContext(gameData:GameData):Game` создает контекст игры. Нужен для всех вызовов логики. см. описание ниже.
* `playerJoin(game:Game, playerId:String)` присоединяет игрока к игре. Когда оба игрока присоединились стартует 1-й раунд.
* `playerMakeTurn(game:Game, playerId:String, playerPosition:PlayerPosition, time:Float)` отправляет в игру ход игрока - позиции камней и приемов. Когда оба игрока отправили свои позиции стартует расчет раунда и события сохраняются в roundData.battleEvents.
* `playerWatchedRoundResult(game:Game, playerId:String, time:Float)` сообщает игре, что игрок просмотрел результат боя. Когда оба игрока просмотрели результат, стартует следующий раунд

## Game
Game - контекст логики игры. 

### свойства
    
    config:GameConfig; - конфиг, см ниже
    data:GameData; - данные, см. ниже
    currentRound:RoundData; - данные текущего раунда
    currentRoundNum:Int; - номер текущего раунда (0-3)
    positionStageTime:Float; - время на раунд

### методы
    addEventListener - добавить слушателя. Будет получать все GameEvent. 

## GameData
GameData - обычный json объект. Может сохраняться в БД.

```
typedef GameData = {
    var id:String;
    var state:GameState;
    var player1:PlayerData;
    var player2:PlayerData;
    var rounds:Array<RoundData>;
}

@:enum
abstract RockState(String) to String {
    var start = "start";
    var attack = "attack";
    var secondAttack = "secondAttack";
    var defence = "defence";
    var atHole = "hole";
}

typedef RockData = {
    var id:Int;
    var line:Int;
    var state:RockState;
}

typedef PlayerData = {
    var id:String;
    var gimmicksSet:Array<GimmickType>;
}

typedef GameConfig = {
    var positionStageTime:Float;
    var roundsCount:Int;
}

enum BattleEvent {
    gimmickPlayed(playerId:String, gimmick:GimmickData);
    gimmickRemoved(playerId:String, gimmick:GimmickData);
    attackFailed(playerId:String, rockId:Int, line:Int);
    moveToHole(playerId:String, rockId:Int, line:Int);
    holeDefenced(playerId:String, line:Int);
    attackDroped(playerId:String, rockId:Int, line:Int);
    rockMirrored(playerId:String, rockId:Int, line:Int);
    goal(playerId:String, rockId:Int, line:Int);
    scoreChanged(playerId:String, amount:Int);
}

typedef PlayerPosition = {
    var rocks:Map<RockState, Array<Int>>;
    var gimmicks:Map<GimmickType, GimmickData>;
}

typedef RoundData = {
    var scores:Map<String, Int>;
    var endTurnTime:Map<String, Float>;
    var watchedTime:Map<String, Float>;
    var startPosition:Map<String, PlayerPosition>;
    var battleEvents:Array<BattleEvent>;
}

typedef GimmickData = {
    var type:GimmickType;
    var lines:Array<Int>;
}

@:enum
abstract GameState(Int) from Int {
    var none = 0;
    var created = 1;
    var placingRocks = 2;
    var playingBattle = 3;
    var gameEnd = 4;
}

@:enum
abstract GimmickParamsType(Int) from Int {
    var attackLine = 0;
    var defenceLine = 1;
    var notActivated = 2;
    var defenceNeighbour = 3;
}

@:enum
abstract GimmickType(String) from String to String {
    var cannon = "cannon";
    var fire = "fire";
    var shield = "shield";
    var mirror = "mirror";
    var lineChange = "switch";
    var spring = "spring";
    var second = "second";
    var vabank = "vabank";
}
```