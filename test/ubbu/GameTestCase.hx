package ubbu;

import utest.Assert;
import ubbu.Types;
import ubbu.Error;
import ubbu.Game;

using ubbu.DataHelper;

class GameTestCase {

    var data:GameData;
    var game:Game;

    public function new() {}

    public function setup() {
        data = DataBuilder.createGame("1");
        game = new Game({
            positionStageTime:10,
            roundsCount:4
        },
        data);
    }

    public function testCreateGame() {
        Assert.notNull(game);
        Assert.equals(GameState.created, game.data.state);
        Assert.equals(0, game.data.rounds.length);
    }

    public function testPlayerStates() {
        Assert.equals(0, game.data.player1.id);
        Assert.equals(0, game.data.player2.id);
    }

    public function testAllRocksAtStartPosition() {
        game.join("1");
        game.join("2");
        Assert.equals(4, ubbu.DataHelper.getLinesWithRocks(game.currentRound, "1", RockState.start).length);
        Assert.equals(4, ubbu.DataHelper.getLinesWithRocks(game.currentRound, "2", RockState.start).length);
    }

    public function testGimmicksSet() {
        Assert.equals(GimmickType.all().length, game.data.player1.gimmicksSet.length);
        Assert.equals(GimmickType.all().length, game.data.player2.gimmicksSet.length);
    }

    public function testJoin() {
        game.join("1");
        Assert.equals(GameState.created, game.data.state);
        game.join("2");
        Assert.equals(GameState.placingRocks, game.data.state);
        Assert.equals(1, game.data.rounds.length);
    }

    public function testCanMoveRock() {
        game.data.state = GameState.created;
        game.join("1");
        game.join("2");

        var attackMove = game.canMoveRock("1", 0, 0, RockState.attack);
        Assert.isTrue(attackMove);
        var defenceLeft = game.canMoveRock("1", 0, 0, RockState.defence);
        Assert.isTrue(defenceLeft);
    }

    public function testMoveRock() {
        game.data.state = GameState.created;
        game.join("1");
        game.join("2");

        Assert.isTrue(game.currentRound.hasRockAtLine("1", RockState.start, 0));
        game.moveRock("1", 0, 0, RockState.attack);
        Assert.isTrue(game.currentRound.hasRockAtLine("1", RockState.attack, 0));
 
        game.moveRock("1", 1, 1, RockState.defence);
        Assert.isTrue(game.currentRound.hasRockAtLine("1", RockState.defence, 1));
        
        game.moveRock("1", 2, 3, RockState.defence);
        Assert.isTrue(game.currentRound.hasRockAtLine("1", RockState.defence, 3));
    }

    public function testPlaceGimmicks() {
        game.data.state = GameState.created;
        var me:String = "1";
        game.join(me);
        game.join("2");

        var mePlayer = game.getPlayer(me);
        game.moveRock(me, 0, 0, RockState.attack);
        game.moveRock(me, 1, 1, RockState.defence);
        game.moveRock(me, 2, 3, RockState.defence);

        game.placeGimmick(me, GimmickType.cannon, [1]);
        var position = game.currentRound.getPlayerPosition(me);
        Assert.equals(GimmickType.all().length, mePlayer.gimmicksSet.length + Lambda.count(position.gimmicks));
        Assert.isTrue(Lambda.count(position.gimmicks) > 0);
    }

    function assertEnumSame(e1:EnumValue, e2:EnumValue) {
        Assert.equals(Type.enumConstructor(e1), Type.enumConstructor(e2));
    }
}