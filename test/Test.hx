import utest.Runner;
import utest.ui.Report;

class Test {
  public static function main() {
    var runner = new Runner();
    runner.addCase(new ubbu.GameTestCase());
    Report.create(runner);
    runner.run();
  }
}