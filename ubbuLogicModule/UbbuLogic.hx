import ubbu.Game;
import ubbu.Types;
import js.node.Http;
import js.Node.console;

@:expose @:keep
class UbbuLogic {

    var config:GameConfig;

    public function new(config:GameConfig) {
        this.config = config;
    }

    public function createGameData(gameId:String):GameData {
        return DataBuilder.createGame(gameId);
    }

    public function createGameContext(gameData:GameData):Game {
        return new Game(config, gameData);
    }

    public function playerJoin(game:Game, playerId:String) {
        game.join(playerId);
    }

    public function playerMakeTurn(game:Game, playerId:String, playerPosition:PlayerPosition, time:Float) {
        game.currentRound.startPosition[playerId] = playerPosition;
        game.endTurn(playerId, time);
    }

    public function playerWatchedRoundResult(game:Game, playerId:String, time:Float) {
        game.battleWatched(playerId, time);
    }

}