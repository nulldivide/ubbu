const path = require("path");

module.exports = {
    mode: "development",
    entry: {
        build: "./src/frontend/vkcom/js/app.js"
    },
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, "public")
    },
    watch: true,
    devtool: "source-map",
    stats: "normal",

    module: {
        rules: [
            {
                test: /\.hbs$/,
                loader: "handlebars-loader"
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|__test__)/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/preset-env"],
                        plugins: ["@babel/plugin-transform-runtime"]
                    }
                }
            }
        ]
    }
};
