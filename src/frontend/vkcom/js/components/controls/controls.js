import Component from "../component";
import CompiledControls from "./controls.hbs";
import Sound from "../../services/sound";
import Tips from "../../tips";
import { SOUND } from "../../app";

export default class Controls extends Component {
    constructor(options) {
        super(options.element);
        this._controls = options.controls;
        this._render.bind(this)();

        this.on(
            "click",
            Controls.FullScreen.bind(this),
            '[id="control_full_screen"]'
        );
        this.on("click", Controls._MuteUnmute.bind(this), '[id="control_sound"]');

        //ставим напоминание про фулскрин
        if (this._controls[1]["type"] === "full_screen") {
            let fullscreenbutton = document.getElementById(
                "control_full_screen"
            );
            Tips.ToggleWaves(fullscreenbutton);
        }

        //когда актуализирован пользователь ставим иконку
        window.addEventListener("useractualised", Controls._SetMuteStateIcon, {
            once: true
        });
    }
    _render() {
        this._element.innerHTML = CompiledControls({
            controls: this._controls
        });
    }

    static FullScreen() {
        //console.log('Fullscreen!');

        //тоглим подсказку на кнопки
        let fullscreenbutton = document.getElementById("control_full_screen");
        Tips.ToggleWaves(fullscreenbutton);

        // check if fullscreen mode is available
        if (
            document.fullscreenEnabled ||
            document.webkitFullscreenEnabled ||
            document.mozFullScreenEnabled ||
            document.msFullscreenEnabled
        ) {
            // which element will be fullscreen
            let elem = document.querySelector("#app");
            //console.log(elem);

            // Проверяем в каком режиме фулскрин
            if (
                !document.fullscreenElement && // alternative standard method
                !document.mozFullScreenElement &&
                !document.webkitFullscreenElement
            ) {
                //ВКЛ фулскрин
                if (elem.requestFullscreen) {
                    elem.requestFullscreen();
                } else if (elem.webkitRequestFullscreen) {
                    elem.webkitRequestFullscreen();
                } else if (elem.mozRequestFullScreen) {
                    elem.mozRequestFullScreen();
                } else if (elem.msRequestFullscreen) {
                    elem.msRequestFullscreen();
                }
            } else {
                //ВЫКЛ фулскрин
                if (document.cancelFullScreen) {
                    document.cancelFullScreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitCancelFullScreen) {
                    document.webkitCancelFullScreen();
                }
            }
        } else {
            document.querySelector(".error").innerHTML =
                "Your browser is not supported fullscreen";
        }
    }

    static _MuteUnmute() {
        Sound.Mute();
        Controls._SetMuteStateIcon();
    }

    static _SetMuteStateIcon() {
        let soundicon = document.getElementById("soundicon");
        switch (SOUND.soundstate) {
            case "on":
                soundicon.setAttribute("src", SOUND.unmuteImage);
                console.log("icon 🔊");
                break;
            case "off":
                soundicon.setAttribute("src", SOUND.muteImage);
                console.log("icon 🔇");
                break;
        }
    }
}
