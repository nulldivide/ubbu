import Component from "./component";
import Router from "../services/router.js";
import { USER } from "../app.js";

export default class Logo extends Component {
    constructor(options) {
        super(options.element);

        this.on("click", Logo.BackToDashboard, '[id="logo"]');
    }

    static BackToDashboard() {
        Router.ChangeLocation(USER.data.where, "dashboard");
    }
}
