export default class ResultIndicator {
    static UpdateResult(imScore, opponentScore) {
        let difference = imScore - opponentScore;
        document
            .getElementById("resultindicator")
            .setAttribute("data-score-difference", difference);
    }
}
