import Component from "../component";
import ControlsTemplate from "../controls/controls.hbs";
import { socket } from "../../services/socketservice";

export default class StadiumControls extends Component {
    constructor(options) {
        super(options.element);
        this._controls = options.controls;
        this._render.bind(this)();

        socket.on("OpponentID", () => {
            StadiumControls._setResignIcon();
        });
    }

    _render() {
        this._element.innerHTML = ControlsTemplate({
            controls: this._controls
        });
    }

    static _setExitIcon() {
        //меняем иконку выйти на сдаться
        let resignbutton = document.getElementById("resignicon");
        resignbutton.setAttribute(
            "src",
            "https://emojipedia-us.s3.amazonaws.com/thumbs/120/apple/118/door_1f6aa.png"
        );
        resignbutton.setAttribute("alt", "Выйти");
    }

    static _setResignIcon() {
        //меняем иконку выйти на сдаться
        let exitbutton = document.getElementById("resignicon");
        exitbutton.setAttribute(
            "src",
            "https://emojipedia-us.s3.amazonaws.com/thumbs/160/google/119/waving-white-flag_1f3f3.png"
        );
        exitbutton.setAttribute("alt", "Сдаться");
    }
}
