import Component from "../../component";
import StadiumCardTemplate from "./playercardonstadium.hbs";

export default class UserOnStadium extends Component {
    constructor(options) {
        super(options.element);

        this.user = options.user;
        this.identifier = options.identifier;

        // исли это оппонент, то сразу рендерим его карточку
        if (this.identifier === "opponent") {
            this._render(this.user, this.identifier);
        } else {
            window.addEventListener("useractualised", event => {
                this.kill();
                //console.log(`Catch USER from stadium card!`);
                this._render(event.detail, this.identifier);
            });
        }
    }

    _render(data, identifier) {
        this._element.innerHTML = StadiumCardTemplate({
            UserPic: data["photo_50"],
            FirstName: data["first_name"],
            Identifier: identifier
        });
    }
}
