import Component from "../component.js";
import Dashboard from "../dashboard/dashboard.js";
import StadiumControls from "./stadiumcontrols.js";
import PlayerCardOnStadium from "./PlayerCardOnStadium/playercardonstadium.js";
import Socket from "../../services/socketservice.js";
import VKdata from "../../services/VKdataService.js";
import ResultMessage from "./ResultMessage/resultmessage.js";
import ResultIndicator from "./resultindicator.js";

import StadiumTemplate from "./stadium.hbs";

import { USER, SOUND } from "../../app.js";
import { socket } from "../../services/socketservice.js";
import Popup from "../popup/popup";
import Microgame from "../microgame/microgame";

let engine;
let endgamereason;
let opponentpresent = false;
let resultwasbenchmarked;
let timeToMicrogameInvite;
let iLeaveTab = false;

let hidden, visibilityChange;
if (typeof document.hidden !== "undefined") {
    // Opera 12.10 and Firefox 18 and later support
    hidden = "hidden";
    visibilityChange = "visibilitychange";
} else if (typeof document.msHidden !== "undefined") {
    hidden = "msHidden";
    visibilityChange = "msvisibilitychange";
} else if (typeof document.webkitHidden !== "undefined") {
    hidden = "webkitHidden";
    visibilityChange = "webkitvisibilitychange";
}

export default class Stadium extends Component {
    constructor(options) {
        super(options.element);

        //рендерим сожерживое стадиона
        this._renderStadium();

        //обрабатываем нажатие на кнопку СДАТЬСЯ
        this.on("click", Stadium.Resign, '[id="control_resign"]');

        //обрабатываем сообщения об изменении счёта в игре и обновляем табло
        window.addEventListener("scoreupdate", Stadium.UpdateScore);

        //создаём заготовку для результурующего сообщения
        this.resultmessage = new ResultMessage({
            element: document.querySelector("#resultmessage")
        });
    }

    static InitGame() {
        //обнуляем контроль получения результата и причину окончания игры
        resultwasbenchmarked = false;
        endgamereason = "";

        let playerId = USER.data["id"];
        let idOfField = "field";

        //сообщаем на сервер о желании играть
        Socket.GameStarter(playerId);

        //инициализируем игровой движок
        engine = new ArenaComponent(idOfField, playerId, Stadium._scoreEmitter);
        let arenaInput = engine.endpoint.input;
        let arenaOutput = engine.endpoint.output;

        let loop = setInterval(Stadium._listenClientLogic, 200, arenaOutput);

        //проверяльщик игровых состояний
        Stadium._stateChecker(engine);

        //создаём листенеры для игровых событий
        Stadium._createGameListeners(arenaInput, loop);

        //когда найдётся соперник — отрисовываем его
        Stadium._makeOpponent();

        //предложение про микроигру
        timeToMicrogameInvite = setTimeout(Popup.Show, 2500, "microgame");
    }

    static Resign() {
        //записываем тригер завершивший игру
        endgamereason = "myresign";

        //сообщаем на сервер о сдаче
        socket.emit("RESIGN");

        if (opponentpresent) {
            //останавливаем движок
            engine.stop();
            opponentpresent = false;
        }

        //прячем стадион
        document.getElementById("stadium").hidden = true;

        //убираемся на стадионе
        Stadium.ClearStadium();

        //показываем дешборд если был скрыт
        if (document.getElementById("dashboard").hidden) {
            console.log("Dashboard is hidden. Display it!");
            Dashboard.ShowDashboard();
        }
    }

    _renderStadium() {
        this._renderPlacesForElements();
        this._renderStadiumControls();
        Stadium._renderMyCard();
        Stadium._renderOpponentCard();
    }

    _renderPlacesForElements() {
        this._element.innerHTML = StadiumTemplate();
    }

    _renderStadiumControls() {
        this._stadiumControls = new StadiumControls({
            element: document.getElementById("stadiumControls"),
            controls: [
                {
                    type: "resign",
                    imageURL:
                        "https://emojipedia-us.s3.amazonaws.com/thumbs/120/apple/118/door_1f6aa.png",
                    title: "Выйти"
                }
            ]
        });
    }

    static _renderMyCard() {
        new PlayerCardOnStadium({
            element: document.querySelector(".im"),
            user: USER.data,
            identifier: "im"
        });
    }

    static _renderOpponentCard() {
        new PlayerCardOnStadium({
            element: document.querySelector(".opponent"),
            user: { first_name: " поиск", photo_50: "" },
            identifier: "opponent"
        });
    }

    static _makeOpponent() {
        socket.on("OpponentID", opponentID => {
            opponentpresent = true;

            clearTimeout(timeToMicrogameInvite);

            VKdata.ListOfPlayers(opponentID, "photo_50").then(opponentdata => {
                let opponentName = opponentdata[0]["first_name"];
                let opponentPhoto = opponentdata[0]["photo_50"];

                document.querySelector(
                    "#opponentname"
                ).innerHTML = opponentName;
                document
                    .querySelector("#opponentpic")
                    .setAttribute("src", opponentPhoto);
            });

            Microgame.Close();
            Popup.Close();

            //в случае запуска из автоответа
            if (USER.data.autoanswer === "on") {
                //показываем стадион
                document.getElementById("stadium").hidden = false;
                //скрываем дешборд
                document.getElementById("dashboard").hidden = true;
            }

            //контроллер покидания браузера во время боя
            Stadium._listenVisibilityChange();
        });
    }

    static _listenVisibilityChange() {
        // Warn if the browser doesn't support addEventListener or the Page Visibility API
        if (
            typeof document.addEventListener === "undefined" ||
            hidden === undefined
        ) {
            console.log(
                "This demo requires a browser, such as Google Chrome or Firefox, that supports the Page Visibility API."
            );
        } else {
            // Handle page visibility change
            document.addEventListener(
                visibilityChange,
                Stadium._handleVisibilityChange,
                false
            );
        }
    }

    static _handleVisibilityChange() {
        if (document[hidden]) {
            iLeaveTab = true;
            Stadium.Resign();
        } else {
            Popup.Show("leavetab");
            Stadium.RemoveVisibilityListener();
            iLeaveTab = false;
        }
    }

    static RemoveVisibilityListener() {
        document.removeEventListener(
            visibilityChange,
            Stadium._handleVisibilityChange
        );
    }

    static _scoreEmitter(scores) {
        let dataEvent = new CustomEvent("scoreupdate", {
            bubbles: false,
            detail: scores
        });
        window.dispatchEvent(dataEvent);
    }

    static _listenClientLogic(arenaOutput) {
        if (arenaOutput.hasMessages()) {
            let m = arenaOutput.get();
            console.log("Client send message:" + JSON.stringify(m));
            socket.emit("ClientMessage", m);
        }
    }

    static _stateChecker(engine) {
        engine.stateChangedSignal.add(state => {
            switch (state) {
                case 2:
                    //start new round
                    Stadium._setRoundMark();
                    console.log(`2️⃣ new round`);
                    break;
                case 3:
                    //play battle
                    console.log(`3️⃣ battle playback`);
                    break;
                case 4:
                    //end game
                    //
                    console.log(`4️⃣ end of the game`);
                    Stadium._setMatchOverMark();
                    Stadium._EndGameMessage();
                    break;
            }
        });
    }

    static _createGameListeners(arenaInput, loop) {
        //сообщение от сервера
        socket.on("ServerMessage", m => {
            console.log("Massage from server: " + JSON.stringify(m));
            arenaInput.put(m);
        });

        //оппонент вышел из приложения
        socket.on("opponentDisconected", () => {
            endgamereason = "opponentDisconected";
            engine.stop();
            console.log("YOU HAVE WON!");
            clearInterval(loop);
            ResultMessage.Show("won");
            //Stadium._ShowResultMessage('won');
            socket.emit("wonfromclient");
            opponentpresent = false;
        });

        //оппонент сдался
        socket.on("opponentResigned", () => {
            endgamereason = "opponentResigned";
            engine.stop();
            console.log(`Victory. Because the rival surrendered.`);
            clearInterval(loop);
            ResultMessage.Show("won");
            //Stadium._ShowResultMessage('won');
            socket.emit("wonfromclient");
            opponentpresent = false;
        });

        //сообщение желающим играть с одного аккаунта самим с собой
        socket.on("PlayJustInOneWindow", () => {
            console.log("Play just in one window!");
            ResultMessage.Show("playwithyourself");
        });

        //оппонент шлёт контрольный счёт
        socket.on("benchmarkresult", result => {
            if (!resultwasbenchmarked) {
                console.log(`The benchmark result "${result}" was obtained`);
                resultwasbenchmarked = true;
                engine.stop();
            }
        });
    }

    static _setRoundMark() {
        //находим есть ли идентификатор раунда
        let checker = document.querySelector(".current");

        if (!checker) {
            document
                .querySelector(".live_round")
                .firstElementChild.classList.add("current");
        } else {
            let elem = document.querySelector(".current");
            elem.classList.remove("current");
            elem.classList.add("completed");

            elem.nextElementSibling.classList.add("current");
        }
    }

    static _setMatchOverMark() {
        document.querySelector(".live_round").classList.add("matchover");
    }

    static _EndGameMessage() {
        switch (endgamereason) {
            //если игра закончилась сдачей одной из сторон
            case "myresign":
                break;

            case "opponentDisconected":
                break;

            case "opponentResigned":
                break;

            //если игра завершилась истечением времени
            default:
                //ставим отметку, что результат получен
                resultwasbenchmarked = true;

                //получим счёт матча
                let imscore = document.querySelector("#imscore").innerHTML;
                let opponentscore = document.querySelector("#opponentscore")
                    .innerHTML;
                let result;

                //победа
                if (imscore > opponentscore) {
                    result = "won";
                } else {
                    //ничья
                    if (imscore === opponentscore) {
                        result = "draw";
                    } else {
                        //поражение
                        result = "lost";
                    }
                }

                let m = { result, imscore, opponentscore };
                socket.emit("RESULT", m);

                //КОНСОЛИМ
                console.log(`Result: ${result}`);

                ResultMessage.Show(result);

                opponentpresent = false;
        }

        endgamereason = "";
    }

    static UpdateScore(event) {
        let scores = event.detail;
        let imScore = 0;
        let opponentScore = 0;

        //суммируем счёт из массива для каждого игрока
        for (let i = 0; i < scores.length; i++) {
            imScore = imScore + scores[i][0];
            opponentScore = opponentScore + scores[i][1];
        }

        //анимируем индикатор
        ResultIndicator.UpdateResult(imScore, opponentScore);

        //консолим счёт
        console.log(`🔢 score: ${scores}`);
        console.log(`🔴 ${imScore}`);
        console.log(`🔵 ${opponentScore}`);

        //озвучиваем моё изменение в счёте
        let imScoreBefore = document.querySelector("#imscore").innerHTML;
        if (imScore - imScoreBefore > 0) {
            SOUND.myScore.play();
        }

        //озвучиваем изменение в счёте опонента
        let opponentScoreBefore = document.querySelector("#opponentscore")
            .innerHTML;
        if (opponentScore - opponentScoreBefore > 0) {
            SOUND.opponentScore.play();
        }

        //монтируем счёт на табло
        document.querySelector("#imscore").innerHTML = imScore;
        document.querySelector("#opponentscore").innerHTML = opponentScore;
    }

    static ClearStadium() {
        //Чистим листенеры с завершонной игры
        Stadium._clearSocketListeners();

        //обнуляем индикатор раунда
        Stadium._clearRoundMark();

        //чистим карточку оппонента
        Stadium._renderOpponentCard();

        //меняем иконку «сдаться» на «выход»
        StadiumControls._setExitIcon();

        if (!iLeaveTab) {
            //листенер покидания вкладки
            Stadium.RemoveVisibilityListener();
        }
    }
    static _clearSocketListeners() {
        let listenerstokill = [
            "ServerMessage",
            "opponentDisconected",
            "opponentResigned",
            "PlayJustInOneWindow"
        ];

        console.log(`Unsubscribed from listeners:`);

        for (let i = 0; i < listenerstokill.length; i++) {
            socket.off(listenerstokill[i]);
            console.log(`"${listenerstokill[i]}"`);
        }
    }
    static _clearRoundMark() {
        let indicators = document.querySelectorAll(".round");
        for (let i = 0; i < indicators.length; i++) {
            indicators[i].classList.remove("current", "completed");
        }

        document.querySelector(".live_round").classList.remove("matchover");
    }
}
