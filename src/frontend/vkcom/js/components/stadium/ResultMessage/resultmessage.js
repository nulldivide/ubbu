import Component from "../../component.js";
import Dashboard from "../../dashboard/dashboard.js";
import Stadium from "../stadium.js";
import Sound from "../../../services/sound";
import HttpService from "../../../services/http";

import VictoryMessageTemplate from "./victorymessage.hbs";
import LostMessageTemplate from "./lostmessage.hbs";
import DrawMessageTemplate from "./drawmessage.hbs";
import PlayWithYourselfTemplate from "./playwithyourself.hbs";

import { socket } from "../../../services/socketservice";

let stickers;

export default class ResultMessage extends Component {
    constructor(options) {
        super(options.element);

        this.on("click", this._playagaine.bind(this), '[id="playmore"]');
        this.on(
            "click",
            this._choseSticker.bind(this),
            '[class="post_match__stickers__catalog"]'
        );

        this.on("click", this._closeMessage.bind(this), '[id="closemessage"]');
        this.on("click", this._closeMessage.bind(this), '[id="nothanks"]');

        this.on(
            "click",
            ResultMessage._reloadApp.bind(this),
            '[id="reloadapp"]'
        );

        //запрашиваем стикеры с сервера
        ResultMessage._getStickers().then(stickS => {
            stickers = ResultMessage._shuffleStickers(stickS);
        });
    }

    static Show(result) {
        let template;
        let element = document.querySelector("#resultmessage");
        element.hidden = false;

        switch (result) {
            case "lost":
                template = LostMessageTemplate();
                break;
            case "draw":
                template = DrawMessageTemplate();
                break;
            case "won":
                template = VictoryMessageTemplate({ stickers });
                break;
            case "playwithyourself":
                template = PlayWithYourselfTemplate();
        }
        Sound.PlayResultSound(result);
        element.innerHTML = template;
    }

    _closeMessage() {
        //удаляем сообщение
        this.kill();

        //прячем стадион
        document.querySelector("#stadium").hidden = true;

        //убираемся на стадионе
        Stadium.ClearStadium();

        //показываем дешборд
        Dashboard.ShowDashboard();
    }

    _playagaine() {
        //убиваем сообщение о победе
        this.kill();

        //убираемся на стадионе
        Stadium.ClearStadium();

        Dashboard.Play();
    }

    static _reloadApp() {
        location.reload();
    }

    _choseSticker(event) {
        if (event.target.src) {
            //отправляем URL стикера на сервер
            let stickerURL = event.target.src;
            socket.emit("sticker", stickerURL);

            //убиваем сообщение о победе
            this.kill();

            //прячем стадион
            document.querySelector("#stadium").hidden = true;

            //убираемся на стадионе
            Stadium.ClearStadium();

            //показываем дешборд
            Dashboard.ShowDashboard();
        } else {
            console.log("This is not a sticker");
        }
    }

    static _getStickers() {
        return HttpService.SendRequest("/public/stickers.json");
    }

    static _shuffleStickers(stickers) {
        //взять исходный массив
        let arr = stickers;
        let newarr = [];
        for (let i = 0; i < arr.length; ) {
            let index = Math.floor(Math.random() * arr.length);
            newarr.push(arr[index]);
            arr.splice(index, 1);
        }
        //console.log(newarr);
        return newarr;
    }
}
