import Component from "../component.js";
import { USER } from "../../app.js";
import TutorialTemplate from "./tutorial.hbs";
import Router from "../../services/router.js";
import SocketServise from "../../services/socketservice";

export default class Tutorial extends Component {
    constructor(options) {
        super(options.element);
        this.on("click", Tutorial.BackToDashboard, '[id="close_tutorial"]');
    }

    static _render() {
        console.log("Rerender tutor");

        let elem = document.querySelector("#tutorial");
        elem.innerHTML = TutorialTemplate();
        Tutorial.CatchTutorEnd();
    }

    static Show() {
        console.log("Show tutorial");
        Tutorial._render();
        document.getElementById("tutorial").hidden = false;
        USER.data.where = "tutorial";

        //ставим белый лого
        document.querySelector(".logo").classList.add("white");

        //записываем в карточку, что пользователь смотрел
        SocketServise.SetValue("newcommer", "1");

        Tutorial.AutoClose();
    }

    static Close() {
        //прячем тютор
        let elem = document.querySelector("#tutorial");
        elem.classList.add("end");
        console.log("fade tutorial");

        //и удаляем содержимое
        setTimeout(() => {
            Router.ChangeLocation("tutorial", "dashboard");
            elem.innerHTML = "";
            elem.classList.remove("end");
        }, 2500);
    }

    static CatchTutorEnd() {
        let elem = document.querySelector("#tutorendtrigger");

        elem.addEventListener(
            "onend",
            () => {
                console.log("Animation finished");
            },
            false
        );
    }

    static AutoClose() {
        document.querySelector("#tutorialGimmick").addEventListener(
            "click",
            () => {
                setTimeout(Tutorial.Close, 9500);
            },
            false
        );
    }

    static BackToDashboard() {
        Router.ChangeLocation(USER.data.where, "dashboard");
    }
}
