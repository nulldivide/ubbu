"use strict";

import Component from "../../component";
import CompiledTemplate from "./playercard.hbs";

export default class PlayerCard extends Component {
    constructor(options) {
        super(options.element);

        window.addEventListener("useractualised", event => {
            this._render(event.detail);
        });
    }

    _render(data) {
        this._element.setAttribute("data-id", data["id"]);
        //console.log(this._element);

        let pointsdifference = 0;
        let ratingdifference;

        if (this._element.querySelector(".user_points")) {
            ratingdifference = this._ratingDifference.bind(this)(data);
            pointsdifference = this._pointsDifference.bind(this)(data);
        }

        this._element.innerHTML = CompiledTemplate({
            UserPic: data["photo_50"],
            FirstName: data["first_name"],
            City: data["city"]["title"],
            Points: data["points"],
            PointsDiff: pointsdifference,
            Rating: data["rating"],
            RatingDiff: ratingdifference
        });

        //чистим разницы в рейтинге и поинтах
        setTimeout(PlayerCard._clearPointsAndRatingDiffs, 1500);
    }

    _ratingDifference(data) {
        let elem = this._element.querySelector(".user_rating");
        let prevrating = elem.innerHTML;
        let difference = prevrating - data["rating"];
        let setvalue;
        if (difference > 0) {
            setvalue = "+" + difference;
        } else {
            if (difference !== 0) {
                setvalue = difference;
            } else {
                setvalue = "";
            }
        }
        return setvalue;
    }

    _pointsDifference(data) {
        let previousPoints = this._element.querySelector(".user_points")
            .firstElementChild.innerHTML;
        let pointsdifference = data["points"] - previousPoints;
        if (pointsdifference) {
            pointsdifference = "+" + pointsdifference;
        } else {
            pointsdifference = "";
        }
        return pointsdifference;
    }

    static _clearPointsAndRatingDiffs() {
        document.getElementById("mypoints").dataset.pointsEmerging = "";
        document.getElementById("myrating").dataset.ratingEmering = "";
    }
}
