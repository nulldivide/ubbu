import Component from "../../component.js";
import CompiledMenu from "./menu.hbs";
import Tips from "../../../tips.js";
import { USER } from "../../../app";
import Router from "../../../services/router.js";
import Rules from "../../rules/rules.js";

export default class Menu extends Component {
    constructor(options) {
        super(options.element);
        this._rander.bind(this)();

        // this._poll = new Poll({
        //     element: document.getElementById("poll")
        // });

        this._rules = new Rules({
            element: document.getElementById("rules")
        });

        this.on("click", Menu.SwitchPage, '[class = "menu"]');

        window.addEventListener("useractualised", event => {
            Menu.AccentOnPoll(event.detail);
        });
    }
    _rander() {
        this._element.innerHTML = CompiledMenu();
    }

    static SwitchPage(event) {
        if (event.target.id !== "menu") {
            let oldlocation = USER.data.where;
            let buttonId = event.target.id;
            let newlocation = buttonId.slice(6);
            Router.ChangeLocation(oldlocation, newlocation);
        }
    }

    static AccentOnPoll(serverdata) {
        let history = serverdata.history;
        if (history !== undefined) {
            if (history.length > 4) {
                //пользователь просмотрривал опрос?
                //console.log(USER.data["checkpoll"]);

                if (USER.data["checkpoll"] !== "yes") {
                    let pollbutton = document.querySelector("#buttonpoll");
                    if (pollbutton) {
                        Tips.OnWaves(pollbutton);
                    }
                }
            }
        }
    }
}
