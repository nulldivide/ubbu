import Component from "../../component";
import Stadium from "../../stadium/stadium.js";
import PlayButton from "../playbutton.js";

import Template from "./autoanswer.hbs";

import { USER } from "../../../app";

export default class AutoAnswer extends Component {
    constructor(options) {
        super(options.element);
        this._render.bind(this)();

        USER.data.autoanswer = "off";

        this.on("click", AutoAnswer.ToggleAutoAnswer, '[id="autoAnswer"]');
    }

    _render() {
        this._element.innerHTML = Template();
    }

    static ToggleAutoAnswer() {
        let elem = document.getElementById("autoAnswer");
        elem.classList.toggle("on");

        let value;
        if (elem.classList.contains("on")) {
            value = "on";

            //выключаем напоминание
            PlayButton.RemoveReminder();

            //запускаем поиск опонента
            Stadium.InitGame();
        } else {
            value = "off";

            //ВКЛ напоминание
            PlayButton.SetReminder();

            //выключаем поиск опонента
            Stadium.Resign();
        }

        USER.data.autoanswer = value;
    }

    //выключает положеные кнопки и записываем в объект
    static Reset() {
        let elem = document.getElementById("autoAnswer");
        elem.classList.remove("on");
        USER.data.autoanswer = "off";
    }

    static Off() {
        AutoAnswer.Reset();
        Stadium.Resign();
    }

    static ShowMessageAboutMicrogame() {}
}
