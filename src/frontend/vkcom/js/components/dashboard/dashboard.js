import Component from "../component.js";
import Menu from "./menu/menu.js";
import PlayerCard from "./playercard/playercard.js";
import RatingColumn from "./columns/ratingcolumn.js";
import FriendsColumn from "./columns/friendscolumn.js";
import HistoryColumn from "./columns/historycolumn.js";
import Stadium from "../stadium/stadium.js";
import Controls from "../controls/controls";
import AutoAnswer from "./autoanswer/autoanswer.js";
import Rating from "../../rating.js";
import User from "../../user.js";
import PlayButton from "./playbutton";
import { USER } from "../../app.js";
import Microgame from "../microgame/microgame";
import Template from "./dashboard.hbs";
import Popup from "../popup/popup";
import Achs from "./columns/achievements";

export default class Dashboard extends Component {
    constructor(options) {
        super(options.element);

        this._render.bind(this)();

        this._mycard = new PlayerCard({
            element: document.getElementById("mycard")
        });

        this._menu = new Menu({ element: document.getElementById("menu") });

        this._autoanswer = new AutoAnswer({
            element: document.getElementById("autoAnswer")
        });

        this.controls = new Controls({
            element: document.getElementById("globalControls"),
            controls: [
                {
                    title: "Звук вкл/выкл",
                    imageURL:
                        "https://emojipedia-us.s3.amazonaws.com/thumbs/160/twitter/120/speaker-with-one-sound-wave_1f509.png",
                    type: "sound"
                },
                {
                    title: "Фулскрин",
                    imageURL:
                        "https://emojipedia-us.s3.amazonaws.com/thumbs/160/apple/118/desktop-computer_1f5a5.png",
                    type: "full_screen"
                }
            ]
        });

        this._friendsColumn = new FriendsColumn({
            element: document.getElementById("friendsColumn"),
            title: "Друзья"
        });

        this._historyColumn = new HistoryColumn({
            element: document.getElementById("myGamesColumn"),
            title: "Моя история"
        });

        this._ratingColumn = new RatingColumn({
            element: document.getElementById("topPlayersColumn"),
            title: "Рейтинг"
        });

        this._achievementsColumn = new Achs({
            element: document.getElementById("achievementsColumn")
        });

        this.stadium = new Stadium({
            element: document.getElementById("stadium")
        });

        this.popup = new Popup({
            element: document.getElementById("msg")
        });

        this.microgame = new Microgame({
            element: document.getElementById("microgame")
        });

        this.on("click", Dashboard.Play.bind(this), '[id="playButton"]');

        //включаем напоминальщик
        PlayButton.SetReminder();

        //прописываем стейт
        USER.data.where = "dashboard";
    }

    _render() {
        this._element.innerHTML = Template();
    }

    static Play() {
        //проверяем не включён ли автоответ
        if (USER.data.autoanswer === "off") {
            Stadium.InitGame();
        }

        //показываем стадион
        this.stadium.show();

        //ставим стейт
        USER.data.where = "stadium";

        //прячем дешборд
        this.hide();

        //сбрасываем кнопку автоответа
        AutoAnswer.Reset();
    }

    static ShowDashboard() {
        //актуализируем данные для дешборда
        User.Actualize();
        Rating.Actualize();

        //выключаем автоответ
        AutoAnswer.Reset();

        //прописываем стейт
        USER.data.where = "dashboard";

        //показываем дешборд
        document.getElementById("dashboard").hidden = false;
    }
}
