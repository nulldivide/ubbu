export default class PlayButton {
    static SetReminder() {
        let timefordelay = 30000; //ms
        setTimeout(() => {
            document
                .getElementById("playButton")
                .classList.add("attention_marker");
        }, timefordelay);
    }

    static RemoveReminder() {
        document
            .getElementById("playButton")
            .classList.remove("attention_marker");
    }
}
