let options = {
    //year: 'numeric',
    month: "long",
    day: "numeric"
};

let lastsetteddate;

module.exports = function(milliseconds) {
    let datetoreturn;

    //находим сегодня
    let today = new Date();
    today = today.toLocaleString("ru", options);

    //находим вчера
    let yesterday = new Date();
    yesterday = yesterday.setDate(yesterday.getDate() - 1);
    yesterday = new Date(yesterday);
    yesterday = yesterday.toLocaleString("ru", options);

    //форматируем полученную дату к формату
    let date = new Date(milliseconds);
    date = date.toLocaleString("ru", options);

    //последняя установленная дата совпадает с полученной?
    if (lastsetteddate !== date) {
        datetoreturn = date;
        lastsetteddate = date;

        //console.log(datetoreturn);

        switch (datetoreturn) {
            case yesterday:
                datetoreturn = "вчера";
                break;
            case today:
                datetoreturn = "";
                break;
        }
    } else {
        datetoreturn = "";
    }

    return datetoreturn;
};
