let options = {
    hour: "numeric",
    minute: "numeric"
};

module.exports = function(miliseconds) {
    let date = new Date(miliseconds);
    return date.toLocaleString("ru", options);
};
