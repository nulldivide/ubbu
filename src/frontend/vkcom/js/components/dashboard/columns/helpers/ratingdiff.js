module.exports = function(ratingafter, ratingbefore) {
    let different = ratingbefore - ratingafter;
    let result;
    if (different >= 0) {
        result = "+" + different;
    } else {
        result = different;
    }
    return result;
};
