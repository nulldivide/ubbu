import Component from "../../component";

import CompiledColumn from "./column.hbs";
import RatingTemplate from "./rating.hbs";

export default class RatingColumn extends Component {
    constructor(options) {
        super(options.element);
        this._title = options.title;
        this._render.bind(this)();

        window.addEventListener("ratingactualised", event => {
            this._renderRatingColumn(event.detail);
        });
    }

    _render() {
        this._element.innerHTML = CompiledColumn({ Title: this._title });
    }

    _renderRatingColumn(rating) {
        this._element.querySelector(
            ".column__content"
        ).innerHTML = RatingTemplate({ arr: rating });
    }
}
