import Component from "../../component.js";
import VKdata from "../../../services/VKdataService.js";

import CompiledColumn from "./column.hbs";
import HistoryTemplate from "./history.hbs";
import NoGamesTemplate from "./NoGames.hbs";

import { USER } from "../../../app.js";

export default class HistoryColumn extends Component {
    constructor(options) {
        super(options.element);
        this._title = options.title;
        this._render.bind(this)();

        window.addEventListener("useractualised", event => {
            this.MakeHistoryColumn(event.detail);
        });
    }

    _render() {
        this._element.innerHTML = CompiledColumn({
            Title: this._title,
            Indicator: true
        });
    }

    MakeHistoryColumn(serverdata) {
        let history = serverdata.history; //array with objs
        // console.log(`HISTORY`);
        // console.log(history);

        //если есть игры
        if (history) {
            let myphoto = USER.photo;
            let myfirst_name = USER.name;

            let opponents = [];

            //get all opponents
            for (let i = 0; i < history.length; i++) {
                opponents.push(history[i]["opponent"]["id"]);
            }

            //get opponents data from VK
            let fields = "photo_50";
            let opponentids = opponents.toString();
            //console.log(`Opponent ids: ${opponentids}`);

            VKdata.ListOfPlayers(opponentids, fields).then(opponentsdata => {
                // opponentsdata is [{},{}...]

                //console.log(opponentsdata);

                //дописываем данные из VK в историю игр
                for (let i = 0; i < history.length; i++) {
                    let opponentid = history[i]["opponent"]["id"];
                    for (let k = 0; k < opponentsdata.length; k++) {
                        if (opponentid === opponentsdata[k]["id"]) {
                            history[i]["opponent"]["photo_50"] =
                                opponentsdata[k]["photo_50"];
                            history[i]["opponent"]["first_name"] =
                                opponentsdata[k]["first_name"];

                            history[i]["me"]["photo_50"] = myphoto;
                            history[i]["me"]["first_name"] = myfirst_name;

                            //записываем результат матча для игрока
                            if (opponentid === history[i]["winnerid"]) {
                                //если выиграл оппонент
                                history[i]["resultForMe"] = "_l";
                                //delete history[i]['winnerid'];
                            } else {
                                //если выйграл игрок
                                if (history[i]["winnerid"]) {
                                    history[i]["resultForMe"] = "_w";
                                    //delete history[i]['winnerid'];
                                }
                            }
                        }
                    }
                }

                //находим процент поражений и ставим значение на страницу
                HistoryColumn._setLossesWinsBar(history);

                //rendercolumn
                this._renderHistoryColumn(history);
            });
        } else {
            //если у пользователя нет истории игр
            this._renderNoGames();
        }
    }

    _renderHistoryColumn(history) {
        this._element.querySelector(
            ".column__content"
        ).innerHTML = HistoryTemplate({ arr: history });
    }

    static _setLossesWinsBar(history) {
        let losses = 0;
        let wins = 0;

        function CountProcentOfLosses(history) {
            let lossesinprocent;

            for (let i = 0; i < history.length; i++) {
                if (history[i]["resultForMe"]) {
                    switch (history[i]["resultForMe"]) {
                        case "_w":
                            wins++;
                            break;
                        case "_l":
                            losses++;
                            break;
                    }
                }
            }

            lossesinprocent = Math.round(losses * 100 / (losses + wins));

            if (lossesinprocent === Infinity) {
                lossesinprocent = 50;
            }

            return lossesinprocent;
        }

        //выставляем индикатор
        let procentofloses = CountProcentOfLosses(history);
        let stylestrtoset = "transform: translateX(" + procentofloses + "%);";
        let elem = document.querySelector(".column__line__indicator_bar");
        elem.setAttribute("style", stylestrtoset);

        //добавляем title на индикатор
        let procentOfWinsForTitle =
            "Побед " + (100 - Math.round(procentofloses * 100) / 100) + "%";
        elem.parentElement.setAttribute("title", procentOfWinsForTitle);
        console.log(`🏆 ${procentOfWinsForTitle}`);

        //выставляем число побед и поражений
        document
            .getElementById("historyIndicator")
            .setAttribute("data-win", wins);
        document
            .getElementById("historyIndicator")
            .setAttribute("data-lose", losses);
    }

    _renderNoGames() {
        this._element.querySelector(
            ".column__content"
        ).innerHTML = NoGamesTemplate();
    }
}
