import Component from "../../component";
import AchievementsTemplate from "./achievements.hbs";
import { socket } from "../../../services/socketservice.js";
import Popup from "../../popup/popup";

export default class Achs extends Component {
    constructor(options) {
        super(options.element);

        //рендерим колонку с ачивками
        socket.on("oldachs", m => {
            let achsWithPos = Achs._addPositions(m);
            this._render.bind(this)(achsWithPos);
        });

        //сообщаем о новых ачивках
        socket.on("newachs", m => {
            Popup.Show("newachs", m.newachs);
        });
    }

    _render(data) {
        this._element.innerHTML = AchievementsTemplate({
            arr: data
        });
    }

    static _addPositions(achs) {
        let achsWithPos = [];
        for (let i = 0; i < achs.length; i++) {
            let ach = achs[i];

            //генерим позиции элементов
            // const maxX = 100;
            // const maxY = 20;
            // const maxAngle = 7;

            // let x = Math.floor(Math.random() * maxX);
            // let y = Math.floor(Math.random() * maxY);
            // let rotate = Math.floor(Math.random() * maxAngle) + 1;
            // rotate *= Math.floor(Math.random() * 2) === 1 ? 1 : -1;

            ach["position"] = { x: 70, y: 0, rotate: 0 };

            achsWithPos.push(ach);
        }
        return achsWithPos;
    }
}
