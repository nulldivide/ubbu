import Component from "../../component";
import VKdata from "../../../services/VKdataService";

import CompiledColumn from "./column.hbs";
import FriendsTemplate from "./friends.hbs";

export default class FriendsColumn extends Component {
    constructor(options) {
        super(options.element);
        this._title = options.title;
        this._render.bind(this)();

        window.addEventListener("ratingactualised", event => {
            this.MakeFriendsColumn(event.detail);
        });

        this.on("click", VKdata.InviteFriends, '[id="invitefriends"]');
    }

    _render() {
        this._element.innerHTML = CompiledColumn({ Title: this._title });
    }

    async MakeFriendsColumn(data) {
        try {
            let raitinglist = data;
            let friends = await VKdata.FriendsInApp();
            let friendsCardsData = []; // это пойдёт в рендеринг

            if (friends) {
                //находим каждого друга в рейтинг листе и откладываем его карточку
                for (let i = 0; i < friends.length; i++) {
                    let target = friends[i];

                    for (let key in raitinglist) {
                        if (raitinglist[key]["id"] === target) {
                            friendsCardsData.push(raitinglist[key]);
                        }
                    }
                }
            }

            //отсортируем по рейтингу
            function sortmethod(a, b) {
                return a["rating"] - b["rating"];
            }
            friendsCardsData.sort(sortmethod);

            //КОНСОЛИМ
            //console.log(friendsCardsData);

            //рендерим колонку
            this._renderFriendsColumn(friendsCardsData);
        } catch (e) {
            console.log(e);
        }
    }

    _renderFriendsColumn(friends) {
        this._element.querySelector(
            ".column__content"
        ).innerHTML = FriendsTemplate({ arr: friends });
    }
}
