import Component from "../component";
import Template from "./microgame.hbs";
import RatingTemplate from "./microgamerating.hbs";
import PilgrimsTemplate from "./pilgrims.hbs";
import { USER } from "../../app";
import Router from "../../services/router";
import { socket } from "../../services/socketservice";
import VKdata from "../../services/VKdataService";

let blueInterval;
let redInterval;
let red = "#ee9999";
let blue = "#8fc9f9";

export default class Microgame extends Component {
    constructor(options) {
        super(options.element);
        this._render.bind(this)();

        socket.emit("getpushechkarating");
        socket.on("pushechkarating", m => {
            Microgame._renderRating(m);
        });

        this.on("click", Microgame.Close, '[id="close_microgame"]');
    }

    _render() {
        this._element.innerHTML = Template();
    }

    static async _renderRating(m) {
        try {
            let data = await Microgame.PrepareRating(m);
            document.getElementById(
                "microgame_rating"
            ).innerHTML = RatingTemplate({
                arr: data.rating
            });

            if (data.pilgrims) {
                document.getElementById(
                    "microgame_pilgrims"
                ).innerHTML = PilgrimsTemplate({
                    arr: data.pilgrims
                });
            }
        } catch (e) {
            console.log(e);
        }
    }

    static Start() {
        let s = Snap("#microgamesvg");
        let gameElem = document.getElementById("microgame");

        blueInterval = setInterval(
            Microgame.CreateCircle,
            1200,
            s,
            gameElem,
            blue
        );

        setTimeout(() => {
            redInterval = setInterval(
                Microgame.CreateCircle,
                1500,
                s,
                gameElem,
                red
            );
        }, 24000);
    }

    static CreateCircle(s, gameElem, color) {
        //console.log(gameElem.offsetWidth + " x " + gameElem.offsetHeight);

        let gameWidth = gameElem.offsetWidth;
        let gameHight = gameElem.offsetHeight;
        let radius = 20;

        let startCX = Math.floor(Math.random() * gameWidth);
        let paddingCX = 200;
        let playingWidth = gameWidth - 2 * paddingCX;

        if (startCX < paddingCX) {
            startCX = paddingCX + Math.floor(Math.random() * playingWidth);
        }
        if (startCX > gameWidth - paddingCX) {
            startCX =
                gameWidth -
                paddingCX -
                Math.floor(Math.random() * playingWidth);
        }

        let minTime = 12000;
        let maxTime = 24000;
        let timeOfMove = Math.floor(Math.random() * maxTime);
        if (timeOfMove < minTime) {
            timeOfMove = minTime;
        }
        let circle = s.circle(startCX, -radius, radius);
        circle.attr({ fill: color });
        circle.addClass("mgcircle");
        circle.animate({ cy: gameHight }, timeOfMove, () => circle.remove());
        circle.click(() => {
            circle.animate({ r: 40, "fill-opacity": 0.0 }, 200, () =>
                Microgame.CountScore(circle, color)
            );
        });
    }

    static CountScore(circle, color) {
        circle.remove();

        let scoreplace = document.getElementById("microgamescore");
        let score = parseInt(scoreplace.innerHTML, 10);
        let incr;
        switch (color) {
            case blue:
                incr = 100;
                break;
            case red:
                incr = -1000;
                break;
        }

        score = score + incr;

        if (USER.data["id"]) {
            socket.emit("setpushechkarating", {
                playerid: USER.data["id"],
                incr
            });
            socket.emit("getpushechkarating");
        }

        scoreplace.innerHTML = score;
    }

    static Stop() {
        clearInterval(blueInterval);
        clearInterval(redInterval);
    }

    static BackToDashboard() {
        Router.ChangeLocation(USER.data.where, "dashboard");
    }

    static Close() {
        Microgame.Stop();
        document.getElementById("microgame").hidden = true;
    }

    static async PrepareRating(obj) {
        let players = obj;
        let idsInArr = [];
        let lastpossitiveindex = undefined;
        let pilgrimsids = [];

        for (let key in players) {
            let id = players[key]["id"];
            idsInArr.push(id);

            //выхватываем рейтинг пользователя в микроигре
            if (id == USER.id) {
                USER.data.pushechka_points = players[key]["points"];
            }

            if (players[key]["points"] < 0) {
                if (lastpossitiveindex === undefined) {
                    lastpossitiveindex = key - 1;
                }

                //находим пилигримов
                if (players[key]["points"] < -100000) {
                    pilgrimsids.push(id);
                }
            }
        }

        let fields = "photo_50";
        let top10ids = idsInArr.slice(0, lastpossitiveindex);

        //console.log(top10ids);

        let dataArr;
        if (top10ids.length) {
            //запрашиваем в VK данные пользователей для рейтинга Пушечки
            dataArr = await VKdata.ListOfPlayers(top10ids, fields);
            //console.log(dataArr);
            for (let i = 0; i < dataArr.length; i++) {
                dataArr[i]["rating"] = i + 1;
                dataArr[i]["points"] = players[i + 1]["points"];
            }
        }

        let pilgrims = await VKdata.ListOfPlayers(pilgrimsids, fields);

        return { rating: dataArr, pilgrims: pilgrims };
    }
}
