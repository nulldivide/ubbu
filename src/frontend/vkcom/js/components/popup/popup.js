import Component from "../component";
import Router from "../../services/router";
import PreMicrogameTemplate from "./premicrogame.hbs";
import LeaveTabTemplate from "./leavetab.hbs";
import NewAchsTemplate from "./newachs.hbs";

export default class Popup extends Component {
    constructor(options) {
        super(options.element);
        this.on("click", Popup.Action, '[id="actionbutton"]');
        this.on("click", Popup.Close, '[id="closepremicromessage"]');
    }

    static Show(messagetype, data) {
        let template;

        switch (messagetype) {
            case "microgame":
                template = PreMicrogameTemplate;
                break;
            case "leavetab":
                template = LeaveTabTemplate;
                break;
            case "newachs":
                template = NewAchsTemplate;
                break;
        }

        Popup._render(template, data);

        setTimeout(Popup.Close, 10000);
    }

    static _render(template, data) {
        if (data) {
            document.getElementById("msg").innerHTML = template({
                arr: data
            });
        } else {
            document.getElementById("msg").innerHTML = template();
        }
    }

    static Action() {
        console.log("Popup action");
        //закрываем попап
        Popup.Close();
        //запускаем микроигру
        Router.ShowNewLocation("microgame");
    }

    static Close() {
        console.log("Close Popup");
        document.getElementById("msg").innerHTML = "";
    }
}
