export default class Component {
    constructor(element) {
        this._element = element;
    }

    hide() {
        this._element.hidden = true;
    }

    show() {
        this._element.hidden = false;
    }

    kill() {
        this._element.innerHTML = "";
    }

    on(eventName, callback, selector) {
        this._element.addEventListener(eventName, event => {
            if (selector && !event.target.closest(selector)) {
                //console.log('Clicked target does not have any callback.');
                return;
            }
            callback(event);
        });
    }
}
