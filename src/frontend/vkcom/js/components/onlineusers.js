import { socket } from "../services/socketservice";
import Component from "./component.js";

export default class OnlineUsers extends Component {
    constructor(options) {
        super(options.element);
        this._ShowOnlineUsers.bind(this)();
    }

    _ShowOnlineUsers() {
        socket.on("onlineusers", m => {
            console.log(m + " 👥");
            this._element.innerHTML = m;
        });
    }
}
