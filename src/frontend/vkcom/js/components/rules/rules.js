import Component from "../component.js";
import Template from "./rules.hbs";
import Router from "../../services/router";
import { USER } from "../../app";

export default class Rules extends Component {
    constructor(options) {
        super(options.element);
        this._render.bind(this)();
        this.on("click", Rules.BackToDashboard, '[id="close_rules"]');
    }

    _render() {
        this._element.innerHTML = Template();
    }

    static BackToDashboard() {
        Router.ChangeLocation(USER.data.where, "dashboard");
    }
}
