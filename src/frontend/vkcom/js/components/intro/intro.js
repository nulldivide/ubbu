import Component from "../component.js";
import IntroTemplate from "./intro.hbs";
import Tutorial from "../tutorial/tutorial.js";
import { socket } from "../../services/socketservice";

export default class Intro extends Component {
    constructor(options) {
        super(options.element);
        Intro._render();

        socket.on("userdata", m => {
            //для новеньких показываем тюториал
            if (!m.newcommer) {
                Tutorial.Show();
            }
            setTimeout(Intro.Close, 2500);
        });
    }

    static _render() {
        document.getElementById("intro").innerHTML = IntroTemplate();
    }

    static Close() {
        //прячем и чистим интро
        let intro = document.getElementById("intro");
        intro.hidden = true;
    }
}
