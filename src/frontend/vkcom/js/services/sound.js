import { SOUND, USER } from "../app";
import { socket } from "./socketservice";
import Intro from "../components/intro/intro.js";

export default class Sound {
    constructor() {
        this.muteImage =
            "https://emojipedia-us.s3.amazonaws.com/thumbs/120/twitter/131/speaker-with-cancellation-stroke_1f507.png";
        this.unmuteImage =
            "https://emojipedia-us.s3.amazonaws.com/thumbs/160/twitter/120/speaker-with-one-sound-wave_1f509.png";
        this.soundstate = "on";

        // this.intro = new Howl({
        //     src: ["/public/sounds/intro/intro.mp3"],
        //     preload: true,
        //     volume: 0.5,
        //     onload: () => {
        //         console.log("Intro sound ready to play️️");
        //         window.addEventListener(
        //             "useractualised",
        //             () => {
        //                 console.log("Intro playing️️");
        //                 Intro.Start();
        //             },
        //             { once: true }
        //         );
        //     },
        //     onend: () => {
        //         console.log("Intro sound finished️️");
        //         Intro.Close();
        //     }
        // });

        this.countdownmain = new Howl({
            src: [
                "/public/sounds/game/countdown/main/96640__cgeffex__sci-fi-beep-03.mp3"
            ],
            preload: true,
            volume: 1.0,
            onplay: () => {
                console.log("✴️️");
            }
        });

        this.countdownlast = new Howl({
            src: [
                "/public/sounds/game/countdown/last/96639__cgeffex__sci-fi-beep-01.mp3"
            ],
            preload: true,
            volume: 1.0,
            onplay: () => {
                console.log("⏱️");
            }
        });
        window.addEventListener("startcounter", () => Sound._tickInterval());

        this.hoverongimmickbutton = new Howl({
            src: [
                "/public/sounds/game/hovergimmickbutton/255764__andreas-mustola__mouse-hover.mp3"
            ],
            preload: true,
            volume: 1.0,
            onplay: () => {
                console.log("👆️️");
            }
        });

        this.myScore = new Howl({
            src: [
                "/public/sounds/game/score/my/406084__gris1azura__bubblebeam.mp3"
            ],
            preload: true,
            volume: 0.5,
            onplay: () => {
                console.log("🔴️");
            }
        });
        this.opponentScore = new Howl({
            src: [
                "/public/sounds/game/score/opponent/174700__paespedro__sci-fi-throw-and-pop.mp3"
            ],
            preload: true,
            volume: 0.5,
            onplay: () => {
                console.log("🔵️");
            }
        });

        this.holeDefenced = new Howl({
            src: ["/public/sounds/game/holedefenced/77956__sugu14__fcanto.mp3"],
            preload: true,
            volume: 1.0,
            onplay: () => {
                console.log("⛔");
            }
        });
        window.addEventListener("holeDefenced", () => this.holeDefenced.play());

        this.rockMirrored = new Howl({
            src: [
                "/public/sounds/game/gimmicks/mirror/266168__plasterbrain__shooting-star-2.mp3"
            ],
            preload: true,
            volume: 1.0,
            onplay: () => {
                console.log("↔️");
            }
        });
        window.addEventListener("rockMirrored", () => this.rockMirrored.play());

        this.cannon = new Howl({
            src: [
                "/public/sounds/game/gimmicks/cannon/196914__dpoggioli__laser-gun.mp3"
            ],
            preload: true,
            volume: 0.5,
            onplay: () => {
                console.log("💣");
            }
        });
        window.addEventListener("cannon", () => this.cannon.play());

        this.shield = new Howl({
            src: [
                "/public/sounds/game/gimmicks/shield/223628__ctcollab__shield-slam-2.mp3"
            ],
            preload: true,
            volume: 0.5,
            onplay: () => {
                console.log("🛡");
            }
        });
        window.addEventListener("shield", () => this.shield.play());

        this.fire = new Howl({
            src: [
                "/public/sounds/game/gimmicks/fire/269623__jaimage__vent-whoosh-slow-big-flame-01.mp3"
            ],
            preload: true,
            volume: 0.5,
            onplay: () => {
                console.log("️🔥");
            }
        });
        window.addEventListener("fire", () => this.fire.play());

        this.vabank = new Howl({
            src: [
                "/public/sounds/game/gimmicks/vabank/44724__vajrapani__vajrapani-laser-fab-lab02.mp3"
            ],
            preload: true,
            volume: 0.5,
            onplay: () => {
                console.log("❇️");
            }
        });
        window.addEventListener("vabank", () => this.vabank.play());

        this.spring = new Howl({
            src: [
                "/public/sounds/game/gimmicks/spring/342688__michael-kur95__spring-01.mp3"
            ],
            preload: true,
            volume: 0.5,
            onplay: () => {
                console.log("️➰");
            }
        });
        window.addEventListener("spring", () => this.spring.play());

        this.second = new Howl({
            src: [
                "/public/sounds/game/gimmicks/second/220173__gameaudio__spacey-1up-power-up.mp3"
            ],
            preload: true,
            volume: 0.5,
            onplay: () => {
                console.log("️⏸");
            }
        });
        window.addEventListener("second", () => this.second.play());

        //Результаты матча
        this.lost = new Howl({
            src: [
                "/public/sounds/game/result/lost/158649__johnnypanic__quickpowerchord3.mp3"
            ],
            preload: true,
            volume: 0.5,
            onplay: () => {
                console.log("️😫");
            }
        });
        this.draw = new Howl({
            src: [
                "/public/sounds/game/result/draw/322895__rhodesmas__disconnected-01.mp3"
            ],
            preload: true,
            volume: 0.5,
            onplay: () => {
                console.log("🤝️");
            }
        });
        this.won = new Howl({
            src: ["/public/sounds/game/result/won/267528__syseq__good.mp3"],
            preload: true,
            volume: 0.5,
            onplay: () => {
                console.log("🏆");
            }
        });

        //когда актуализирован пользователь устанавливаем его MuteState
        window.addEventListener(
            "useractualised",
            event => {
                //console.log(event.detail);
                SOUND.soundstate = event.detail.sound;
                console.log(SOUND.soundstate);
                Sound._SetPlayerMuteState();

                this._HoverGimmickButton();
            },
            { once: true }
        );
    }

    static _tickInterval() {
        let i = 1;
        setTimeout(function tick() {
            switch (i) {
                case 3:
                    SOUND.countdownlast.play();
                    break;
                default:
                    SOUND.countdownmain.play();
                    i++;
                    if (i < 4) {
                        setTimeout(tick, 720);
                    }
            }
        }, 600);
    }

    _HoverGimmickButton() {
        let gimmicksPanel = document.getElementById("gimmicksPanel");
        gimmicksPanel.addEventListener("mouseover", event => {
            console.log(event.target.id);
            let targetid = event.target.id;
            let classforchecking = "." + targetid;
            let elem = document.querySelector(classforchecking);
            if (gimmicksPanel.classList.contains("sleep")) {
                console.log("Pannel sleeped");
                return;
            }
            if (elem.classList.contains("used")) {
                console.log("Gimmick already used");
                return;
            }
            SOUND.hoverongimmickbutton.play();
        });
    }

    static _SetPlayerMuteState() {
        switch (SOUND.soundstate) {
            case "on":
                Howler.volume(1.0);
                break;
            case "off":
                Howler.volume(0.0);
                break;
            default:
                Howler.volume(1.0);
                break;
        }
    }

    static Mute() {
        console.log(`Mutestate before: ${SOUND.soundstate}`);

        switch (SOUND.soundstate) {
            case "on":
                SOUND.soundstate = "off";
                console.log("🔊 to 🔇");
                Howler.volume(0.0);
                break;
            case "off":
                SOUND.soundstate = "on";
                console.log("🔇 to 🔊");
                Howler.volume(1.0);
                break;
            default:
                SOUND.soundstate = "off";
                console.log("DEFAULT to 🔇");
                Howler.volume(0.0);
                break;
        }

        console.log(`Mutestate after: ${SOUND.soundstate}`);

        //сообщаем в БД о выборе пользователя
        let playerId = USER.data["id"];
        let value = SOUND.soundstate;
        let key = "sound";
        socket.emit("setvalue", { playerId, key, value });
    }

    static PlayResultSound(result) {
        switch (result) {
            case "lost":
                SOUND.lost.play();
                break;
            case "draw":
                SOUND.draw.play();
                break;
            case "won":
                SOUND.won.play();
                break;
            default:
                SOUND.draw.play();
                break;
        }
    }

    static _ChooseTrack() {
        let arr = [
            "/public/sounds/108-supermayer_-_two_of_us_(geiger_mix).mp3",
            "/public/sounds/betamaxx__redlining_6th.mp3"
        ];

        let i = Math.floor(Math.random() * arr.length);
        //console.log("I: " + i);
        return arr[i];
    }
}
