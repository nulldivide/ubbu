export default class VKdata {
    static CurrentPlayer() {
        return new Promise((resolve, reject) => {
            VK.api("users.get", { fields: "city, photo_50" }, data => {
                resolve(data.response[0]);
            });
        }).catch(e => console.log(e));
    }

    static ListOfPlayers(ids, fields) {
        return new Promise((resolve, reject) => {
            VK.api("users.get", { user_ids: ids, fields: fields }, data => {
                resolve(data.response);
            });
        }).catch(e => console.log(e));
    }

    static FriendsInApp() {
        return new Promise((resolve, reject) => {
            VK.api("friends.getAppUsers", data => {
                resolve(data.response);
            });
        }).catch(e => console.log(e));
    }

    static InviteFriends() {
        VK.callMethod("showInviteBox");
    }
}
