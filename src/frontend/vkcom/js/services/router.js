import Stadium from "../components/stadium/stadium";
import AutoAnswer from "../components/dashboard/autoanswer/autoanswer";
import { USER } from "../app";
import SocketServise from "./socketservice";
import Tips from "../tips";
import Tutorial from "../components/tutorial/tutorial.js";
import Microgame from "../components/microgame/microgame";

export default class Router {
    static ChangeLocation(oldlocation, newlocation) {
        console.log(`🔃 from ${oldlocation} to ${newlocation}`);
        Router.HideOldLocation(oldlocation, newlocation);
        Router.ShowNewLocation(newlocation);
    }

    static HideOldLocation(oldlocation, newlocation) {
        //особые правила для локаций
        switch (oldlocation) {
            case "dashboard":
                //выключаем автоответ
                if (USER.data.autoanswer === "on") {
                    AutoAnswer.Off();
                }
                break;
            case "stadium":
                Stadium.Resign();
                break;
            case "poll":
                //ставим тёмный лого
                document.querySelector(".logo").classList.remove("white");
                break;
            case "microgame":
                Microgame.Stop();
                break;
            default:
                break;
        }
        if (newlocation !== "microgame") {
            //прячем предыдущую страницу
            document.getElementById(oldlocation).hidden = true;
        }
    }

    static ShowNewLocation(newlocation) {
        USER.data.where = newlocation;
        switch (newlocation) {
            case "poll":
                //убираем подсказку с кнопки
                let pollbutton = document.querySelector("#buttonpoll");
                Tips.OffWaves(pollbutton);

                //эмитим, что польователь зашёл в опрос
                SocketServise.SetValue("checkpoll", "yes");

                //ставим белый лого
                document.getElementById("logo").classList.add("white");
                break;
            case "tutorial":
                Tutorial.Show();
                break;
            case "microgame":
                Microgame.Start();
                break;
            default:
                //ставим тёмный лого
                document.getElementById("logo").classList.remove("white");
                break;
        }

        document.getElementById(newlocation).hidden = false;
    }
}
