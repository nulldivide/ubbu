export default class HttpService {
    static SendRequest(url, { method = "GET" } = {}) {
        return fetch(url, { method })
            .then(response => response.json())
            .catch(error => {
                console.error(error);

                return Promise.reject(error);
            });
    }
}
