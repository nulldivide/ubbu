import { USER } from "../app";

const socket = io();

//при разрыве сокета перезагружаем страницу
// socket.on('disconnect', () => {
//     console.log('Socket disconnected. Autoreloading...');
//     setTimeout(() => {
//         location.reload()
//     }, 3500);
// });

export default class SocketServise {
    static Shuttle(params) {
        return new Promise((resolve, reject) => {
            socket.emit("shuttleToServer", params);
            socket.on("suttleToClient", data => {
                resolve(data);
            })
        }).catch(e => {
            console.error(e);
        });
    }

    static GameStarter(playerid) {
        socket.emit("PLAY", playerid);
    }

    static SetValue(key, value) {
        let playerId = USER.data["id"];
        socket.emit("setvalue", { playerId, key, value });
    }
}

export { socket };
