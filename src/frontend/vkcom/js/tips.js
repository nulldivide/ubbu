export default class Tips {
    static ToggleWaves(elem) {
        elem.classList.toggle("attention_marker");
    }

    static OffWaves(elem) {
        elem.classList.remove("attention_marker");
    }

    static OnWaves(elem) {
        elem.classList.add("attention_marker");
    }
}
