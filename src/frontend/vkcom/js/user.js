import VKdata from "./services/VKdataService";
import { USER } from "./app";
import { socket } from "./services/socketservice.js";

export default class User {
    constructor() {
        this.data = {};
        this.id = "";
        this.name = "";
        this.city = "";
        this.photo = "";

        this.gamedata = "";

        this._tellAboutActualization.bind(this)();

        this.BuildUserData.bind(this)();
    }

    BuildUserData() {
        User.Recognize()
            .then(VKdataObj => {
                this.id = VKdataObj["id"];
                this.name = VKdataObj["first_name"];
                this.photo = VKdataObj["photo_50"];

                User.Actualize();

                if (!VKdataObj["city"]) {
                    VKdataObj["city"] = {};
                    VKdataObj["city"]["title"] = "&#127757";
                }

                Object.assign(this.data, VKdataObj);

                // console.log(this.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    static async Recognize() {
        try {
            return await VKdata.CurrentPlayer();
        } catch (e) {
            console.log(e);
        }
    }

    static Actualize() {
        //запращиваем данные из базы
        let userid = USER.id;

        //если есть айдишка, то актуализируем
        if (userid) {
            socket.emit("actualizeuser", userid);
        }
    }

    _tellAboutActualization() {
        let dataEvent = new CustomEvent("useractualised", {
            bubbles: false,
            detail: this.data
        });
        socket.on("userdata", DBdata => {
            // console.log('User actual DATA from server');
            // console.log(DBdata);

            USER.gamedata = DBdata;

            Object.assign(USER.data, DBdata);

            window.dispatchEvent(dataEvent);
        });
    }
}
