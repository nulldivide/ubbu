import Socket from "./services/socketservice";
import VKdata from "./services/VKdataService";
import { RATING } from "./app";

export default class Rating {
    constructor() {
        this.ratinglist = [];
        this.pushechkarating = [];

        Rating.Actualize();
    }

    static Actualize() {
        let rating = {};
        let idsInArr = [];

        Socket.Shuttle("raitinglist")
            //вытягиваем айдишки в массив из объекта
            .then(obj => {
                rating = obj;

                for (let key in obj) {
                    let id = obj[key]["id"];
                    idsInArr.push(id);
                }

                return idsInArr.toString();
            })
            .then(ids => {
                let fields = "city, photo_50";

                //запрашиваем в VK данные пользователей для рейтинг-листа
                VKdata.ListOfPlayers(ids, fields)
                    //конвертируем данные
                    .then(dataArr => {
                        for (let i = 0; i < dataArr.length; i++) {
                            dataArr[i]["rating"] = i + 1;
                            dataArr[i]["points"] = rating[i + 1]["points"];

                            //если город не указан, то ставим глобус
                            if (!dataArr[i]["city"]) {
                                dataArr[i]["city"] = {};
                                dataArr[i]["city"]["title"] = "&#127757";
                            }
                        }

                        //записываем данные в класс
                        RATING.ratinglist = dataArr;

                        //сообщаем подписчикам об актуализации рейтинга
                        let dataEvent = new CustomEvent("ratingactualised", {
                            bubbles: false,
                            detail: RATING.ratinglist
                        });

                        window.dispatchEvent(dataEvent);
                        //console.log("Rating dispetched");
                    });
            })
            .catch(e => {
                console.error(e);
            });
    }
}
