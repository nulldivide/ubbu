import Dashboard from "./components/dashboard/dashboard.js";
import User from "./user.js";
import Rating from "./rating";
import Sound from "./services/sound.js";
import Logo from "./components/logo.js";
import Intro from "./components/intro/intro";
import Tutorial from "./components/tutorial/tutorial";
import OnlineUsers from "./components/onlineusers.js";

export let VKinition = VK.init(
    function() {
        console.log("VK inited");
    },
    function() {
        console.log("VK Error");
        location.reload();
    },
    "5.78"
);

new Intro({ element: document.getElementById("intro") });
new Tutorial({ element: document.getElementById("tutorial") });
new OnlineUsers({ element: document.getElementById("onlineUsers") });

let LOGO = new Logo({ element: document.querySelector(".logo") });
let USER = new User();
let RATING = new Rating();
let SOUND = new Sound();

new Dashboard({
    element: document.getElementById("dashboard")
});

export { LOGO, USER, RATING, SOUND };
