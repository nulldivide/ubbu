import ubbu.Game;
import ubbu.Types;
import view.IView;
import view.ArenaView;
import snap.Snap;
import messaging.Endpoint;
import messaging.Message;
import haxe.Timer;
import msignal.Signal;

@:expose
class ArenaComponent {

    public var endpoint(default, null):Endpoint;
    public var stateChangedSignal:Signal1<GameState>;

    var game:Game;
    var arenaView:ArenaView;
    var snap:Snap;
    var timer:Timer;
    var playerId:String;
    var scoreRenderer:Array<Array<Int>> -> Void;

    public function new(layoutId:String = "arena", playerId:String, scoreRenderer:Array<Array<Int>> -> Void) {
        this.playerId = playerId;
        this.scoreRenderer = scoreRenderer;
        this.stateChangedSignal = new Signal1<GameState>();
        snap = new Snap('#$layoutId');
        endpoint = new Endpoint();

        timer = new Timer(0);
        timer.run = checkMessages;
    }

    public function stop() {
        timer.stop();
        game.stop();
    }

    function checkMessages() {
        if (endpoint.input.hasMessages()) {
            var message = endpoint.input.get();
            processMessage(message);
        }
    }

    function processMessage<T>(message:Message<T>) {
        switch (message.type) {

            case Messages.CONNECTED:
                var data:ConnectedPayload = message.payload;
                var gameData:GameData = haxe.Unserializer.run(data.serializedGameData);
                
                game = new Game(data.gameConfig, gameData);
                game.addEventListener(onGameEvent);
                arenaView = new ArenaView(game, playerId);
                arenaView.renderScore = scoreRenderer;
                game.join(playerId);
                arenaView.show(snap);

            case Messages.PLAYER_JOIN:
                var playerId:String = message.payload;
                game.join(playerId);

            case Messages.ROCK_MOVED:
                var p:RockMovedPayload = message.payload;
                game.moveRock(p.playerId, p.from, p.to, p.state);

            case Messages.GIMMICK_PLACED:
                var p:GimmickPlacedPayload = message.payload;
                game.placeGimmick(p.playerId, p.gimmick.type, p.gimmick.lines);

            case Messages.PLAYER_END_TURN:
                var playerId:String = message.payload;
                game.endTurn(playerId, message.time);

            case Messages.PLAYER_WATCHED:
                var playerId:String = message.payload;
                game.battleWatched(playerId, message.time);

            default:
        }
    }

    
    function initViews(game:Game) {
        //ArenaView.ID => new ArenaView(game, connection.myId)
    }

    function onGameEvent(event:GameEvent) {
        var m:Message<Any> = switch (event) {
            case rockMoved(playerId, from, to, state) if (playerId == this.playerId):
                cast Messages.rockMoved(playerId, from, to, state);

            case playerJoin(playerId) if (playerId == this.playerId):
                cast Messages.playerJoin(playerId);

            case gimmickPlaced(playerId, gimmick) if (playerId == this.playerId):
                cast Messages.gimmickPlaced(playerId, gimmick);

            case playerEndTurn(playerId) if (playerId == this.playerId):
                cast Messages.playerEndTurn(playerId);

            case playerWatched(playerId) if (playerId == this.playerId):
                cast Messages.playerWatched(playerId);

            default:
                null;
        }
        if (m != null)
            endpoint.output.put(m);

        switch (event) {
            case stateChanged(from, to):
                stateChangedSignal.dispatch(to);
            default:
        }
    }
}
