package ubbu;

@:enum
abstract RockState(String) to String {
    var start = "start";
    var attack = "attack";
    var secondAttack = "secondAttack";
    var defence = "defence";
    var atHole = "hole";
}

typedef RockData = {
    var id:Int;
    var line:Int;
    var state:RockState;
}

typedef PlayerData = {
    var id:String;
    var gimmicksSet:Array<GimmickType>;
}

typedef GameConfig = {
    var positionStageTime:Float;
    var roundsCount:Int;
}

typedef GameData = {
    var id:String;
    var state:GameState;
    var player1:PlayerData;
    var player2:PlayerData;
    var rounds:Array<RoundData>;
}

enum BattleEvent {
    gimmickPlayed(playerId:String, gimmick:GimmickData);
    gimmickRemoved(playerId:String, gimmick:GimmickData);
    attackFailed(playerId:String, rockId:Int, line:Int);
    moveToHole(playerId:String, rockId:Int, line:Int);
    holeDefenced(playerId:String, line:Int);
    attackDroped(playerId:String, rockId:Int, line:Int);
    rockMirrored(playerId:String, rockId:Int, line:Int);
    goal(playerId:String, rockId:Int, line:Int);
    scoreChanged(playerId:String, amount:Int);
}

typedef PlayerPosition = {
    var rocks:Map<RockState, Array<Int>>;
    var gimmicks:Map<GimmickType, GimmickData>;
}

typedef RoundData = {
    var scores:Map<String, Int>;
    var endTurnTime:Map<String, Float>;
    var watchedTime:Map<String, Float>;
    var startPosition:Map<String, PlayerPosition>;
    var battleEvents:Array<BattleEvent>;
}

typedef GimmickData = {
    var type:GimmickType;
    var lines:Array<Int>;
}

@:enum
abstract GameState(Int) from Int {
    var none = 0;
    var created = 1;
    var placingRocks = 2;
    var playingBattle = 3;
    var gameEnd = 4;
}

@:enum
abstract GimmickParamsType(Int) from Int {
    var attackLine = 0;
    var defenceLine = 1;
    var notActivated = 2;
    var defenceNeighbour = 3;

    public function placingOnYourLine():Bool {
        return switch (this) {
            case defenceNeighbour | defenceLine:
                false;
            default:
                true;
        }
    }
}

@:enum
abstract GimmickType(String) from String to String {
    var cannon = "cannon";
    var fire = "fire";
    var shield = "shield";
    var mirror = "mirror";
    var lineChange = "switch";
    var spring = "spring";
    var second = "second";
    var vabank = "vabank";

    public static function all():Array<GimmickType> {
        return AbstractEnumTools.getValues(GimmickType);
    }

    public function isOnePass():Bool {
        return switch (this) {
            case lineChange, fire:
                false;
            default:
                true;
        }
    }

    public function paramTypes():Array<GimmickParamsType> {
        return switch (this) {
            case cannon: [defenceLine];
            case second: [notActivated];
            case fire: [defenceLine];
            case shield: [attackLine];
            case mirror: [attackLine];
            case lineChange: [defenceLine, defenceNeighbour];
            case spring: [attackLine];
            default: [];
        }
    }
}

class DataBuilder {
    static inline var rocksCount:Int = 4;

    public static function createGame(id:String):GameData {
        return {
            id:id,
            state:GameState.none,
            player1:createPlayer(),
            player2:createPlayer(),
            rounds:[]
        };
    }

    public static function createPlayer():PlayerData {
        return {
            id:"",
            gimmicksSet:GimmickType.all()
        }
    }

    public static function createRock(id:Int):RockData {
        return {
            id:id,  
            line:id,
            state:RockState.start
        };
    }

    public static function createRound(data:GameData):RoundData {
        var player1Id = data.player1.id;
        var player2Id = data.player2.id;
        return {
            scores:[player1Id => 0, player2Id => 0],
            endTurnTime:[player1Id => 0, player2Id => 0],
            watchedTime:[player1Id => 0, player2Id => 0],
            startPosition:[player1Id => createPlayerPosition(), player2Id => createPlayerPosition()],
            battleEvents:[]
        };
    }

    public static function createPlayerPosition():PlayerPosition {
        return {
            rocks:[
                RockState.start => [for (i in 0...Game.LINES_COUNT) i],
                RockState.attack => [for (i in 0...Game.LINES_COUNT) -1],
                RockState.defence => [for (i in 0...Game.LINES_COUNT) -1],
                RockState.secondAttack => [for (i in 0...Game.LINES_COUNT) -1]
            ],
            gimmicks: new Map<GimmickType, GimmickData>()
        };
    }
}
