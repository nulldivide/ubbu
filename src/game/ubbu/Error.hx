package ubbu;

import ubbu.Types;

class Error {
    public var message(default, null):String;
    public function new(message:String) {
        this.message = message;
    }
}

class WrongInputError extends Error {

}

class RockAlreadyMoved extends WrongInputError {

    public function new(rockId:Int) {
        super('rock already moved: $rockId');
    }
}

class GimmickNotInSet extends WrongInputError {

    public function new(type:GimmickType) {
        super('no gimmick in set: $type');
    }
}

class PlayerNotFound extends Error {

    public function new(playerId:String) {
        super('player not found: $playerId');
    }
}

class RockNotFound extends Error {

    public function new(rockId:Int) {
        super('rock not found: $rockId');
    }
}
