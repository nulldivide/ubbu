package ubbu;

import ubbu.Types;
import ubbu.Game;

class DataHelper {
    
    public static function getPlayerPosition(roundData:RoundData, playerId:String):PlayerPosition {
        return roundData.startPosition[playerId];
    }

    public static function getLinesWithRocks(roundData:RoundData, playerId:String, state:RockState):Array<Int> {
        var rocksGrid = getPlayerPosition(roundData, playerId).rocks;
        var lines = rocksGrid.get(state);
        return [for (i in 0...lines.length) if (lines[i] >= 0) i];
    }

    public static function getRockId(roundData:RoundData, playerId:String, state:RockState, line:Int):Int {
        var rocksGrid = getPlayerPosition(roundData, playerId).rocks;
        var lines = rocksGrid.get(state);
        if (lines[line] < 0) {
            throw "no rock at line";
        }
        return lines[line];
    }

    public static function getWatchedTime(roundData:RoundData, playerId:String):Float {
        return roundData.watchedTime[playerId];
    }

    public static function hasRockAtLine(roundData:RoundData, playerId:String, state:RockState, line:Int):Bool {
        return (getLinesWithRocks(roundData, playerId, state).indexOf(line) >= 0);
    }

    public static function isGimmickPlayed(roundData:RoundData, playerId:String, type:GimmickType):Bool {
        return getPlayerPosition(roundData, playerId).gimmicks.exists(type);
    }

    public static function getGimmick(roundData:RoundData, playerId:String, type:GimmickType):GimmickData {
        return getPlayerPosition(roundData, playerId).gimmicks.get(type);
    }

    public static function hasGimmickAtLine(roundData:RoundData, playerId:String, type:GimmickType, line:Int):Bool {
        if (isGimmickPlayed(roundData, playerId, type)) {
            var gimmick = getGimmick(roundData, playerId, type);
            return (line == gimmick.lines[0]);
        }
        return false;
    }

    public static function allPlayersDone(roundData:RoundData):Bool {
        for (t in roundData.endTurnTime) {
            if (t <= 0) {
                return false;
            }
        }
        return true;
    }

    public static function allPlayersWatched(roundData:RoundData):Bool {
        for (t in roundData.watchedTime) {
            if (t <= 0) {
                return false;
            }
        }
        return true;
    }

    public static function getOppositeLine(line:Int):Int {
        var maxLine = Game.LINES_COUNT;
        return maxLine - line - 1;
    }
}
