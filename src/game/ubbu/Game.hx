package ubbu;

import ubbu.Types;
import ubbu.Error;
import ubbu.BattleCalculator;

using ubbu.DataHelper;

enum GameEvent {
    playerJoin(playerId:String);
    stateChanged(prev:GameState, curr:GameState);
    rockMoved(playerId:String, from:Int, to:Int, state:RockState);
    gimmickPlaced(playerId:String, gimmick:GimmickData);
    playerEndTurn(playerId:String);
    playerWatched(playerId:String);
}

class Game {

    public static inline var LINES_COUNT:Int = 4;
    
    public var config(default, null):GameConfig;
    public var data(default, null):GameData;
    public var currentRound(get, null):RoundData;
    public var currentRoundNum(get, null):Int;
    public var positionStageTime(get, null):Float;
    var eventListeners:Array<GameEvent -> Void>;

    public function new(config:GameConfig, data:GameData) {
        this.data = data;
        this.config = config;
        eventListeners = [];
        if (data.state == GameState.none) {
            enterCreated();
        }
    }

    public function addEventListener(listener:GameEvent -> Void) {
        eventListeners.push(listener);
    }

    public function getPlayer(id:String) {
        if (data.player1.id == id) {
            return data.player1;
        }
        else if (data.player2.id == id) {
            return data.player2;
        }
        else {
            throw new PlayerNotFound(id);
        }
    }

    public function getPlayerIds():Array<String> {
        var result = [];
        if (data.player1.id != "") {
            result.push(data.player1.id);
        }

        if (data.player2.id != "") {
            result.push(data.player2.id);
        }
        return result;
    }

    public function getEnemyId(myId:String):String {
        if (data.player1.id == myId) {
            return data.player2.id;
        }
        else {
            return data.player1.id;
        }
    }

    public function join(id:String) {
        var empty = getPlayer("");
        empty.id = id;
        dispatchEvent(playerJoin(id));

        if (readyToGo()) {
            enterPlacingRocks();
        }
    }

    public function battleWatched(id:String, time:Float) {
        currentRound.watchedTime[id] = time;
        dispatchEvent(playerWatched(id));
        if (allPlayersWhatched()) {
            if (currentRoundNum < config.roundsCount - 1) {
                enterPlacingRocks();
            }
            else {
                enterGameEnd();
            }
        }
    }

    public function stop() {
        enterGameEnd();
    }

    public function hasAnyMoves(playerId:String):Bool {
        var atStartPosition = currentRound.getLinesWithRocks(playerId, RockState.start);
        return (atStartPosition.length > 1);
    }

    public function hasDefenceAtLine(playerId, line:Int):Bool {
        return currentRound.hasRockAtLine(playerId, RockState.defence, line);
    }

    public function canMoveRock(playerId:String, id:Int, line:Int, state:RockState):Bool {
        var rockAtStart = currentRound.hasRockAtLine(playerId, RockState.start, id);
        var lineIsCorrect = (line >= 0) && (line < LINES_COUNT);
        var movesAvailable = [
            RockState.attack => 1,
            RockState.defence => 2
        ];
        var hasMoves = (currentRound.getLinesWithRocks(playerId, state).length < movesAvailable[state]);
        if ((state == RockState.start) && (id != line)) {
            lineIsCorrect = false;
        }
        if (state == RockState.defence) {
            if ((id != line) && (id + 1 != line)) {
                lineIsCorrect = false;
            }
            else if (currentRound.hasRockAtLine(playerId, RockState.defence, line)) {
                lineIsCorrect = false;
            }
        }
        return rockAtStart && lineIsCorrect && hasMoves;
    }

    public function moveRock(playerId:String, id:Int, line:Int, state:RockState) {
        var position = currentRound.getPlayerPosition(playerId);
        position.rocks[RockState.start][id] = -1;
        position.rocks[state][line] = id;
        dispatchEvent(rockMoved(playerId, id, line, state));
    }

    public function placeGimmick(playerId:String, type:GimmickType, params:Array<Int>) {
        var player = getPlayer(playerId);
        if (!player.gimmicksSet.remove(type)) {
            throw new GimmickNotInSet(type);
        }
        var gimmick:GimmickData = {type:type, lines:params};
        dispatchEvent(gimmickPlaced(playerId, gimmick));
        if (gimmick.type == GimmickType.second) {
            var line = gimmick.lines[0];
            checkRockNotMovedYet(playerId, line);
            moveRock(playerId, line, line, RockState.secondAttack);
        }
        var position = currentRound.getPlayerPosition(playerId);
        position.gimmicks.set(type, gimmick);
    }

    public function endTurn(playerId:String, time:Float):Void {
        if (currentRound.endTurnTime[playerId] > 0) {
            throw "turn already ended";
        }
        currentRound.endTurnTime[playerId] = time;
        dispatchEvent(playerEndTurn(playerId));
        if (allPlayersDone()) {
            enterPlayingBattle();
        }
    }

    function dispatchEvent(event:GameEvent) {
        for (listener in eventListeners) {
            listener(event);
        }
    }

    function enterCreated() {
        changeStateTo(created);
    }

    function enterPlacingRocks() {
        data.rounds.push(DataBuilder.createRound(data));
        changeStateTo(placingRocks);
    }

    function enterPlayingBattle() {
        var battleCalculator = new BattleCalculator(currentRound, getPlayerIds());
        currentRound.battleEvents = battleCalculator.calculate();
        changeStateTo(playingBattle);
    }

    function enterGameEnd() {
        changeStateTo(gameEnd);
    }

    function changeStateTo(state:GameState) {
        var prev = data.state;
        data.state = state;
        dispatchEvent(stateChanged(prev, state));
    }

    function readyToGo():Bool {
        return ((data.player1.id != "") && (data.player2.id != ""));
    }

    function allPlayersDone():Bool {
        return currentRound.allPlayersDone();
    }

    function allPlayersWhatched():Bool {
        return currentRound.allPlayersWatched();
    }

    function checkRockNotMovedYet(playerId:String, line:Int) {
        var startPosition = currentRound.getLinesWithRocks(playerId, RockState.start);
        if (startPosition.indexOf(line) < 0) {
             throw new RockAlreadyMoved(line);
        }
    }

    function get_positionStageTime() {
        return config.positionStageTime;
    }

    function get_currentRoundNum():Int {
        return data.rounds.length - 1;
    }

    function get_currentRound():RoundData {
        return data.rounds[currentRoundNum];
    }
}
