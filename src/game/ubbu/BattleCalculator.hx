package ubbu;

import ubbu.Types;
using ubbu.DataHelper;

class BattleCalculator {

    var roundData:RoundData;
    var calcOrder:Array<String>;
    var events:Array<BattleEvent>;
    
    public function new(data:RoundData, calcOrder:Array<String>) {
        this.calcOrder = calcOrder;
        this.roundData = clone(data);
    }

    public function calculate():Array<BattleEvent> {
        events = [];
        for (playerId in calcOrder) {
            calcSecondAttack(playerId);
            calcMainAttack(playerId);
        }
        
        return events;
    }

    function calcSecondAttack(playerId:String) {
        if (roundData.isGimmickPlayed(playerId, GimmickType.second)) {
            var gimmick:GimmickData = roundData.getGimmick(playerId, GimmickType.second);
            playGimmick(playerId, gimmick);
            var line:Int = gimmick.lines[0];
            calcAttack(playerId, line, false);
        }
    }

    function calcMainAttack(playerId:String) {
        var lines = roundData.getLinesWithRocks(playerId, RockState.attack);
        if (lines.length == 1) {
            calcAttack(playerId, lines[0], true);
        }
    }

    function calcAttack(playerId:String, line:Int, isMain:Bool) {
        var enemyId:String = getEnemyId(playerId);
        var rockId = getAttackingRock(playerId, line, isMain);
        
        if (roundData.hasGimmickAtLine(enemyId, GimmickType.cannon, line)) {
            playGimmick(enemyId, roundData.getGimmick(enemyId, GimmickType.cannon));
            if (roundData.hasGimmickAtLine(playerId, GimmickType.shield, line)) {
                playGimmick(playerId, roundData.getGimmick(playerId, GimmickType.shield));
                calcMoveToHole(playerId, line, isMain);
            }
            else {
                storeEvent(attackFailed(playerId, rockId, line));
                if (isMain) {
                    if (roundData.isGimmickPlayed(playerId, GimmickType.vabank)) {
                        playGimmick(playerId, roundData.getGimmick(playerId, GimmickType.vabank));
                        addScore(enemyId);
                    }
                }
            }
        }
        else {
            calcMoveToHole(playerId, line, isMain);
        }
    }

    function calcMoveToHole(playerId:String, line:Int, isMain:Bool) {
        var enemyId:String = getEnemyId(playerId);
        var enemyLine = DataHelper.getOppositeLine(line);
        var actualAttackLine = line;
        var rockId = getAttackingRock(playerId, line, isMain);
        
        if (roundData.hasGimmickAtLine(enemyId, GimmickType.lineChange, line)) {
            var lineChange = roundData.getGimmick(enemyId, GimmickType.lineChange);
            var newLine = lineChange.lines[1];
            actualAttackLine = newLine;
            enemyLine = DataHelper.getOppositeLine(newLine);
            playGimmick(enemyId, lineChange);
        }

        storeEvent(moveToHole(playerId, rockId, line));
        if (roundData.hasRockAtLine(enemyId, RockState.defence, enemyLine)) {
             storeEvent(holeDefenced(enemyId, enemyLine));
             clearDefence(enemyId, enemyLine);
             calcDropAttack(playerId, line, isMain);

        }
        else if (roundData.hasGimmickAtLine(enemyId, GimmickType.fire, actualAttackLine)) {
            var fire = roundData.getGimmick(enemyId, GimmickType.fire);
            playGimmick(enemyId, fire);
            calcDropAttack(playerId, line, isMain);
        }
        else {
            storeEvent(goal(playerId, rockId, line));
            addScore(playerId);
            if (isMain) {
                if (roundData.isGimmickPlayed(playerId, GimmickType.vabank)) {
                    playGimmick(playerId, roundData.getGimmick(playerId, GimmickType.vabank));
                    addScore(playerId);
                }
            }
        }
    }

    function calcDropAttack(playerId:String, line:Int, isMain:Bool) {
        var rockId = getAttackingRock(playerId, line, isMain);
        var enemyId:String = getEnemyId(playerId);
        storeEvent(attackDroped(playerId, rockId, line));
        if (roundData.hasGimmickAtLine(playerId, GimmickType.spring, line)) {
            var spring = roundData.getGimmick(playerId, GimmickType.spring);
            playGimmick(playerId, spring);
            calcMoveToHole(playerId, line, isMain);
        }
        else if (roundData.hasGimmickAtLine(playerId, GimmickType.mirror, line)) {
            var mirror = roundData.getGimmick(playerId, GimmickType.mirror);
            playGimmick(playerId, mirror);
            var newLine = Math.floor(Math.abs(Game.LINES_COUNT - 1 - line));
            storeEvent(rockMirrored(playerId, line, newLine));
            var rocksGrid = roundData.getPlayerPosition(playerId).rocks;
            var state = RockState.attack;
            var lines = rocksGrid.get(state);
            lines[line] = -1;
            lines[newLine] = rockId;
            calcAttack(playerId, newLine, isMain);
        }
        else {
            storeEvent(attackFailed(playerId, rockId, line));
            if (isMain) {
                if (roundData.isGimmickPlayed(playerId, GimmickType.vabank)) {
                    playGimmick(playerId, roundData.getGimmick(playerId, GimmickType.vabank));
                    addScore(enemyId);
                }
            }
        }
    }

    function addScore(playerId) {
        roundData.scores[playerId] += 1;
        events.push(scoreChanged(playerId, roundData.scores[playerId]));
    }

    function playGimmick(playerId:String, gimmick:GimmickData) {
        storeEvent(gimmickPlayed(playerId, gimmick));
        if (gimmick.type.isOnePass()) {
            var pos = roundData.getPlayerPosition(playerId);
            pos.gimmicks.remove(gimmick.type);
            storeEvent(gimmickRemoved(playerId, gimmick));
        }
    }

    function clearDefence(playerId:String, line:Int) {
        var rocksGrid = roundData.getPlayerPosition(playerId).rocks;
        var lines = rocksGrid.get(RockState.defence);
        lines[line] = -1;
    }

    function storeEvent(event:BattleEvent) {
        events.push(event);
    }

    function getEnemyId(playerId:String):String {
        return calcOrder.filter(function(id) return id != playerId)[0];
    }

    function getAttackingRock(playerId:String, line:Int, isMain:Bool):Int {
        var state = (isMain) ? RockState.attack : RockState.secondAttack;
        return roundData.getRockId(playerId, state, line);
    }

    function clone(data:RoundData):RoundData {
        return haxe.Unserializer.run(haxe.Serializer.run(data));
    }
}