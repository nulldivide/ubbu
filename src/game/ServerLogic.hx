import ubbu.Game;
import ubbu.Types;
import messaging.Endpoint;
import messaging.Message;

@:expose
class ServerLogic {

    public var endpoint(default, null):Endpoint;

    var game:Game;
    var playerId:String;
    var delayedMessages:Array<Message<Any>>;
    
    public static function create(id:String, config:GameConfig):Game {
        var data = DataBuilder.createGame(id);
        return new Game(config, data);
    }

    public function new(game:Game, playerId:String) {
        this.game = game;
        this.playerId = playerId;
        delayedMessages = [];
        game.addEventListener(onGameEvent);
        endpoint = new Endpoint();
        var startMessage:Message<Any> = cast Messages.connected(game.data, game.config);
        endpoint.output.put(startMessage);
    }

    public function putMessage(message:Message<Any>) {
        endpoint.input.put(message);
        checkMessages();
    }

    public function checkMessages() {
        if (endpoint.input.hasMessages()) {
            var message = endpoint.input.get();
            processMessage(message);
        }
    }

    public function getByRoundScores(playerId):Array<Int> {
        var result = [];
        for (round in game.data.rounds) {
            result.push(round.scores[playerId]);
        }
        return result;
    }

    public function getTotalScores(playerId):Int {
        var total = 0;
        for (round in getByRoundScores(playerId)) {
            total += round;
        }
        return total;
    }

    function processMessage<T>(message:Message<T>) {
        switch (message.type) {

            case Messages.PLAYER_JOIN:
                var playerId:String = message.payload;
                if (playerId == this.playerId) {
                    game.join(playerId);
                }

            case Messages.ROCK_MOVED:
                var p:RockMovedPayload = message.payload;
                var playerId = p.playerId;
                if (playerId == this.playerId) {
                    game.moveRock(playerId, p.from, p.to, p.state);
                }

            case Messages.GIMMICK_PLACED:
                var p:GimmickPlacedPayload = message.payload;
                if (p.playerId == this.playerId) {
                    game.placeGimmick(p.playerId, p.gimmick.type, p.gimmick.lines);
                }

            case Messages.PLAYER_END_TURN:
                var playerId:String = message.payload;
                if (playerId == this.playerId) {
                    game.endTurn(playerId, message.time);
                }

            case Messages.PLAYER_WATCHED:
                var playerId:String = message.payload;
                if (playerId == this.playerId) {
                    game.battleWatched(playerId, message.time);
                }

            default:
        }
    }

    function onGameEvent(event:GameEvent) {
        switch (event) {
            case playerJoin(playerId) if (playerId != this.playerId):
                var m:Message<Any> = cast Messages.playerJoin(playerId);
                endpoint.output.put(m);

            case rockMoved(playerId, from, to, state) if (playerId != this.playerId):
                var m:Message<Any> = cast Messages.rockMoved(playerId, from, to, state);
                delayedMessages.push(m);

            case gimmickPlaced(playerId, gimmick) if (playerId != this.playerId):
                var m:Message<Any> = cast Messages.gimmickPlaced(playerId, gimmick);
                delayedMessages.push(m);
                
            case stateChanged(_, GameState.playingBattle):
                sendDelayedMessages();
                var enemyId = game.getEnemyId(playerId);
                var m:Message<Any> = cast Messages.playerEndTurn(enemyId);
                endpoint.output.put(m);

            case playerWatched(playerId) if (playerId != this.playerId):
                var m:Message<Any> = cast Messages.playerWatched(playerId);
                endpoint.output.put(m);

            default:
        }
    }

    function sendDelayedMessages() {
        for (m in delayedMessages) {
            endpoint.output.put(m);
        }
        delayedMessages = [];
    }
}
