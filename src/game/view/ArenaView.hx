package view;

import ubbu.Game;
import ubbu.Types;
import snap.Snap;
import haxe.Timer;
import view.arena.*;
import ubbu.DataHelper;
import ext.ArenaAnimations;

typedef AttackPath = {
    var startLine:Int;
    var endLine:Int;
    var path:String;
}

class GimmicksPanelView {

    var element:SnapElement;

    public function new() {
        element = Snap.select("#gimmicksPanel");
    }

    public function sleepMode() {
        trace("sleep mode");
        element.removeClass("on");
        element.addClass("sleep");
    }

    public function activeMode() {
        trace("active mode");
        element.removeClass("sleep");
        element.addClass("on");
    }
}

class ArenaView implements IView {

    public static inline var ID:String = "Arena";
    public static inline var YOURS_COLOUR:String = "#BE202E";
    public static inline var ENEMY_COLOUR:String = "#1D76BB";
    public static inline var ROCK_RADIUS:Int = 20;

    public var snap(default, null):Snap;
    
    var rocksGroup:SnapElement;
    var countdown:SnapElement;
    var staticLine:SnapElement;
    var progressLine:SnapElement;
    var progressIndicator:SnapElement;
    var rocks:Map<String, Array<SnapElement>>;
    var gimmicks:Map<String, Map<GimmickType, SnapElement>>;
    var attackPathes:Map<String, Array<AttackPath>>;
    var gimmicksPanel:GimmicksPanelView;

    var placingRocks:PlacingRocks;
    var placingGimmicks:PlacingGimmicks;
    var playingBattle:PlayingBattle;
    public var scores(default, null):Array<Array<Int>>;
    public var animator(default, null):ArenaAnimations;

    var myId:String;
    var enemyId:String;
    var game:Game;
    var timer:Timer;
    var timerStarted:Float;

    public function new(game:Game, myId:String) {
        this.myId = myId;
        this.game = game;
        this.rocks = new Map<String, Array<SnapElement>>();
        this.gimmicks = new Map<String, Map<GimmickType, SnapElement>>();
        this.attackPathes = new Map<String, Array<AttackPath>>();
        this.scores = [for (i in 0...4) [0, 0]];
        animator = new ArenaAnimations();
    }

    public function show(snap:Snap) {
        this.snap = snap;
        gimmicksPanel = new GimmicksPanelView();

        placingRocks = new PlacingRocks(snap, game);
        placingGimmicks = new PlacingGimmicks(snap, game, gimmicksPanel);
        playingBattle = new PlayingBattle(snap, game, animator);
        playingBattle.scoreChangedSignal.add(onScoreChanged);
        playingBattle.doneSignal.add(onBattleWhatched);
        initLayout();
        clearArena();
        initialRender();
        validateState();
        game.addEventListener(onGameEvent);
    }

    public function hide() {}

    public dynamic function renderScore(scores:Array<Array<Int>>) {
        trace('Scores: $scores');
    }

    function onGameEvent(event:GameEvent) {
        switch (event) {
            case playerJoin(id):
                initPlayer(id);

            case stateChanged(created, placingRocks):
                enemyId = game.getEnemyId(myId);
                animator.animateGettingReady()
                    .then(function(_) startTurn());

            case stateChanged(playingBattle, placingRocks):
                initialRender();
                animator.animateGettingReady()
                    .then(function(_) startTurn());

            case stateChanged(placingRocks, playingBattle):
                var battleEntities = {
                    rocks:rocks,
                    gimmicks:gimmicks,
                    attackPathes:attackPathes
                };
                playingBattle.activate(battleEntities, myId);

            case stateChanged(_, gameEnd):
                placingRocks.deactivate();
                placingGimmicks.deactivate();
                timer.stop();
                animator.stopAll();
                clearArena();
            
            case rockMoved(playerId, rockId, to, state):
                onRockMoved(playerId, rockId, to, state);

            case gimmickPlaced(playerId, gimmick):
                onGimmickPlaced(playerId, gimmick);

            case playerEndTurn(playerId) if (playerId == myId):
                placingRocks.deactivate();
                placingGimmicks.deactivate();

            default:
                trace(event);
        }
    }

    function initLayout() {
        Snap.select("#rocks").addClass("on");
        staticLine = Snap.select("#staticLine");
        progressLine = Snap.select("#progressLine");
        progressIndicator = Snap.select("#verticalProgressIndicator");
    }

    function initialRender() {
        renderTimer(game.positionStageTime);
        renderScore(scores);
        for (id in game.getPlayerIds()) {
            initPlayer(id);
        }
    }

    function validateState() {
        switch (game.data.state) {
            case GameState.placingRocks:
                 animator.animateGettingReady()
                    .then(function(_) startTurn());
            default:
        }
    }

    function initPlayer(id:String) {
        placeRocks(id);
        initAttackPathes(id);
    }

    function placeRocks(playerId:String) {
        var isYours = (playerId == myId) ? "Your" : "Enemy";
        rocks[playerId] = [for (i in 0...Game.LINES_COUNT) Snap.select('#rock$isYours$i')];
    }

    function initAttackPathes(playerId:String) {
        attackPathes[playerId] = [for (line in 0...Game.LINES_COUNT) createAttackPath(line, line, (playerId != myId))];
    }

    function startTurn() {
        trace('startTurn');
        timerStarted = Timer.stamp();
        timer = new Timer(0);
        timer.run = function() {
            var timeLeft = game.positionStageTime - (Timer.stamp() - timerStarted);
            if (timeLeft <= 0) {
                timer.stop();
                timeLeft = 0;
                onTimeOver();
            }
            renderTimer(timeLeft);
        }

        Snap.select("#timer").addClass("on");
        gimmicksPanel.sleepMode();
        placingRocks.activate(myId, onRocksPlaced);
    }

    function onTimeOver() {
        game.endTurn(myId, Date.now().getTime());
    }

    function onRocksPlaced() {
        placingGimmicks.activate(myId);
    }

    function onBattleWhatched() {
        clearArena();
        game.battleWatched(myId, Date.now().getTime());
    }

    function clearArena() {
        renderTimer(game.positionStageTime);
        for (gimmicksOfPlayer in gimmicks) {
            for (elm in gimmicksOfPlayer) {
                elm.removeClass("on");
            }
        }

        for (i in 0...4) {
            Snap.select('#attackSecondLineEnemy$i').removeClass("off");
            Snap.select('#attackSecondLineYour$i').removeClass("off");
        }

        for (playerId in rocks.keys()) {
            var rocksOfPlayer = rocks[playerId];
            var isYours = (playerId == myId);
            for (rockId in 0...4) {
                var elm = rocksOfPlayer[rockId];
                var bbox = PositionHelper.getRockPosition(rockId, isYours, RockState.start);
                elm.attr({cx:bbox.cx, cy:bbox.cy});
            }
        }
    }

    function renderTimer(timeLeft:Float) {
        var progress:Float = 1.0 - timeLeft / game.positionStageTime;
        var x1:Float = Std.parseFloat(staticLine.attr("x1"));
        var x2:Float = Std.parseFloat(staticLine.attr("x2"));
        var width:Float = x2 - x1;
        var progressX:Float = x1 +  progress * width;
        progressLine.attr("x2", progressX);
        progressIndicator.attr({x1:progressX, x2:progressX});
    }
    
    function onRockMoved(playerId:String, rockId:Int, line:Int, state:RockState) {
        var isYours = (playerId == myId);
        var rock = rocks[playerId][rockId];
        var bbox = PositionHelper.getRockPosition(line, isYours, state);
        rock.animate({cx:bbox.cx, cy:bbox.cy}, 50);
        if (isYours) {
            var rockElement = Snap.select('#animationRockYour$line');
            rockElement.removeClass("on");
        }
    }

    function onGimmickPlaced(playerId:String, gimmick:GimmickData) {
        var isYours:Bool = (playerId == myId);
        if (!gimmicks.exists(playerId)) {
            gimmicks[playerId] = new Map<GimmickType, SnapElement>();
        }
        if (gimmick.type == GimmickType.lineChange) {
            var elements = PositionHelper.lineChangeElements(gimmick.lines, isYours);
            elements.line.addClass("on");
            gimmicks[playerId][gimmick.type] = elements.line;

            var line = gimmick.lines[0];
            var oldPathId = (isYours) ? '#attackSecondLineEnemy$line' : '#attackSecondLineYour$line';
            Snap.select(oldPathId).addClass("off");
        }
        else {
            var id = '${gimmick.type}${(isYours) ? "Your" : "Enemy"}';
            if (gimmick.lines.length > 0) {
                id = '$id${gimmick.lines[0]}';
            }
            var gimmickView = Snap.select('#$id');
            gimmickView.addClass("on");
            gimmicks[playerId][gimmick.type] = gimmickView;
        }

        if (gimmick.type == GimmickType.lineChange) {
            var attackPath = createAttackPath(gimmick.lines[0], gimmick.lines[1], isYours);
            var opponentId = game.getEnemyId(playerId);
            trace('Line change: $opponentId, ${gimmick.lines[0]}');
            attackPathes[opponentId][gimmick.lines[0]] = attackPath;
        }
        /*
        var gimmickView = createGimmickView(gimmick, isYours);
        
        gimmicks[playerId][gimmick.type] = gimmickView;

        if (gimmick.type == GimmickType.lineChange) {
            var attackPath = createAttackPath(gimmick.lines[0], gimmick.lines[1], isYours);
            var opponentId = game.getEnemyId(playerId);
            trace('Line change: $opponentId, ${gimmick.lines[0]}');
            attackPathes[opponentId][gimmick.lines[0]] = attackPath;
            var path = snap.path(attackPath.path);
            path.attr({fill:"transparent", stroke:ArenaView.ENEMY_COLOUR, "stroke-miterlimit":"10", opacity:0.7});
            gimmicksGroup.append(path);
        }
        */
    }

    function onScoreChanged(playerId:String, amount:Int) {
        var index = (playerId == myId) ? 0 : 1;
        scores[game.currentRoundNum][index] = amount;
        renderScore(scores);
    }

    function createAttackPath(startLine:Int, endLine:Int, isYours:Bool):AttackPath {
        return {
            startLine:startLine,
            endLine:endLine,
            path:createPath(startLine, endLine, isYours)
        };
    }

    function createPath(startLine:Int, endLine:Int, isYours:Bool):String {
        var startPos = PositionHelper.getRockPosition(startLine, !isYours, RockState.attack);
        var endPos = PositionHelper.getRockPosition(endLine, !isYours, RockState.atHole);
        var startPosX = startPos.cx;
        var startPosY = startPos.y + startPos.height;
        var endPosX = PositionHelper.getHoleEnterX(startPos, endPos);
        var endPosY = PositionHelper.getHoleEnterY(startPos, endPos);
        return if (startLine == endLine) {
            'M $startPosX $startPosY L $endPosX $endPosY';
        }
        else {
            var controlPoint = PositionHelper.getRockPosition(startLine, !isYours, RockState.atHole);
            'M $startPosX $startPosY Q ${controlPoint.cx} ${controlPoint.cy}, $endPosX $endPosY';
        }
    }

    function isGimmickOnRock(gimmick:GimmickData):Bool {
        return switch (gimmick.type) {
            case GimmickType.lineChange:
                var line = DataHelper.getOppositeLine(gimmick.lines[1]);
                game.hasDefenceAtLine(myId, line);

            case GimmickType.second:
                true;

            default:
                false;
        }
    }
}
