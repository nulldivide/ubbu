package view;

import snap.Snap;

interface IView {

    function show(snap:Snap):Void;
    function hide():Void;
}
