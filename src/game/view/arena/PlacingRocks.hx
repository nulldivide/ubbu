package view.arena;

import ubbu.Game;
import ubbu.Types;
import snap.Snap;
import view.ArenaView;
import msignal.Signal;

using ubbu.DataHelper;

@:enum
abstract Direction(String) to String from String {
    var top = "Top";
    var left = "Left";
    var right = "Right";

    public function targetLine(fromLine:Int):Int {
        return (this == Direction.right) ? fromLine + 1 : fromLine;
    }

    public function targetState():RockState {
        return (this == Direction.top) ? RockState.attack : RockState.defence;
    }

    public function toClassName():String {
        return this.toLowerCase();
    }
}

class ActivationZone {

    public var line(default, null):Int;
    public var direction(default, null):Direction;
    public var rockElement(default, null):SnapElement;
    public var onApply(default, null):Void -> Void;
    @:isVar public var enabled(get, set):Bool;

    var element:SnapElement;

    public function new(line:Int, direction:Direction, onApply:Void -> Void) {
        this.line = line;
        this.direction = direction;
        this.onApply = onApply;
        this.element = Snap.select('#activationZone$direction$line');
        this.rockElement = Snap.select('#animationRockYour$line');
    }

    public function bindZone(zone:ActivationZone) {
        this.line = zone.line;
        this.rockElement = zone.rockElement;
        this.onApply = zone.onApply;
        this.direction = zone.direction;
    }

    public function onHover(_) {
        togleAnimateRock(true);
    }

    public function onOut(_) {
        togleAnimateRock(false);
    }

    public function onClick(_) {
        onApply();
    }

    function togleAnimateRock(v:Bool) {
        if (v) {
            rockElement.addClass(direction.toClassName());
        }
        else {
            rockElement.removeClass(direction.toClassName());
        }
    }

    function get_enabled():Bool {
        return this.enabled;
    }

    function set_enabled(v:Bool):Bool {
        if (this.enabled == v)
            return this.enabled;

        this.enabled = v;
        if (v) {
            element.hover(onHover, onOut);
            element.click(onClick);
            element.addClass("on");
            
        }
        else {
            element.unhover(onHover, onOut);
            element.unclick(onClick);
            element.removeClass("on");
            onOut(new js.html.Event(""));
        }
        return v;
    }
}

class PlacingRocks {

    var playerId:String;
    var snap:Snap;
    var game:Game;
    var doneCallback:Void->Void;
    var zones:Map<String, ActivationZone>;

    public function new(snap:Snap, game:Game) {
        this.snap = snap;
        this.game = game;
    }

    public function activate(playerId:String, doneCallback:Void -> Void) {
        this.doneCallback = doneCallback;
        this.playerId = playerId;
        var directions = AbstractEnumTools.getValues(Direction);
        zones = new Map<String, ActivationZone>();
        for (i in 0...Game.LINES_COUNT) {
            for (direction in directions) {
                var line = direction.targetLine(i);
                var state = direction.targetState();
                if (game.canMoveRock(playerId, i, line, state)) {
                    var zone = new ActivationZone(i, direction, onZoneApply.bind(i, line, state));
                    zones['$i:$direction'] = zone;
                }
            }
            activateAnimationRock(i);
        }
        validateControls();
    }

    public function deactivate() {
         for (zone in zones) {
             zone.enabled = false;
         }
    }

    function onZoneApply(rockId:Int, line:Int, state:RockState) {
        zones['$rockId:${Direction.top}'].enabled = false;
        if (game.canMoveRock(playerId, rockId - 1, line, RockState.defence)) {
            zones['$rockId:${Direction.left}'].bindZone(zones['${rockId - 1}:${Direction.right}']);
        }

        if (game.canMoveRock(playerId, rockId + 1, line + 1, RockState.defence)) {
            zones['$rockId:${Direction.right}'].bindZone(zones['${rockId + 1}:${Direction.left}']);
        }
        
        game.moveRock(playerId, rockId, line, state);
        validateControls();
        deactivateAnimationRock(rockId);
    }

    function validateControls() {
        if (!game.hasAnyMoves(playerId)) {
            deactivate();
            doneCallback();
        }
        else {
            for (zone in zones) {
                var line = zone.direction.targetLine(zone.line);
                var state = zone.direction.targetState();
                zone.enabled = game.canMoveRock(playerId, zone.line, line, state);
            }
        }
    }

    function activateAnimationRock(line:Int) {
        var animationRock = Snap.select('#animationRockYour$line');
        animationRock.addClass("on");
    }

    function deactivateAnimationRock(line:Int) {
        var rockElement = Snap.select('#animationRockYour$line');
        var directions = AbstractEnumTools.getValues(Direction);
        for (direction in directions) {
            rockElement.removeClass(direction.toClassName());
        }
        rockElement.removeClass("on");
    }
}
