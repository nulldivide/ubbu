package view.arena;

import snap.Snap;
import ubbu.Game;
import ubbu.Types;
import ubbu.BattleCalculator;
import view.ArenaView;
import msignal.Signal;
import js.Promise;
import ext.ArenaAnimations;

using ubbu.DataHelper;

typedef BattleEntities =
{
    var rocks:Map<String, Array<SnapElement>>;
    var gimmicks:Map<String, Map<GimmickType, SnapElement>>;
    var attackPathes:Map<String, Array<AttackPath>>;
}

enum Nil {
    nil;
}

class PlayingBattle {

    var entities:BattleEntities;
    var game:Game;
    var snap:Snap;
    var events:Array<BattleEvent>;
    var myId:String;
    var animator:ArenaAnimations;
    public var scoreChangedSignal(default, null):Signal2<String, Int>;
    public var doneSignal(default, null):Signal0;

    public function new(snap:Snap, game:Game, animator:ArenaAnimations) {
        this.snap = snap;
        this.game = game;
        this.animator = animator;
        events = [];
        scoreChangedSignal = new Signal2<String, Int>();
        doneSignal = new Signal0();
    }

    public function activate(entities:BattleEntities, myId:String) {
        this.entities = entities;
        this.myId = myId;
        events = game.currentRound.battleEvents.copy();
        playBattle();
    }

    function playBattle() {
        var head = Promise.resolve(0);
        var animations:Array<Void -> Promise<Int>> = [];
        for (event in events) {
            animations.push(animateEvent.bind(event));
        }
        var all = sequence(animations);
        all.then(function(_) {
            doneSignal.dispatch();
        });
    }

    function animateEvent(event:BattleEvent):Promise<Int> {
        return switch (event) {
            case gimmickPlayed(playerId, gimmick):
                animateGimmickPlayed(playerId, gimmick);
            case gimmickRemoved(playerId, gimmick):
                animateGimmickRemoved(playerId, gimmick);
            case moveToHole(playerId, rockId, line):
                animateMoveToHole(playerId, rockId, line);
            case goal(playerId, rockId, line):
                animateGoal(playerId, rockId, line);
            case holeDefenced(playerId, line):
                animateHoleDefenced(playerId, line);
            case attackDroped(playerId, rockId, line):
                animateAttackDroped(playerId, rockId, line);
            case rockMirrored(playerId, rockId, line):
                animateRockMirrored(playerId, rockId, line);
            case attackFailed(playerId, rockId, line):
                animateAttackFailed(playerId, rockId, line);
            case scoreChanged(playerId, amount):
                return new Promise<Int>(function(resolve, reject) {
                    scoreChangedSignal.dispatch(playerId, amount);
                    resolve(0);
                });
        }
    }

    function animateGimmickPlayed(playerId:String, gimmick:GimmickData):Promise<Int> {
        var gimmickView = entities.gimmicks[playerId][gimmick.type];
        return animator.animateGimmickPlayed(gimmickView);
        //return animate(gimmickView, {opacity:0.3}, 500)
        //    .then(return animate(gimmickView, {opacity:1.0}, 500));
    }

    function animateGimmickRemoved(playerId:String, gimmick:GimmickData):Promise<Int> {
        var gimmickView = entities.gimmicks[playerId][gimmick.type];
        return animator.animateGimmickRemoved(gimmickView);
        //return animate(gimmickView, {opacity:0.3}, 500);
    }

    function animateMoveToHole(playerId:String, rockId:Int, line:Int):Promise<Int> {
        var rock = entities.rocks[playerId][rockId];
        var path = entities.attackPathes[playerId][line].path;
        return animator.animateMoveToHole(rock, path);
        /*
        var pathLength = Snap.path.getTotalLength(path);
        return new Promise<Int>(function(resolve, reject) {
            Snap.animate(0, pathLength,
                function(val:Float):Void {
                    var p:{x:Float, y:Float, alpha:Float} = Snap.path.getPointAtLength(path, val);
                    rock.attr("cx", p.x);
                    rock.attr("cy", p.y);
                },
            500, null, function(_) resolve(0));
        });
        */
    }

    function animateRockMirrored(playerId:String, rockId:Int, line:Int):Promise<Int> {
        var rock = entities.rocks[playerId][rockId];
        var isYours = (playerId == myId);
        var bbox = PositionHelper.getRockPosition(line, isYours, RockState.attack);
        return animator.animateRockMirrored(rock, bbox);
        /*
        return new Promise<Int>(function(resolve, reject) {
            var rock = entities.rocks[playerId][rockId];
            var isYours = (playerId == myId);
            var bbox = PositionHelper.getRockPosition(line, isYours, RockState.attack);
            rock.attr({cx:bbox.cx, cy:bbox.cy});
            resolve(0);
        });
        */
    }

    function animateHoleDefenced(playerId, line):Promise<Int> {
        var rockId = game.currentRound.getRockId(playerId, RockState.defence, line);
        var isYours = (playerId == myId);
        var rock = entities.rocks[playerId][rockId];
        var bbox = PositionHelper.getRockPosition(rockId, isYours, RockState.start);
        return animator.animateHoleDefenced(rock, bbox);
        //return animate(rock, {cx:bbox.cx, cy:bbox.cy}, 500);
    }

    function animateAttackDroped(playerId, rockId, line):Promise<Int> {
        var rock = entities.rocks[playerId][rockId];
        var path = entities.attackPathes[playerId][line].path;
        return animator.animateAttackDroped(rock, path);
        /*
        return new Promise<Int>(function(resolve, reject) {
            var rock = entities.rocks[playerId][rockId];
            var path = entities.attackPathes[playerId][line].path;
            var pathLength = Snap.path.getTotalLength(path);
            Snap.animate(pathLength, 0,
                function(val:Float):Void {
                    var p:{x:Float, y:Float, alpha:Float} = Snap.path.getPointAtLength(path, val);
                    rock.attr("cx", p.x);
                    rock.attr("cy", p.y);
                },
            500, null, function(_) resolve(0));
        });
        */
    }

    function animateAttackFailed(playerId, rockId, line):Promise<Int> {
        var isYours = (playerId == myId);
        var rock = entities.rocks[playerId][rockId];
        var bbox = PositionHelper.getRockPosition(rockId, isYours, RockState.start);
        return animator.animateAttackFailed(rock, bbox);
        //return animate(rock, {cx:bbox.cx, cy:bbox.cy}, 500);
    }

    function animateGoal(playerId, rockId, line):Promise<Int> {
        var isYours = (playerId == myId);
        var rock = entities.rocks[playerId][rockId];
        var atHolePos = PositionHelper.getRockPosition(rockId, isYours, RockState.atHole);
        var startPos = PositionHelper.getRockPosition(rockId, isYours, RockState.start);
        return animator.animateGoal(rock, atHolePos, startPos);
        //return animate(rock, {cx:bbox.cx, cy:bbox.cy}, 500);
    }

    function animate(view:SnapElement, properties:Any, time:Float, ?easing:String):Promise<Int> {
        return new Promise(function(resolve, reject) {
            view.animate(properties, time, easing, function(_) resolve(0));
        });
    }

    function sequence<T>(promiseProviders:Array<Void -> Promise<T>>):Promise<T> {
        var head:Promise<T> = Promise.resolve(null);
        var tail:Promise<T> = Lambda.fold(promiseProviders, function(next:Void -> Promise<T>, prev:Promise<T>):Promise<T> {
            return prev.then(function(_) return next());
        }, head);
        return tail;
    }
}
