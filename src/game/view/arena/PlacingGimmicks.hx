package view.arena;

import ubbu.Game;
import ubbu.Types;
import view.ArenaView;
import snap.Snap;
import msignal.Signal;

using ubbu.DataHelper;

class GimmickSelector {

    var type:GimmickType;
    var element:SnapElement;
    var hitArea:SnapElement;
    public var clickedSignal:Signal1<GimmickType>;
    public var canSelect(default, set):Bool;

    public function new(snap:Snap, type:GimmickType) {
        this.hitArea = Snap.select('#$type');
        this.element = hitArea.parent().children()[1];
        this.type = type;
        clickedSignal = new Signal1<GimmickType>();
    }

    function onHover(_) {
        element.addClass("on");
    }

    function onOut(_) {
        element.removeClass("on");
    }

    function onClick(_) {
        clickedSignal.dispatch(type);
    }

    function set_canSelect(v:Bool):Bool {
        hitArea.unhover(onHover, onOut);
        hitArea.unclick(onClick);
        if (v) {
            hitArea.hover(onHover, onOut);
            hitArea.click(onClick);
            hitArea.parent().removeClass("used");
        }
        else {
            hitArea.parent().addClass("used");
        }
        return v;
    }
}

private class ActivationZone {

    public dynamic function onActivate() {}
    public dynamic function onDeactivate() {}
    public dynamic function onApply() {}
    public dynamic function onHover() {}
    public dynamic function onOut() {}
    var element:SnapElement;

    public function new(element:SnapElement) {
        this.element = element;
    }

    public function activate() {
        element.addClass("on");
        element.hover(onHoverListener, onOutListener);
        element.click(onClick);
    }

    public function deactivate() {
        element.removeClass("on");
        element.unhover(onHoverListener, onOutListener);
        element.unclick(onClick);
        onDeactivate();
    }

    function onHoverListener(_) {
        onHover();
    }

    function onOutListener(_) {
        onOut();
    }

    function onClick(_) {
        onApply();
    }
}

class PlacingGimmicks {

    var snap:Snap;
    var game:Game;
    var selectedGimmick:GimmickData;
    var playerId:String;
    var selectors:Map<GimmickType, GimmickSelector>;
    var activatedZones:Array<ActivationZone>;
    var gimmicksPanel:GimmicksPanelView;

    public function new(snap:Snap, game:Game, gimmicksPanel:GimmicksPanelView) {
        this.snap = snap;
        this.game = game;
        this.selectors = new Map<GimmickType, GimmickSelector>();
        this.gimmicksPanel = gimmicksPanel;
        activatedZones = [];
    }

    public function activate(playerId:String) {
        gimmicksPanel.activeMode();

        this.playerId = playerId;
        var allGimmicks = GimmickType.all();
        var activeSet = game.getPlayer(playerId).gimmicksSet;
        
        for (gimType in allGimmicks) {
            var sel = new GimmickSelector(snap, gimType);
            selectors.set(gimType, sel);
            sel.clickedSignal.add(onGimmickSelected);
            sel.canSelect = (activeSet.indexOf(gimType) >= 0);
        }
    }

    public function deactivate() {
        for (selector in selectors) {
            selector.canSelect = false;
        }
        clearActivationZones();
        gimmicksPanel.sleepMode();
    }

    function onGimmickSelected(type:GimmickType) {
        clearActivationZones();
        selectedGimmick = {type:type, lines:[]};
        tryApply();
    }

    function tryApply() {
        var linesToAsk:Array<GimmickParamsType> = selectedGimmick.type.paramTypes();
        if (linesToAsk.length > selectedGimmick.lines.length) {
            for (lineType in linesToAsk) {
                tryAssignLines(lineType);
            }
            if (linesToAsk.length > selectedGimmick.lines.length) {
                activateZones();
            }
        }

        if (linesToAsk.length == selectedGimmick.lines.length) {
            placeGimmick();
        }
    }

    function placeGimmick() {
        game.placeGimmick(playerId, selectedGimmick.type, selectedGimmick.lines);
        clearActivationZones();
        selectors[selectedGimmick.type].canSelect = false;
    }

    function tryAssignLines(lineType:GimmickParamsType) {
        var lines = getPossibleLines(playerId, selectedGimmick.lines, lineType);
        var isYours = lineType.placingOnYourLine();
        if (lines.length == 1) {
            selectedGimmick.lines.push(lines[0]);
        }
    }

    function activateZones() {
        clearActivationZones();
        if (selectedGimmick.type == GimmickType.lineChange) {
            for (i in 0...4) {
                for (dir in ["Left", "Right"]) {
                    var onActivate:Void -> Void = function() {
                        Snap.select('#switchYour$i').addClass.bind("visibleMiddle");
                    }
                    var onDeactivate:Void -> Void = function() {
                        Snap.select('#switchYour$i').removeClass("visibleMiddle");
                        Snap.select('#switchYour$i').removeClass("on");
                        var lines = getLines(i, dir);
                        var elements = PositionHelper.lineChangeElements(lines, true);
                        elements.offLine.removeClass("off");
                        elements.elm.removeClass("on");
                        elements.line.removeClass("on");
                    }
                    var zoneElm = Snap.select('#activationZone$dir$i');
                    if (zoneElm != null) {
                        var zone = new ActivationZone(zoneElm);
                        zone.onActivate = onActivate;
                        zone.onDeactivate = onDeactivate;
                        zone.onApply = onLineChangeApply.bind(i, dir);
                        zone.onHover = onLineChangeZoneHover.bind(i, dir);
                        zone.onOut = onLineChangeZoneOut.bind(i, dir);
                        activatedZones.push(zone);
                    }
                }
            }
        }
        else {
            for (i in 0...4) {
                var zoneElm = Snap.select('#gimmicksActivationZone${3 - i}');
                var zone = new ActivationZone(zoneElm);
                zone.onActivate = cast Snap.select('#${selectedGimmick.type}Your$i').addClass.bind("visibleMiddle");
                zone.onDeactivate = onGimmickDeactivate.bind(i);
                zone.onApply = onGimmickApply.bind(i);
                zone.onHover = onGimmickZoneHover.bind(i);
                zone.onOut = onGimmickZoneOut.bind(i);
                activatedZones.push(zone);
            }
        }
        for (zone in activatedZones) {
            zone.activate();
            zone.onOut();
        }
    }

    function onGimmickDeactivate(line:Int) {
        Snap.select('#${selectedGimmick.type}Your$line').removeClass("visibleMiddle");
        Snap.select('#${selectedGimmick.type}Your$line').removeClass("on");
    }

    function onGimmickZoneHover(line:Int) {
        var elm = Snap.select('#${selectedGimmick.type}Your${line}');
        elm.removeClass("visibleMiddle");
        elm.addClass("on");
    }

    function onGimmickZoneOut(line:Int) {
        var elm = Snap.select('#${selectedGimmick.type}Your${line}');
        elm.removeClass("on");
        elm.addClass("visibleMiddle");
    }

    function onGimmickApply(line:Int) {
        clearActivationZones();
        selectedGimmick.lines.push(line);
        placeGimmick();
    }

    function onLineChangeZoneHover(id:Int, dir:String) {
        var lines = getLines(id, dir);
        var elements = PositionHelper.lineChangeElements(lines, true);
        elements.offLine.addClass("off");
        elements.elm.removeClass("visibleMiddle");
        elements.elm.addClass("on");
        elements.line.addClass("on");
    }

    function onLineChangeApply(id:Int, dir:String) {
        clearActivationZones();
        var lines = getLines(id, dir);
        selectedGimmick.lines = lines;
        placeGimmick();
    }

    function onLineChangeZoneOut(id:Int, dir:String) {
        var lines = getLines(id, dir);
        var elements = PositionHelper.lineChangeElements(lines, true);
        elements.offLine.removeClass("off");
        elements.elm.addClass("visibleMiddle");
        elements.elm.removeClass("on");
        elements.line.removeClass("on");
    }
    
    function getLines(id:Int, dir:String):Array<Int> {
        return switch [id, dir] {
            case [0, "Left"]: [2, 3];
            case [0, "Right"]: [3, 2];
            case [1, "Left"]: [1, 2];
            case [1, "Right"]: [2, 1];
            case [2, "Left"]: [0, 1];
            case [2, "Right"]: [1, 0];
            case [3, "Left"]: [1, 0];
            default:
                throw 'not matched zone: $id $dir';
        }
    }

    function getPossibleLines(playerId:String, selected:Array<Int>, lineType:GimmickParamsType):Array<Int> {
        return switch (lineType) {
            case attackLine:
                game.currentRound.getLinesWithRocks(playerId, RockState.attack);

            case defenceLine:
                [for (line in 0...Game.LINES_COUNT) line];

            case defenceNeighbour:
                var firstSelected = selected[0];
                var result = [];
                if (firstSelected > 0) {
                    result.push(firstSelected - 1);
                }
                if (firstSelected < Game.LINES_COUNT - 1) {
                    result.push(firstSelected + 1);
                }
                result;

            case notActivated:
                game.currentRound.getLinesWithRocks(playerId, RockState.start);
                
            default:
                [];
        }
    }

    function clearActivationZones() {
        for (zone in activatedZones) {
            zone.deactivate();
        }
        activatedZones = [];
    }
}
