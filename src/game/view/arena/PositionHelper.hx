package view.arena;

import ubbu.Types;
import snap.Snap;

class PositionHelper {

    public static function getRockPosition(line:Int, isYours:Bool, state:RockState):BBox {
        var owner:String = (isYours) ? "Your" : "Enemy";
        var stateLabel:String = state;
        if (state == RockState.secondAttack) {
            stateLabel = RockState.attack;
        }
        var posId = '$stateLabel$owner$line';
        return Snap.select('#$posId').getBBox();
    }

    public static function getGimmickPosition(type:GimmickType, lines:Array<Int>, isYours:Bool):BBox {
        var owner:String = (isYours) ? "Your" : "Enemy";
        var pos:String = (lines.length > 0) ? '${lines[lines.length -1]}' : "";
        var posId = '$type$owner$pos';
        return Snap.select('#$posId').getBBox();
    }

    public static function getLineBBox(line:Int, isYours:Bool):BBox {
        var bbox = getRockPosition(line, isYours, RockState.attack);
        bbox.y -= 80;
        bbox.height += 160;
        return bbox;
    }

    public static function getHoleEnterX(attackPos:BBox, holePos:BBox):Float {
        var diff = holePos.cx - attackPos.cx;
        if (Math.abs(diff) < 1) {
            diff = 0.0;
        }
        return 
            if (diff < 0) {
                holePos.x + holePos.width;
            } 
            else if (diff > 0) {
                holePos.x;
            }
            else {
                holePos.cx;
            }
    }

    public static function lineChangeElements(lines:Array<Int>, isYours:Bool):{elm:SnapElement, line:SnapElement, offLine:SnapElement} {
        var from = lines[0];
        var to = lines[1];
        var suffix1 = (isYours) ? "Your" : "Enemy";
        var suffix2 = (isYours) ? "Enemy" : "Your";
        var offLine = Snap.select('#attackSecondLine${suffix2}$from');

        return switch [from, to] {
            case [2, 3]:
                {elm:Snap.select('#switch${suffix1}3'),
                line:Snap.select('#switchLineToLeft${suffix2}2'),
                offLine:offLine};
            case [3, 2]:
                {elm:Snap.select('#switch${suffix1}2'),
                line:Snap.select('#switchLineToRight${suffix2}2'),
                offLine:offLine};
            case [1, 2]:
                {elm:Snap.select('#switch${suffix1}2'),
                line:Snap.select('#switchLineToLeft${suffix2}1'),
                offLine:offLine}
            case [2, 1]:
                {elm:Snap.select('#switch${suffix1}1'),
                line:Snap.select('#switchLineToRight${suffix2}1'),
                offLine:offLine}
            case [0, 1]:
                {elm:Snap.select('#switch${suffix1}1'),
                 line:Snap.select('#switchLineToLeft${suffix2}0'),
                 offLine:offLine}
            case [1, 0]:
                {elm:Snap.select('#switch${suffix1}0'),
                line:Snap.select('#switchLineToRight${suffix2}0'),
                offLine:offLine}
            default:
                throw 'not matched zone: $from $to';
        }
    }

    public static function getHoleEnterY(attackPos:BBox, holePos:BBox):Float {
        return 
            if (isNear(holePos.cx, attackPos.cx)) {
                if (attackPos.y < holePos.y) {
                    holePos.y;
                }
                else {
                    holePos.y + holePos.height;
                }
            } 
            else {
                holePos.cy;
            }
    }

    public static function isNear(v1:Float, v2:Float) {
        var diff = v1 - v2;
        return (Math.abs(diff) < 1);
    }
}
