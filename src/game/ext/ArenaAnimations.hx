package ext;

import snap.Snap;
import js.Promise;

@:native("arenaAnimations")
extern class ArenaAnimations {
  @:selfCall public function new():Void;

  function animateGettingReady():Promise<Int>;
  function animateGimmickPlayed(gimmickView:SnapElement):Promise<Int>;
  function animateGimmickRemoved(gimmickView:SnapElement):Promise<Int>;
  function animateMoveToHole(rockView:SnapElement, attackPath:String):Promise<Int>;
  function animateRockMirrored(rockView:SnapElement, mirrorPosition:BBox):Promise<Int>;
  function animateHoleDefenced(rockView:SnapElement, startPosition:BBox):Promise<Int>;
  function animateAttackDroped(rockView:SnapElement, attackPath:String):Promise<Int>;
  function animateAttackFailed(rockView:SnapElement, startPosition:BBox):Promise<Int>;
  function animateGoal(rockView:SnapElement, atHolePosition:BBox, startPosition:BBox):Promise<Int>;

  function stopAll():Void;
}