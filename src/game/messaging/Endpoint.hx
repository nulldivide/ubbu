package messaging;

private class Queue {

    var messages:Array<Message<Any>>;

    public function new() {
        messages = [];
    }

    public function hasMessages():Bool {
        return (messages.length > 0);
    }

    public function get():Null<Message<Any>> {
        return messages.shift();
    }

    public function put(message:Message<Any>) {
        messages.push(message);
    }
}

class Endpoint {
    
    public var input(default, null):Queue;
    public var output(default, null):Queue;

    public function new() {
        input = new Queue();
        output = new Queue();
    }
}
