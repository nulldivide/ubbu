package messaging;

import ubbu.Types;

typedef Message<TPayload> = {
    var id:Int;
    var type:MessageType<TPayload>;
    var payload:TPayload;
    var time:Float;
}

abstract MessageType<TPayload>(String) to String from String {}

typedef ConnectedPayload = {
    var serializedGameData:String;
    var gameConfig:GameConfig;
}

typedef RockMovedPayload = {
    var playerId:String;
    var from:Int;
    var to:Int;
    var state:RockState;
}

typedef GimmickPlacedPayload = {
    var playerId:String;
    var gimmick:GimmickData;
}

class Messages {
    static var lastId:Int = 0;

    public inline static var CONNECTED:MessageType<ConnectedPayload> = "connected";
    public inline static var PLAYER_JOIN:MessageType<String> = "player_join";
    public inline static var ROCK_MOVED:MessageType<RockMovedPayload> = "rock_moved";
    public inline static var GIMMICK_PLACED:MessageType<GimmickPlacedPayload> = "gimmick_placed";
    public inline static var PLAYER_END_TURN:MessageType<String> = "player_end_turn";
    public inline static var PLAYER_WATCHED:MessageType<String> = "player_watched";

    public static function create<T>(type:MessageType<T>, payload:T):Message<T> {
        var id = ++lastId;

        var message = {
            id:id,
            type:type,
            payload:payload,
            time:Date.now().getTime() / 1000.0
        };

        return message;
    }

    public static function connected(gameData:GameData, gameConfig:GameConfig):Message<ConnectedPayload> {
        return create(CONNECTED, {
            serializedGameData:haxe.Serializer.run(gameData),
            gameConfig:gameConfig
        });
    }

    public static function playerJoin(playerId):Message<String> {
        return create(PLAYER_JOIN, playerId);
    }

    public static function playerEndTurn(playerId):Message<String> {
        return create(PLAYER_END_TURN, playerId);
    }

    public static function playerWatched(playerId):Message<String> {
        return create(PLAYER_WATCHED, playerId);
    }

    public static function rockMoved(playerId, lineFrom, lineTo, state):Message<RockMovedPayload> {
        return create(ROCK_MOVED, {
            playerId:playerId,
            from:lineFrom,
            to:lineTo,
            state:state
        });
    }

    public static function gimmickPlaced(playerId:String, gimmick:GimmickData):Message<GimmickPlacedPayload> {
        return create(GIMMICK_PLACED, {
            playerId:playerId,
            gimmick:gimmick
        });
    }
}