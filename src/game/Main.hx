import ubbu.Game;
import ubbu.Types;
import view.IView;
import view.ArenaView;
import snap.Snap;

class Main {

    static var views:Map<String, IView>;
    static var currentView:IView;
    static var snap:Snap;
    static var params:Map<String, String>;
    static var connection:ServerConnection;

    public static function main() {
        snap = new Snap("#arena");
        var hash = js.Browser.location.hash;
        params = getParams(hash);
        initModel(function(game:Game) {
            initViews(game);
            start();
        });
    }

    public static function navigate(viewId:String) {
        if (!views.exists(viewId)) {
            throw 'view not found: $viewId';
        }
        setView(views[viewId]);
    }

    static function setView(view:IView) {
        if (view != currentView) {
            if (currentView != null) {
                currentView.hide();
            }
            view.show(snap);
            currentView = view;
        }
    }

    static function initModel(onSuccess:Game -> Void) {
        var gameFactory = Game.new;
        connection = new ServerConnection("http://mumumomo.ru/arena/server/", gameFactory);
        var gameId:String = params["gameId"];
        var playerId:String = params["playerId"];
        connection.connectGame(gameId, playerId, onSuccess);
    }

    static function initViews(game:Game) {
        var url = js.Browser.location.href.split("#")[0];
        js.Browser.document.getElementById("gameLink").innerHTML = '$url#gameId=${game.data.id}';
        views = [
            ArenaView.ID => new ArenaView(game, connection.myId)
        ];
    }

    static function start() {
        navigate(ArenaView.ID);
    }

    static function getParams(hash:String):Map<String, String> {
        hash = StringTools.replace(hash, "#", "");
        var result = new Map<String, String>();
        var params:Array<String> = hash.split("&");
        for (pair in params) {
            var kv = pair.split("=");
            result.set(kv[0], kv[1]);
        }
        return result;
    }
}
