import ubbu.Types;
import ubbu.Game;
import haxe.Timer;
import haxe.remoting.HttpAsyncConnection;

using ubbu.DataHelper;

class ServerConnection {

    public var myId(default, null):String;
    var url:String;
    var gameFactory:GameConfig -> GameData -> Game;
    var game:Game;
    var pollingTimer:Timer;
    var connection:HttpAsyncConnection;
    var requestIsPending:Bool;

    public function new(url:String, gameFactory:GameConfig -> GameData -> Game) {
        this.url = url;
        this.gameFactory = gameFactory;
        requestIsPending = false;
        connection = haxe.remoting.HttpAsyncConnection.urlConnect(url);
        connection.setErrorHandler(onError);
    }
    
    public function connectGame(gameId:String, playerId:String, onSuccess:Game -> Void) {
        if (gameId == null) {
            gameId = '${Math.floor(Date.now().getTime())}';
        }
        if (playerId == null) {
            playerId = '${Math.floor(Date.now().getTime())}';
        }
        
        connection.api.connectGame.call([gameId, playerId], function(result:Dynamic) {
            //result.config.positionStageTime = 60;
            game = gameFactory(result.config, result.data);
            myId = playerId;
            startGameSyncing(game);
            onSuccess(game);
        });
    }

    function startGameSyncing(game:Game) {
        game.addEventListener(onGameEvent);
        if (game.data.state == GameState.created) {
            startPolling(waitForOpponent);
        }
    }

    function startPolling(request:Void -> Void ) {
        stopActivePolling();
        pollingTimer = new Timer(500);
        pollingTimer.run = request;
    }

    function sendMyTurn1() {
        var myTurn:PlayerPosition = game.currentRound.getPlayerPosition(myId);
        var enemyId = game.getEnemyId(myId);
        var remoteRound:RoundData = haxe.Unserializer.run(haxe.Serializer.run(game.currentRound));
        remoteRound.startPosition[enemyId] = myTurn;
        remoteRound.endTurnTime[enemyId] = 1;
        onRoundProceed(remoteRound);
    }

    function sendReadyToNextRound1() {
        var time = game.currentRound.getWatchedTime(myId);
        var enemyId = game.getEnemyId(myId);
        haxe.Timer.delay(game.battleWatched.bind(enemyId, time),0);
    }

    function sendMyTurn() {
        var myTurn = game.currentRound.getPlayerPosition(myId);
        connection.api.sendTurn.call([game.data.id, myId, myTurn], 
            function(result:RoundData) {
                if (!result.allPlayersDone()) {
                    startPolling(waitOpponentTurn);
                }
                else {
                    onRoundProceed(result);
                }
            });
    }

    function sendReadyToNextRound() {
        var time = game.currentRound.getWatchedTime(myId);
        connection.api.sendWatched.call([game.data.id, myId, game.currentRoundNum, time],
            function(watchTime:Map<String, Float>) {
                trace('sendReadyToNextRound $watchTime');
                var enemyId = game.getEnemyId(myId);
                if (watchTime[enemyId] > 0) {
                    game.battleWatched(enemyId, watchTime[enemyId]);
                }
                else {
                    startPolling(waitOpponentWatched);
                }
            });
    }

    function onGameEvent(event:GameEvent) {
        switch(event) {
            case playerEndTurn(id):
                 if (id == myId) {
                    sendMyTurn();
                 }
            case playerWatched(id):
                if (id == myId) {
                    sendReadyToNextRound();
                }
            default:
        }
    }

    function waitForOpponent1() {
        game.join("2");
        stopActivePolling();
    }

    function waitForOpponent() {
        if (!requestIsPending) {
            connection.api.waitForOpponent.call([game.data.id, myId], function(result) {
                requestIsPending = false;
                if (result.opponent != "") {
                    game.join(result.opponent);
                    stopActivePolling();
                }
            });
        }
    }

    function waitOpponentTurn() {
        if (!requestIsPending) {
            connection.api.waitOpponentTurn.call([game.data.id, myId], function(result:RoundData) {
                requestIsPending = false;
                if (result.allPlayersDone()) {
                    onRoundProceed(result);
                    stopActivePolling();
                }
            });
        }
    }

    function waitOpponentWatched() {
        if (!requestIsPending) {
            connection.api.waitOpponentWatched.call([game.data.id, myId, game.currentRoundNum], function(watchTime:Map<String, Float>) {
                trace('waitOpponentWatched $watchTime');
                requestIsPending = false;
                var enemyId = game.getEnemyId(myId);
                if (watchTime[enemyId] > 0) {
                    game.battleWatched(enemyId, watchTime[enemyId]);
                    stopActivePolling();
                }
            });
        }
    }

    function onRoundProceed(round:RoundData) {
        var enemyId = game.getEnemyId(myId);
        var enemyPosition = round.getPlayerPosition(enemyId);
        
        var statesToProcess = [RockState.attack, RockState.defence];
        for (state in statesToProcess) {
            var lines = round.getLinesWithRocks(enemyId, state);
            for (line in lines) {
                var rockId = round.getRockId(enemyId, state, line);
                game.moveRock(enemyId, rockId, line, state);
            }
        }

        for (gimmick in enemyPosition.gimmicks) {
            game.placeGimmick(enemyId, gimmick.type, gimmick.lines);
        }
        game.endTurn(enemyId, round.endTurnTime[enemyId]);
    }

    function stopActivePolling() {
        if (pollingTimer != null) {
            pollingTimer.stop();
            pollingTimer = null;
        }
    }

    function onError(err) {
        requestIsPending = false;
        trace("Error : " + Std.string(err));
    }
}