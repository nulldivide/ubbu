package replay;

import ubbu.Game;
import ubbu.Types;
import view.IView;
import view.ArenaView;
import snap.Snap;
import haxe.Timer;

using ubbu.DataHelper;

class Main {

    static var snap:Snap;
    static var params:Map<String, String>;
    static var game:Game;
    static var replayData:GameData;
    static var myId:String;
    static var enemyId:String;
    static var actionQueue:Array<Void -> Void> = [];
    static var replay:Timer;

    public static function main() {
        snap = new Snap("#arena");
        var hash = js.Browser.location.hash;
        params = getParams(hash);
        initModel(function(game:Game) {
            startReplay(game);
        });
    }

    static function initModel(onSuccess:Game -> Void) {
        var gameFactory = Game.new;
        var url = "http://mumumomo.ru/arena/server/";
        var gameId:String = params["gameId"];
        var playerNum = Std.parseInt(params["playerNum"]);
        var connection = haxe.remoting.HttpAsyncConnection.urlConnect(url);
        connection.setErrorHandler(onRemoteError);
        connection.api.getGameLog.call([gameId], function(result) {
            replayData = result.data;
            if (playerNum == 1) {
                myId = replayData.player1.id;
                enemyId = replayData.player2.id;
            }
            else {
                myId = replayData.player2.id;
                enemyId = replayData.player1.id;
            }
            var blankData = DataBuilder.createGame('replay:$gameId');
            game = new Game(result.config, blankData);
            onSuccess(game);
        });
    }

    static function startReplay(game:Game) {
        var view = new ArenaView(game, myId);
        view.show(snap);
        game.join(myId);
        game.join(enemyId);

        for (round in replayData.rounds) {
            var myPosition = round.getPlayerPosition(myId);
            
            var statesToProcess = [RockState.attack, RockState.defence];
            for (state in statesToProcess) {
                var lines = round.getLinesWithRocks(myId, state);
                for (line in lines) {
                    var rockId = round.getRockId(myId, state, line);
                    queue(game.moveRock.bind(enemyId, rockId, line, state));
                }
            }

            for (gimmick in myPosition.gimmicks) {
                queue(game.placeGimmick.bind(enemyId, gimmick.type, gimmick.lines));
            }
            queue(game.endTurn.bind(enemyId, round.endTurnTime[enemyId]));
            queue(proccessEnemyTurn.bind(round));
        }

        replay = new Timer(500);
        replay.run = processNext;
    }

    static function proccessEnemyTurn(round:RoundData) {
        var enemyPosition = round.getPlayerPosition(enemyId);
        
        var statesToProcess = [RockState.attack, RockState.defence];
        for (state in statesToProcess) {
            var lines = round.getLinesWithRocks(enemyId, state);
            for (line in lines) {
                var rockId = round.getRockId(enemyId, state, line);
                game.moveRock(enemyId, rockId, line, state);
            }
        }

        for (gimmick in enemyPosition.gimmicks) {
            game.placeGimmick(enemyId, gimmick.type, gimmick.lines);
        }
        game.endTurn(enemyId, round.endTurnTime[enemyId]);
    }

    static function queue(action:Void->Void) {
        actionQueue.push(action);
    }

    static function processNext() {
        var action = actionQueue.shift();
        if (action != null) {
            action();
        }
        else {
            replay.stop();
        }
    }

    static function getParams(hash:String):Map<String, String> {
        hash = StringTools.replace(hash, "#", "");
        var result = new Map<String, String>();
        var params:Array<String> = hash.split("&");
        for (pair in params) {
            var kv = pair.split("=");
            result.set(kv[0], kv[1]);
        }
        return result;
    }

    static function onRemoteError(err) {
        trace("Error : " + Std.string(err));
    }
}
